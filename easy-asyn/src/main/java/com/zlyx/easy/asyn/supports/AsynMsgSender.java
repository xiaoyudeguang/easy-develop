package com.zlyx.easy.asyn.supports;

/**
 * @Auth 赵光
 * @Describle
 * @2019年6月15日 上午10:42:33
 */
public interface AsynMsgSender {

	/**
	 * 自定义发送异步消息方法(点对点)
	 * 
	 * @param title
	 * @param msg
	 * @return
	 */
	void doQuque(String channel, String msg);

	/**
	 * 自定义发送异步消息方法(发布订阅)
	 * 
	 * @param title
	 * @param msg
	 * @return
	 */
	void doTopic(String channel, String msg);
}
