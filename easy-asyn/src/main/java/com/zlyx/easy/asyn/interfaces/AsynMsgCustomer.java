package com.zlyx.easy.asyn.interfaces;

import java.lang.reflect.ParameterizedType;

/**
 * @Auth 赵光
 * @Describle 异步消息消费者接口
 * @2019年9月7日
 */
public interface AsynMsgCustomer<T> {

	/**
	 * 消息订阅
	 * 
	 * @param channel 通道
	 * @param msgBody 消息内容
	 */
	public void subscribe(String channel, T msg);

	/*
	 * 读取抽象类
	 * 
	 * @return
	 */
	@SuppressWarnings("unchecked")
	default Class<T> getTClass() {
		return (Class<T>) ((ParameterizedType) this.getClass().getGenericInterfaces()[0]).getActualTypeArguments()[0];
	}
}
