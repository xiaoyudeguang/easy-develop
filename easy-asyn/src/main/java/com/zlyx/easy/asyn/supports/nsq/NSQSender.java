package com.zlyx.easy.asyn.supports.nsq;

import org.springframework.beans.factory.annotation.Autowired;

import com.github.brainlag.nsq.NSQProducer;
import com.zlyx.easy.asyn.supports.AsynMsgSender;
import com.zlyx.easy.core.loggers.Logger;

/**
 * @Auth 赵光
 * @Describle
 * @2019年9月7日
 */
public class NSQSender implements AsynMsgSender {

	@Autowired(required = false)
	private NSQProducer producer;

	@Override
	public void doQuque(String channel, String msg) {
		throw new UnsupportedOperationException("不支持QUEUE消息类型");
	}

	@Override
	public void doTopic(String channel, String msg) {
		try {
			producer.produce(channel, msg.getBytes());
		} catch (Exception e) {
			Logger.err(e);
		}
	}

}
