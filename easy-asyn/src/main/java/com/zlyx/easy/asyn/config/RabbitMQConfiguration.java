package com.zlyx.easy.asyn.config;

import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.core.Queue;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.zlyx.easy.asyn.interfaces.AsynMsgCustomer;
import com.zlyx.easy.asyn.supports.AbstractMsgListener;
import com.zlyx.easy.asyn.supports.AsynMsgSender;
import com.zlyx.easy.asyn.supports.rabbitmq.RabbitMQListener;
import com.zlyx.easy.asyn.supports.rabbitmq.RabbitMQSender;
import com.zlyx.easy.asyn.utils.MsgUtils;

/**
 * @Auth 赵光
 * @Describle
 * @2019年9月8日
 */
@Configuration
@ConditionalOnClass(AmqpTemplate.class)
public class RabbitMQConfiguration {

	@Bean
	@ConditionalOnBean(AsynMsgCustomer.class)
	@ConditionalOnMissingBean(AbstractMsgListener.class)
	public RabbitMQListener configRabbitMQListener() {
		return new RabbitMQListener();
	}

	@Bean
	@ConditionalOnMissingBean(AsynMsgSender.class)
	public RabbitMQSender configRabbitMQSender() {
		return new RabbitMQSender();
	}

	@Bean
	@ConditionalOnBean(AsynMsgCustomer.class)
	@ConditionalOnMissingBean(Queue.class)
	public Queue configRabbitMQQueue() {
		return new Queue(MsgUtils.DEFAULT_CHANNEL);
	}

}
