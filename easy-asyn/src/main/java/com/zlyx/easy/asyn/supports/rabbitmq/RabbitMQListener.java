package com.zlyx.easy.asyn.supports.rabbitmq;

import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;

import com.zlyx.easy.asyn.supports.AbstractMsgListener;
import com.zlyx.easy.asyn.utils.MsgUtils;

/**
 * @Auth 赵光
 * @Describle
 * @2019年9月8日
 */
@RabbitListener(queues = MsgUtils.DEFAULT_CHANNEL)
public class RabbitMQListener extends AbstractMsgListener {

	@RabbitHandler
	public void process(String msg) {
		handleMsg(msg);
	}
}
