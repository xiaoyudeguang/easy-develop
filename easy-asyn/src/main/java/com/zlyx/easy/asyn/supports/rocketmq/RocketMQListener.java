package com.zlyx.easy.asyn.supports.rocketmq;

import org.apache.rocketmq.spring.annotation.ConsumeMode;
import org.apache.rocketmq.spring.annotation.MessageModel;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;

import com.zlyx.easy.asyn.supports.AbstractMsgListener;
import com.zlyx.easy.asyn.utils.MsgUtils;

/**
 * @Auth 赵光
 * @Describle
 * @2019年9月7日
 */
@RocketMQMessageListener(topic = MsgUtils.DEFAULT_CHANNEL, consumerGroup = "easy-asyn", consumeMode = ConsumeMode.CONCURRENTLY, messageModel = MessageModel.CLUSTERING)
public class RocketMQListener extends AbstractMsgListener
		implements org.apache.rocketmq.spring.core.RocketMQListener<String> {

	@Override
	public void onMessage(String message) {
		handleMsg(message);
	}
}
