package com.zlyx.easy.asyn.model;

import com.zlyx.easy.core.utils.JsonUtils;

/**
 * @Auth 赵光
 * @Describle 异步消息
 * @2019年9月7日
 */
public class AsynMessage {

	private String channel;

	private Object msg;

	public AsynMessage() {
	}

	public AsynMessage(String channel, Object msg) {
		super();
		this.channel = channel;
		this.msg = msg;
	}

	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public Object getMsg() {
		return msg;
	}

	public void setMsg(Object msg) {
		this.msg = msg;
	}

	@Override
	public String toString() {
		return JsonUtils.toJson(this);
	}
}
