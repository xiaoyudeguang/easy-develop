package com.zlyx.easy.asyn.supports.activemq;

import javax.jms.Destination;

import org.apache.activemq.command.ActiveMQQueue;
import org.apache.activemq.command.ActiveMQTopic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;

import com.zlyx.easy.asyn.supports.AsynMsgSender;

/**
 * @Auth 赵光
 * @Describle
 * @2019年9月7日
 */
public class ActiveMQSender implements AsynMsgSender {

	@Autowired
	private JmsTemplate jmsTemplate;

	@Override
	public void doQuque(String channel, String message) {
		Destination destination = new ActiveMQQueue(channel);
		jmsTemplate.convertAndSend(destination, message);
	}

	@Override
	public void doTopic(String channel, String message) {
		Destination destination = new ActiveMQTopic(channel);
		jmsTemplate.convertAndSend(destination, message);
	}

}
