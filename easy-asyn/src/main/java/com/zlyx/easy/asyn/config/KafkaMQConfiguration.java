package com.zlyx.easy.asyn.config;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.core.KafkaTemplate;

import com.zlyx.easy.asyn.interfaces.AsynMsgCustomer;
import com.zlyx.easy.asyn.supports.AbstractMsgListener;
import com.zlyx.easy.asyn.supports.AsynMsgSender;
import com.zlyx.easy.asyn.supports.kafkamq.KafkaMQListener;
import com.zlyx.easy.asyn.supports.kafkamq.KafkaMQSender;

/**
 * @Auth 赵光
 * @Describle
 * @2019年9月7日
 */
@EnableKafka
@Configuration
@ConditionalOnClass(KafkaTemplate.class)
public class KafkaMQConfiguration {

	@Bean
	@ConditionalOnBean(AsynMsgCustomer.class)
	@ConditionalOnMissingBean(AbstractMsgListener.class)
	public KafkaMQListener configKafkaMQListener() {
		return new KafkaMQListener();
	}

	@Bean
	@ConditionalOnMissingBean(AsynMsgSender.class)
	public KafkaMQSender configKafkaMQSender() {
		return new KafkaMQSender();
	}
}
