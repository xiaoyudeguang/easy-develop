package com.zlyx.easy.asyn.supports.activemq;

import org.springframework.jms.annotation.JmsListener;

import com.zlyx.easy.asyn.supports.AbstractMsgListener;
import com.zlyx.easy.asyn.utils.MsgUtils;

public class ActiveMQListener extends AbstractMsgListener {

	@JmsListener(destination = MsgUtils.DEFAULT_CHANNEL, containerFactory = "jmsListenerContainerTopic")
	public void onTopicMessage(String msg) {
		handleMsg(msg);
	}

	@JmsListener(destination = MsgUtils.DEFAULT_CHANNEL, containerFactory = "jmsListenerContainerQueue")
	public void onQueueMessage(String msg) {
		handleMsg(msg);
	}
}