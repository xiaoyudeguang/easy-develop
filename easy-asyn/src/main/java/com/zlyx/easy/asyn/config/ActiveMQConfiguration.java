package com.zlyx.easy.asyn.config;

import javax.jms.ConnectionFactory;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.config.DefaultJmsListenerContainerFactory;
import org.springframework.jms.config.JmsListenerContainerFactory;

import com.zlyx.easy.asyn.interfaces.AsynMsgCustomer;
import com.zlyx.easy.asyn.supports.AbstractMsgListener;
import com.zlyx.easy.asyn.supports.AsynMsgSender;
import com.zlyx.easy.asyn.supports.activemq.ActiveMQListener;
import com.zlyx.easy.asyn.supports.activemq.ActiveMQSender;

@Configuration
@ConditionalOnClass(JmsListenerContainerFactory.class)
public class ActiveMQConfiguration {

	@Bean
	public JmsListenerContainerFactory<?> jmsListenerContainerTopic(ConnectionFactory activeMQConnectionFactory) {
		DefaultJmsListenerContainerFactory bean = new DefaultJmsListenerContainerFactory();
		bean.setPubSubDomain(true);
		bean.setConnectionFactory(activeMQConnectionFactory);
		return bean;
	}

	@Bean
	public JmsListenerContainerFactory<?> jmsListenerContainerQueue(ConnectionFactory activeMQConnectionFactory) {
		DefaultJmsListenerContainerFactory bean = new DefaultJmsListenerContainerFactory();
		bean.setConnectionFactory(activeMQConnectionFactory);
		return bean;
	}

	@Bean
	@ConditionalOnBean(AsynMsgCustomer.class)
	@ConditionalOnMissingBean(AbstractMsgListener.class)
	public ActiveMQListener configActiveMQListener() {
		return new ActiveMQListener();
	}

	@Bean
	@ConditionalOnMissingBean(AsynMsgSender.class)
	public ActiveMQSender configActiveMQSender() {
		return new ActiveMQSender();
	}
}
