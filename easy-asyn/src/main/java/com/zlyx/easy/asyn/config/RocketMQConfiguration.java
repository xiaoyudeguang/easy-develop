package com.zlyx.easy.asyn.config;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.zlyx.easy.asyn.interfaces.AsynMsgCustomer;
import com.zlyx.easy.asyn.supports.AbstractMsgListener;
import com.zlyx.easy.asyn.supports.AsynMsgSender;
import com.zlyx.easy.asyn.supports.rocketmq.RocketMQListener;
import com.zlyx.easy.asyn.supports.rocketmq.RocketMQSender;

/**
 * @Auth 赵光
 * @Describle 消费者配置
 * @2019年9月7日
 */
@Configuration
@ConditionalOnClass(org.apache.rocketmq.spring.core.RocketMQListener.class)
public class RocketMQConfiguration {

	@Bean
	@ConditionalOnBean(AsynMsgCustomer.class)
	@ConditionalOnMissingBean(AbstractMsgListener.class)
	public RocketMQListener configRocketMQListener() {
		return new RocketMQListener();
	}

	@Bean
	@ConditionalOnMissingBean(AsynMsgSender.class)
	public RocketMQSender configRocketMQSender() {
		return new RocketMQSender();
	}
}
