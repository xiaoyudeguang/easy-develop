package com.zlyx.easy.asyn.supports.kafkamq;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;

import com.zlyx.easy.asyn.supports.AbstractMsgListener;
import com.zlyx.easy.asyn.utils.MsgUtils;

/**
 * @Auth 赵光
 * @Describle
 * @2019年9月7日
 */
public class KafkaMQListener extends AbstractMsgListener {

	@KafkaListener(id = "easy-asyn", topics = MsgUtils.DEFAULT_CHANNEL)
	public void listen(ConsumerRecord<?, ?> record) {
		handleMsg((String) record.value());
	}
}
