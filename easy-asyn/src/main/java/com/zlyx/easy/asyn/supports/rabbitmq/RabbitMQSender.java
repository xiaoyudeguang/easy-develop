package com.zlyx.easy.asyn.supports.rabbitmq;

import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;

import com.zlyx.easy.asyn.supports.AsynMsgSender;

/**
 * @Auth 赵光
 * @Describle
 * @2019年9月8日
 */
public class RabbitMQSender implements AsynMsgSender {

	@Autowired(required = false)
	private AmqpTemplate rabbitTemplate;

	@Override
	public void doQuque(String channel, String msg) {
		throw new UnsupportedOperationException("不支持QUEUE消息类型");
	}

	@Override
	public void doTopic(String channel, String msg) {
		try {
			rabbitTemplate.convertAndSend(channel, msg);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
