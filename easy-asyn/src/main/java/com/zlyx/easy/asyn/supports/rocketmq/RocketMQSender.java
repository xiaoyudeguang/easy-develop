package com.zlyx.easy.asyn.supports.rocketmq;

import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.beans.factory.annotation.Autowired;

import com.zlyx.easy.asyn.supports.AsynMsgSender;

/**
 * @Auth 赵光
 * @Describle
 * @2019年9月7日
 */
public class RocketMQSender implements AsynMsgSender {

	@Autowired(required = false)
	private RocketMQTemplate rocketMQTemplate;

	@Override
	public void doQuque(String channel, String msg) {
		rocketMQTemplate.convertAndSend(channel, msg);
	}

	@Override
	public void doTopic(String channel, String msg) {
		throw new UnsupportedOperationException("不支持TOPIC消息类型");
	}

}
