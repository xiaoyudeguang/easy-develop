package com.zlyx.easy.asyn.supports.nsq;

import com.github.brainlag.nsq.NSQMessage;
import com.github.brainlag.nsq.callbacks.NSQMessageCallback;
import com.zlyx.easy.asyn.supports.AbstractMsgListener;

/**
 * @Auth 赵光
 * @Describle
 * @2019年9月7日
 */
public class NSQListener extends AbstractMsgListener implements NSQMessageCallback {

	@Override
	public void message(NSQMessage message) {
		handleMsg(new String(message.getMessage()));
		message.finished();
	}
}
