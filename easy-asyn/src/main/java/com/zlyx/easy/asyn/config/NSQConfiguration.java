package com.zlyx.easy.asyn.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.github.brainlag.nsq.NSQConsumer;
import com.github.brainlag.nsq.NSQProducer;
import com.github.brainlag.nsq.lookup.DefaultNSQLookup;
import com.github.brainlag.nsq.lookup.NSQLookup;
import com.zlyx.easy.asyn.interfaces.AsynMsgCustomer;
import com.zlyx.easy.asyn.supports.AbstractMsgListener;
import com.zlyx.easy.asyn.supports.AsynMsgSender;
import com.zlyx.easy.asyn.supports.nsq.NSQListener;
import com.zlyx.easy.asyn.supports.nsq.NSQSender;
import com.zlyx.easy.asyn.utils.MsgUtils;

/**
 * @Auth 赵光
 * @Describle 消费者配置
 * @2019年9月7日
 */
@Configuration
@ConfigurationProperties("nsq")
@ConditionalOnClass(NSQProducer.class)
public class NSQConfiguration {

	@Value("${$nsq.produce.host:127.0.0.1}")
	private String produceHost;

	@Value("${$nsq.produce.port:4150}")
	private int producePort;

	@Value("${$nsq.lookup.host:127.0.0.1}")
	private String lookupHost;

	@Value("${$nsq.lookup.port:4161}")
	private int lookupPort;

	@Bean
	@ConditionalOnBean(AsynMsgCustomer.class)
	@ConditionalOnMissingBean(AbstractMsgListener.class)
	public NSQListener configNSQListener() {
		return new NSQListener();
	}

	@Bean
	@ConditionalOnMissingBean(AsynMsgSender.class)
	public NSQSender configNSQSender() {
		return new NSQSender();
	}

	@Bean
	public NSQProducer configNSQProducer() {
		return new NSQProducer().addAddress(produceHost, producePort).start();
	}

	@SuppressWarnings("resource")
	@Bean
	public NSQConsumer configNSQConsumer() {
		NSQLookup lookup = new DefaultNSQLookup();
		lookup.addLookupAddress(lookupHost, lookupPort);
		return new NSQConsumer(lookup, MsgUtils.DEFAULT_CHANNEL, MsgUtils.DEFAULT_CHANNEL, new NSQListener()).start();
	}
}
