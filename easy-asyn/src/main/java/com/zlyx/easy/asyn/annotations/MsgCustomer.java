package com.zlyx.easy.asyn.annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.springframework.stereotype.Component;

/**
 * @Auth 赵光
 * @Describle 异步消息消费者
 * @2019年9月7日
 */
@Target({ ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Component
public @interface MsgCustomer {

	/**
	 * 监听的channel(支持占位符)
	 * 
	 * @return
	 */
	String[] channels();
}
