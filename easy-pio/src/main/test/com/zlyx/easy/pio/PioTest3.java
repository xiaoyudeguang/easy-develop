package com.zlyx.easy.pio;

import java.util.List;

import com.zlyx.easy.core.model.UserModel;
import com.zlyx.easy.core.utils.JsonUtils;
import com.zlyx.easy.pio.model.PioModel;

/**
 * 
 * @Auth 赵光
 * @Describle
 * @2019年1月3日 下午2:54:41
 */
public class PioTest3 {

	public static void main(String[] args) throws Exception {
		// 创建一个PioModel对象来完成后续操作
		PioModel pioModel = new PioModel("D:\\sheet\\test1.xlsx", 4, "测试测试");
		pioModel.setFields("name", "age", "id_num", "phone", "brith", "address");
		List<UserModel> models = JsonUtils.toList(JsonUtils.toJson(pioModel.toMap()));
		System.out.println(models);
	}

}
