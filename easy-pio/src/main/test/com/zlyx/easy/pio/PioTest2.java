package com.zlyx.easy.pio;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import com.zlyx.easy.core.model.UserModel;
import com.zlyx.easy.core.utils.JsonUtils;
import com.zlyx.easy.core.utils.RandomUtils;
import com.zlyx.easy.pio.model.PioModel;

/**
 * 
 * @Auth 赵光
 * @Describle
 * @2019年1月3日 下午2:54:41
 */
public class PioTest2 {

	public static void main(String[] args) throws Exception {
		UserModel model = new UserModel();
		model.setId(RandomUtils.randomID());
		model.setName("小王");
		model.setAge(10);
		Map<String, String> data = JsonUtils.toMap(model);
		writeExcel(data);

		List<Map<String, String>> datas = Arrays.asList(data, data, data, data);
		writeExcel(datas);
	}

	/**
	 * 写出单行数据
	 * 
	 * @param data
	 * @throws Exception
	 */
	public static void writeExcel(Map<String, String> data) throws Exception {
		PioModel pioModel = new PioModel("D:\\sheet\\test21.xlsx", "测试测试");
		pioModel.setHeaders("姓名", "ID", "年龄");
		pioModel.transer(data);
		pioModel.write();
	}

	/**
	 * 写出多行数据
	 * 
	 * @param datas
	 * @throws Exception
	 */
	public static void writeExcel(List<Map<String, String>> datas) throws Exception {
		PioModel pioModel = new PioModel("D:\\sheet\\test22.xlsx", "测试测试");
		pioModel.setHeaders("姓名", "ID", "年龄");
		pioModel.transer(datas);
		pioModel.write();
	}

}
