package com.zlyx.easy.pio;

import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import com.zlyx.easy.core.collections.Lists;
import com.zlyx.easy.pio.model.PioModel;

/**
 * 
 * @Auth 赵光
 * @Describle
 * @2019年1月3日 下午2:54:41
 */
public class PioTest {

	public static void main(String[] args) throws Exception {
		// 创建一个PioModel对象来完成后续操作
		PioModel pioModel = new PioModel("D:\\sheet\\test1.xlsx", 4, "测试测试");
		// 设置表头
		pioModel.setHeaders("姓名", "年龄", "身份证号", "手机号", "出生地", "住址", "年级");
		// 插入一行数据
		pioModel.appendRow(Lists.newList("小王", "18", "142330199501060326", "17696012456", "北京市海淀区", "北京市海淀区"));
		pioModel.appendRow(Lists.newList("小王", "18", "142330199501060326", "17696012456", "北京市海淀区", "北京市海淀区"));
		pioModel.appendRow(Lists.newList("小王", "18", "142330199501060326", "17696012456", "北京市海淀区", "北京市海淀区"));
		// 插入一列数据
		pioModel.insertColumn(Lists.newList("1", "2", "3"), 8, 1);

		// 自定义excel属性
		Workbook book = pioModel.getWorkbook();
		book.setActiveSheet(4);

		// 自定义sheet表格属性
		Sheet sheet = pioModel.getSheet();
		sheet.setDefaultColumnWidth(15);
		sheet.setDefaultRowHeight((short) 5);

		// 写出excel
//		pioModel.view();

//		// 写出excel
		pioModel.write();
	}

}
