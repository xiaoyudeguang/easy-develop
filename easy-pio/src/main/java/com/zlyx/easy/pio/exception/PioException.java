/**
 * 
 * @Auth 赵光
 * @Describle
 * @2019年1月3日 下午2:54:41
 */
package com.zlyx.easy.pio.exception;

/**
 * PIO异常
 * 
 * @Auth 赵光
 * @Describle
 * @2019年1月3日 下午2:54:41
 */
public class PioException extends Exception {

	private static final long serialVersionUID = 1L;

	public PioException(String msg) {
		super(msg);
	}

	/**
	 * 抛出异常
	 * 
	 * @param msg
	 * @throws PioException
	 */
	public static void throwException(String msg) throws PioException {
		throw new PioException(msg);
	}
}
