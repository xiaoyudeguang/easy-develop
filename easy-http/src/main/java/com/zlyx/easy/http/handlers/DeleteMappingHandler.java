package com.zlyx.easy.http.handlers;

import org.springframework.beans.factory.annotation.Autowired;

import com.zlyx.easy.core.handler.annotations.Brancher;
import com.zlyx.easy.http.executor.HttpExecutor;

/**
 * 
 * @Auth 赵光
 * @Describle
 * @2019年12月25日
 */
@Brancher(todo = { "处理DELETE请求" }, value = "DELETE")
public class DeleteMappingHandler extends GetMappingHandler {

	public DeleteMappingHandler(@Autowired(required = false) HttpExecutor httpService) {
		super(httpService);
	}

}
