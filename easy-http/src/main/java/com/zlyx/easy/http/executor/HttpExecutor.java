package com.zlyx.easy.http.executor;

import org.springframework.http.HttpEntity;

import com.zlyx.easy.http.models.RequestModel;

/**
 * 请求执行器
 * 
 * @Auth 赵光
 * @Describle
 * @2020年1月12日
 */
public interface HttpExecutor {

	/**
	 * 执行Http请求
	 * 
	 * @param model
	 * @param entity
	 * @param cls
	 * @return
	 * @throws Exception
	 */
	<T> T excute(RequestModel model, String url, HttpEntity<?> entity, Class<T> cls) throws Exception;
}
