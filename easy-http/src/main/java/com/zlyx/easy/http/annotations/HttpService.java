package com.zlyx.easy.http.annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.springframework.stereotype.Component;

/**
 * Http服务接口注解
 * 
 * @Auth 赵光
 * @Describle
 * @2019年12月25日
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Component
public @interface HttpService {

	/**
	 * 远程服务别名(暂时没啥用，啥时候玩个注册中心啥的就不用改代码了)
	 * 
	 * @return
	 */
	String name() default "";

	/**
	 * 服务端域名或IP+端口
	 * 
	 * @return
	 */
	String url();

	/**
	 * 封装请求参数的类，用法类似mybatis的@SqlProvider
	 * 
	 * @return
	 */
	Class<?> providerClass() default Void.class;

}
