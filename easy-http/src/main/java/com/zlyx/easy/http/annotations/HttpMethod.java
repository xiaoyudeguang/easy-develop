package com.zlyx.easy.http.annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Http服务接口注解
 * 
 * @Auth 赵光
 * @Describle
 * @2019年12月25日
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface HttpMethod {

	/**
	 * 请求路径
	 * 
	 * @return
	 */
	String value();

	/**
	 * 请求方式
	 * 
	 * @return
	 */
	RequestMethod method();

	/**
	 * 封装请求参数的方法
	 * 
	 * @return
	 */
	String providerMethod() default "";

	/**
	 * 请求头
	 * 
	 * @return
	 */
	String[] headers() default "";

	/**
	 * 请求的提交内容类型
	 * 
	 * @return
	 */
	String consume() default "";
}
