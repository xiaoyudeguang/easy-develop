package com.zlyx.easy.http.parser;

import java.lang.reflect.Method;

import com.zlyx.easy.http.models.RequestModel;

/**
 * @Auth 赵光
 * @Describle
 * @2020年1月11日
 */
public interface HttpMethodParser {

	/**
	 * 解析请求类型
	 * 
	 * @param proxyClass
	 * @param method
	 * @param args
	 * @return
	 * @throws Exception
	 */
	RequestModel parse(Class<?> proxyClass, Method method, Object[] args) throws Exception;

}
