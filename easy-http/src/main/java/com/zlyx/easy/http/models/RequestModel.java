package com.zlyx.easy.http.models;

import java.util.Map;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.web.bind.annotation.RequestMethod;

import com.zlyx.easy.core.map.Maps;
import com.zlyx.easy.core.utils.JsonUtils;

/**
 * 
 * @Auth 赵光
 * @Describle
 * @2019年12月25日
 */
public class RequestModel {

	/**
	 * 远程服务别名
	 */
	private String name;

	/**
	 * 服务接口
	 */
	private Class<?> cls;

	/**
	 * 请求方式
	 */
	private RequestMethod method;

	/**
	 * 远程地址
	 */
	private String baseUrl;

	/**
	 * 请求地址
	 */
	private String path;

	/**
	 * 请求参数
	 */
	private Map<String, Object> params;

	/**
	 * 请求头
	 */
	private HttpHeaders headers;

	/**
	 * mediaType
	 */
	private String mediaType;

	/**
	 * 返回类型
	 */
	private Class<?> returnType;

	public Class<?> getCls() {
		return cls;
	}

	public void setCls(Class<?> cls) {
		this.cls = cls;
	}

	public RequestMethod getMethod() {
		return method;
	}

	public HttpMethod getHttpMethod() {
		return HttpMethod.valueOf(method.name());
	}

	public void setMethod(RequestMethod method) {
		this.method = method;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public Map<String, Object> getParams() {
		if (params == null) {
			params = Maps.newMap();
		}
		return params;
	}

	public void setParams(Map<String, Object> params) {
		this.params = params;
	}

	public Class<?> getReturnType() {
		if (returnType == void.class) {
			return String.class;
		}
		return returnType;
	}

	public void setReturnType(Class<?> returnType) {
		this.returnType = returnType;
	}

	public String getBaseUrl() {
		return baseUrl;
	}

	public void setBaseUrl(String baseUrl) {
		this.baseUrl = baseUrl;
	}

	public HttpHeaders getHeaders() {
		return headers;
	}

	public void setHeaders(HttpHeaders headers) {
		this.headers = headers;
	}

	public String getMediaType() {
		return mediaType;
	}

	public void setMediaType(String mediaType) {
		this.mediaType = mediaType;
	}

	/**
	 * 请求路径
	 * 
	 * @return
	 */
	public String getUrl() {
		if (baseUrl != null && baseUrl.endsWith("/")) {
			baseUrl = baseUrl.substring(0, baseUrl.length() - 1);
		}
		if (baseUrl == null) {
			baseUrl = "";
		}
		if (path != null && !path.startsWith("/")) {
			path = "/" + path;
		}
		if (path == null) {
			path = "";
		}
		return baseUrl + path;
	}

	@Override
	public String toString() {
		return JsonUtils.toJson(this);
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
}
