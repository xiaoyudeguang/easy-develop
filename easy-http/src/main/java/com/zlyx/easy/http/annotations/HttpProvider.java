package com.zlyx.easy.http.annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Http服务接口注解
 * 
 * @Auth 赵光
 * @Describle
 * @2019年12月25日
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface HttpProvider {

	/**
	 * 指定参数封装方法
	 * 
	 * @return
	 */
	String providerMethod() default "";

}
