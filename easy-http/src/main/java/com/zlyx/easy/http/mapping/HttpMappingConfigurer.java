/**
 *
 * @Author 赵光
 * @Desc HttpMapping
 * @Date 2020年6月11日
 */
package com.zlyx.easy.http.mapping;

/**
 * 服务映射配置接口
 * 
 * @Author 赵光
 * @Desc HttpMapping
 * @Date 2020年6月11日
 */
public interface HttpMappingConfigurer {

	/**
	 * 根据名称获取请求url
	 * 
	 * @param name
	 * @return
	 */
	String getHttpUrl(String name);
}
