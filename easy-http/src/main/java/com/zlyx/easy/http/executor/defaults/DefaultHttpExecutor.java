package com.zlyx.easy.http.executor.defaults;

import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMethod;

import com.zlyx.easy.core.http.HttpUtils;
import com.zlyx.easy.core.loggers.Logger;
import com.zlyx.easy.core.utils.RandomUtils;
import com.zlyx.easy.http.executor.HttpExecutor;
import com.zlyx.easy.http.models.RequestModel;

/**
 * 默认请求执行器
 * 
 * @Auth 赵光
 * @Describle
 * @2020年1月12日
 */
public class DefaultHttpExecutor implements HttpExecutor {

	public <T> T excute(RequestModel model, String url, HttpEntity<?> entity, Class<T> cls) throws Exception {
		RequestMethod method = model.getMethod();
		String id = RandomUtils.randomID();
		Logger.info(model.getCls(), "【{}】 {} 请求地址 ：{}", id, method, url);
		if (entity != null) {
			Logger.info(model.getCls(), "【{}】 {} 请求参数 ：{}", id, method, entity.getBody());
		}
		ResponseEntity<T> response = HttpUtils.restTemplate().exchange(url, model.getHttpMethod(), entity, cls);
		if (response != null) {
			Logger.info(model.getCls(), "【{}】 {} 请求结果 ：{}", id, method, response.getBody());
			return response.getBody();
		}
		return null;
	}
}