package com.zlyx.easy.http.headers;

import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

/**
 * HttpHeaders构建工具
 * 
 * @Auth 赵光
 * @Describle
 * @2020年1月12日
 */
public class Headers {

	/**
	 * application/atom+xml
	 */
	public static HttpHeaders APPLICATION_ATOM_XML_HEADERS = build(MediaType.APPLICATION_ATOM_XML);

	/**
	 * application/x-www-form-urlencoded
	 */
	public static HttpHeaders APPLICATION_FORM_URLENCODED_HEADERS = build(MediaType.APPLICATION_FORM_URLENCODED);

	/**
	 * application/json
	 */
	public static HttpHeaders APPLICATION_JSON_HEADERS = build(MediaType.APPLICATION_JSON);

	/**
	 * application/json;charset=UTF-8
	 */
	public static final HttpHeaders APPLICATION_JSON_UTF8_HEADERS = build(MediaType.APPLICATION_JSON_UTF8);

	/**
	 * application/octet-stream
	 */
	public static HttpHeaders APPLICATION_OCTET_STREAM_HEADERS = build(MediaType.APPLICATION_OCTET_STREAM);

	/**
	 * application/pdf
	 */
	public static HttpHeaders APPLICATION_PDF_HEADERS = build(MediaType.APPLICATION_PDF);

	/**
	 * application/problem+json
	 */
	public static HttpHeaders APPLICATION_PROBLEM_JSON_HEADERS = build(MediaType.APPLICATION_PROBLEM_JSON);

	/**
	 * application/problem+json;charset=UTF-8
	 */
	public static HttpHeaders APPLICATION_PROBLEM_JSON_UTF8_HEADERS = build(MediaType.APPLICATION_PROBLEM_JSON_UTF8);

	/**
	 * application/problem+xml
	 */
	public static HttpHeaders APPLICATION_PROBLEM_XML_HEADERS = build(MediaType.APPLICATION_PROBLEM_XML);

	/**
	 * application/rss+xml
	 */
	public static HttpHeaders APPLICATION_RSS_XML_HEADERS = build(MediaType.APPLICATION_RSS_XML);

	/**
	 * application/stream+json
	 */
	public static HttpHeaders APPLICATION_STREAM_JSON_HEADERS = build(MediaType.APPLICATION_STREAM_JSON);

	/**
	 * application/xhtml+xml
	 */
	public static HttpHeaders APPLICATION_XHTML_XML_HEADERS = build(MediaType.APPLICATION_XHTML_XML);

	/**
	 * application/xml
	 */
	public static HttpHeaders APPLICATION_XML_HEADERS = build(MediaType.APPLICATION_XML);

	/**
	 * image/gif
	 */
	public static HttpHeaders IMAGE_GIF_HEADERS = build(MediaType.IMAGE_GIF);

	/**
	 * image/jpeg
	 */
	public static HttpHeaders IMAGE_JPEG_HEADERS = build(MediaType.IMAGE_JPEG);

	/**
	 * image/png
	 */
	public static HttpHeaders IMAGE_PNG_HEADERS = build(MediaType.IMAGE_PNG);

	/**
	 * application/xhtml+xml
	 */
	public static HttpHeaders MULTIPART_FORM_DATA_HEADERS = build(MediaType.MULTIPART_FORM_DATA);

	/**
	 * multipart/form-data
	 */
	public static HttpHeaders TEXT_EVENT_STREAM_HEADERS = build(MediaType.TEXT_EVENT_STREAM);

	/**
	 * text/event-stream
	 */
	public static HttpHeaders TEXT_HTML_HEADERS = build(MediaType.TEXT_HTML);

	/**
	 * text/markdown
	 */
	public static HttpHeaders TEXT_MARKDOWN_HEADERS = build(MediaType.TEXT_MARKDOWN);

	/**
	 * text/plain
	 */
	public static HttpHeaders TEXT_PLAIN_HEADERS = build(MediaType.TEXT_PLAIN);

	/**
	 * text/xml
	 */
	public static HttpHeaders TEXT_XML_HEADERS = build(MediaType.TEXT_XML);

	/**
	 * 构建HttpHeaders
	 * 
	 * @param mediaType
	 * @return
	 */
	public static HttpHeaders build(MediaType mediaType) {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(mediaType);
		return headers;
	}

	/**
	 * 构建HttpHeaders
	 * 
	 * @param mediaTypeValue
	 * @return
	 */
	public static HttpHeaders build(String mediaTypeValue) {
		HttpHeaders headers = new HttpHeaders();
		headers.set(HttpHeaders.CONTENT_TYPE, mediaTypeValue);
		return headers;
	}
}
