package com.zlyx.easy.http.handlers;

import com.zlyx.easy.core.spring.SpringUtils;
import com.zlyx.easy.http.executor.HttpExecutor;
import com.zlyx.easy.http.executor.defaults.DefaultHttpExecutor;
import com.zlyx.easy.http.models.RequestModel;

/**
 * 
 * @Auth 赵光
 * @Describle
 * @2019年12月25日
 */
public abstract class AbstractMappingHandler {

	protected HttpExecutor httpService;

	/**
	 * 创建包含默认DefaultHttpService的处理器
	 */
	public AbstractMappingHandler() {
		this(null);
	}

	/**
	 * 创建处理器
	 * 
	 * @param httpService
	 */
	public AbstractMappingHandler(HttpExecutor httpService) {
		this.httpService = httpService;
		if (this.httpService == null) {
			this.httpService = new DefaultHttpExecutor();
		}
	}

	/**
	 * 处理请求
	 * 
	 * @param requestModel
	 * @return
	 * @throws Exception
	 */
	public abstract Object handleMapping(RequestModel requestModel) throws Exception;

	/**
	 * 转发业务
	 * 
	 * @param beanName
	 * @param data
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public static <T> T call(RequestModel rm) throws Exception {
		return (T) SpringUtils.<AbstractMappingHandler>getBean(rm.getMethod().name()).handleMapping(rm);
	}

}