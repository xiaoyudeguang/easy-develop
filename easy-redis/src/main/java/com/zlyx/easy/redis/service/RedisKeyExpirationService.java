package com.zlyx.easy.redis.service;

/**
 * <p>
 * Redis键过期处理 Service
 * </p>
 *
 * @author zg
 */
public interface RedisKeyExpirationService {

	/**
	 * 处理键过期事件
	 *
	 * @param expiredKey 过期key
	 * @param pattern    pattern
	 */
	void doProcess(String expiredKey, byte[] pattern);
}
