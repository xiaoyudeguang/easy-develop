package com.zlyx.easy.redis.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.springframework.context.annotation.Import;

import com.zlyx.easy.redis.config.RedisListenerConfig;

/**
 * <p>
 * 启用Redis键过期监听器
 * </p>
 *
 * @author 赵光
 * @version v1.0
 */
@Documented
@Inherited
@Target({ ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
@Import({ RedisListenerConfig.class })
public @interface EnableRedisListener {
}
