package com.zlyx.easy.weixin.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.zlyx.easy.core.loggers.Logger;
import com.zlyx.easy.core.utils.ClassUtils;
import com.zlyx.easy.weixin.service.WeiXinDataService;
import com.zlyx.easy.weixin.service.WeiXinHttpService;

import springfox.documentation.annotations.ApiIgnore;

/**
 * 微信web接口
 * 
 * @Auth 赵光
 * @Describle
 * @2019年1月3日 下午2:54:41
 */
@ApiIgnore
@RestController
@RequestMapping("/${application.simple-name:}")
public class WeiXinController {

	@Autowired(required = false)
	private WeiXinDataService dataService;

	@Autowired
	private WeiXinHttpService httpService;

	/**
	 * 获取微信token
	 * 
	 * @param appId  微信appId
	 * @param secret 微信秘钥
	 * @return
	 * @throws Exception
	 */
	@GetMapping("/token")
	public Object token(@RequestParam String appId, String secret) throws Exception {
		Logger.debug("WexinController.token: appId = {}, secret ={}", appId, secret);
		return httpService.token(appId, secret);
	}

	/**
	 * 接收微信数据
	 * 
	 * @param dataMap 数据
	 * @return
	 */
	@PostMapping("/callback")
	public Object callback(@RequestBody Map<String, String> dataMap) {
		Logger.debug("WexinController.callback: dataMap = {}", dataMap);
		return ClassUtils.getInstance(dataService, WeiXinDataService.class).handleData(dataMap);
	}

	/**
	 * 发送数据到微信
	 * 
	 * @param path        路径
	 * @param accessToken 微信token
	 * @param dataMap     数据
	 * @return
	 * @throws Exception
	 */
	@PostMapping("/send")
	public Object sendData(String path, String accessToken, Map<String, Object> dataMap) throws Exception {
		Logger.debug("WexinController.send: path = {}, accessToken = {}, dataMap = {}", path, accessToken, dataMap);
		return httpService.sendData(path, accessToken, dataMap);
	}
}
