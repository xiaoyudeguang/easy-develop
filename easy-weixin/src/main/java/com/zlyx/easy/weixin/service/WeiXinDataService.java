package com.zlyx.easy.weixin.service;

import java.util.Map;

/**
 * 微信数据处理业务 Service
 * 
 * @Auth 赵光
 * @Describle
 * @2019年1月3日 下午2:54:41
 */
public interface WeiXinDataService {

	/**
	 * 处理数据
	 * 
	 * @param dataMap
	 * @return
	 */
	public Object handleData(Map<String, String> dataMap);
}
