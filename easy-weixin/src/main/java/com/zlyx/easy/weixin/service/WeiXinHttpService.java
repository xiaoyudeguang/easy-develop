package com.zlyx.easy.weixin.service;

import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.zlyx.easy.core.http.HttpResponse;
import com.zlyx.easy.core.http.HttpUtils.HttpClient;
import com.zlyx.easy.http.headers.Headers;

/**
 * 微信交互 HttpService
 * 
 * @Auth 赵光
 * @Describle
 * @2019年1月3日 下午2:54:41
 */
@Component
public class WeiXinHttpService {

	@Value("${easy.weixin.api.url:https://api.weixin.qq.com}")
	private String url;

	@Value("${easy.weixin.api.token-path:/cgi-bin/token}")
	private String tokenPath;

	/**
	 * 获取微信token
	 * 
	 * @param appId  微信appId
	 * @param secret 微信秘钥
	 * @return
	 * @throws Exception
	 */
	public HttpResponse token(String appId, String secret) throws Exception {
		return HttpClient.url(url, tokenPath).param("appId", appId).param("secret", secret)
				.param("grant_type", "client_credential").get();
	}

	/**
	 * 发送数据到微信
	 * 
	 * @param path        地址后缀
	 * @param accessToken 微信token
	 * @param dataMap     数据
	 * @return
	 * @throws Exception
	 */
	public HttpResponse sendData(String path, String accessToken, Map<String, Object> dataMap) throws Exception {
		return HttpClient.url(url, path).headers(Headers.APPLICATION_JSON_UTF8_HEADERS)
				.param("access_token", accessToken).params(dataMap).post();
	}
}
