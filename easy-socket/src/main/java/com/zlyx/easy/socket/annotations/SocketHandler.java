package com.zlyx.easy.socket.annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.springframework.stereotype.Component;

/**
 * Socket消息处理器
 * 
 * @Auth 赵光
 * @Describle
 * @2018年12月22日 下午4:41:07
 */
@Target({ ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Component
public @interface SocketHandler {

	/**
	 * 不允许出现端口一样的情况
	 * 
	 * @return
	 */
	int port();

	/**
	 * 描述
	 * 
	 * @return
	 */
	String[] todo();

	/**
	 * 开启状态(默认开启)
	 * 
	 * @return
	 */
	boolean isOpen() default true;

	/**
	 * 消息服务类型
	 * 
	 * @return
	 */
	MsgType type();

	/**
	 * 缓冲区大小(可根据接收数据长度动态配置,默认为1024*1
	 * 
	 * @return
	 */
	float size() default 1;

	public enum MsgType {
		/**
		 * AIO_TCP
		 */
		ATM,
		/**
		 * BIO_TCP
		 */
		BTM,
		/**
		 * BIO_UDP
		 */
		BUM,
		/**
		 * NIO_TCP
		 */
		NTM,
		/**
		 * NIO_UDP
		 */
		NUM;
	}
}