package com.zlyx.easy.socket.interfaces;

import com.zlyx.easy.core.handler.IHandler;

/**
 * 消息处理器接口
 * 
 * @author 赵光
 * @Description: 消息处理器接口
 * @date 2018年11月14日
 */
public interface IMsgHandler<T> extends IHandler<T> {

	/**
	 * 逻辑处理
	 * 
	 * @param data
	 * @return
	 */
	T doHandle(T data) throws Exception;

	/**
	 * 方法级控制开关(优先级高于@MsgHandler注解)
	 * 
	 * @return
	 */
	default boolean isOpen() {
		return true;
	}

}
