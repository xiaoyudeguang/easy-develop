package com.zlyx.easy.socket.defaults;

import com.zlyx.easy.core.loggers.Logger;
import com.zlyx.easy.socket.interfaces.IMsgHandler;

/**
 * 默认消息处理器
 * 
 * @author 赵光
 * @Description: 默认消息处理器
 * @date 2018年11月14日
 */
public class DefaultMsgHandler implements IMsgHandler<String> {

	@Override
	public String doHandle(String data) throws Exception {
		Logger.info("接收数据 -> " + data);
		return null;
	}
}
