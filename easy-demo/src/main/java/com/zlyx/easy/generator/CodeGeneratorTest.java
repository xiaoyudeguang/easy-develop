///**
// *
// * @Author EDZ
// * @Desc CodeGenerator
// * @Date 2020年5月10日
// */
//package com.zlyx.easy.generator;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.context.ApplicationContext;
//import org.springframework.stereotype.Component;
//
//import com.baomidou.mybatisplus.generator.config.DataSourceConfig;
//import com.zlyx.easy.core.refresh.IHandlerOnRefreshed;
//import com.zlyx.easy.web.generate.core.CodeGenerator;
//import com.zlyx.easy.web.generate.core.defaults.DefaultTypeConvert;
//
///**
// *
// * @Author EDZ
// * @Desc CodeGenerator
// * @Date 2020年5月10日
// */
//@Component
//public class CodeGeneratorTest implements IHandlerOnRefreshed {
//
//	@Autowired
//	private CodeGenerator codeGenerator;
//
//	/**
//	 * 代码生成方式1
//	 */
//
//	@Override
//	public void doOnRefreshed(ApplicationContext context) throws Exception {
//		codeGenerator.run("com.zlyx.easy.test", "User", "user", "赵光");
//	}
//
//	/**
//	 * 代码生成方式二
//	 * 
//	 * @param args
//	 */
//	public static void main(String[] args) {
//		DataSourceConfig dataSource = new DataSourceConfig();
//		dataSource.setUrl("jdbc:mysql://127.0.0.1:3306/test?useUnicode=true&amp;characterEncoding=UTF-8&useSSL=false");
//		dataSource.setDriverName("com.mysql.jdbc.Driver");
//		dataSource.setUsername("root");
//		dataSource.setPassword("123456");
//		dataSource.setTypeConvert(DefaultTypeConvert.typeConvert);
//		CodeGenerator codeGenerator = new CodeGenerator(dataSource);
//		codeGenerator.run("com.zlyx.easy.test", "User", "user", "赵光");
//	}
//
//}
