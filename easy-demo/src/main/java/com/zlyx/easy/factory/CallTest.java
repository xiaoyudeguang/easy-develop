package com.zlyx.easy.factory;

import com.zlyx.easy.core.factory.defaults.annotations.Caller;

/**
 * @Auth 赵光
 * @Describle
 * @2019年6月18日 下午8:57:19
 */
@Caller(todo = "Caller")
public interface CallTest {

	@Caller(todo = "测试", value = "testHandler")
	public String test(String name, int age);
}
