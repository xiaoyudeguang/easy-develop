package com.zlyx.easy.http.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.zlyx.easy.core.loggers.Logger;
import com.zlyx.easy.core.model.UserModel;
import com.zlyx.easy.core.utils.RequestUtils;
import com.zlyx.easy.http.service.TestService;

@RestController
@RequestMapping("version/{version}")
public class HttpController {

	@Autowired
	private TestService testService;

	@PostMapping("appId/{appId}/http/testPost")
	public List<UserModel> testPost(String appId, Double version, String name, Integer age) throws Exception {
		Logger.info("params = {}", RequestUtils.getParamsMap());
		return testService.testPost(appId, version, name, age);
	}

	@GetMapping("appId/{appId}/http/testGet")
	public List<UserModel> testGet(String appId, Double version, String name, Integer age) throws Exception {
		Logger.info("params = {}", RequestUtils.getParamsMap());
		return testService.testGet(appId, version, name, age);
	}

	@PutMapping("appId/{appId}/http/testPut")
	public List<UserModel> testPut(String appId, Double version, String name, Integer age) throws Exception {
		Logger.info("params = {}", RequestUtils.getParamsMap());
		return testService.testPut(appId, version, name, age);
	}

	@DeleteMapping("appId/{appId}/http/testDelete")
	public List<UserModel> testDelete(String appId, Double version, String name, Integer age) throws Exception {
		Logger.info("params = {}", RequestUtils.getParamsMap());
		return testService.testDelete(appId, version, name, age);
	}

	@PostMapping(value = "/appId/{appId}/http/testPayload", consumes = { MediaType.APPLICATION_JSON_UTF8_VALUE })
	public List<UserModel> testPayload(String appId, double version, String name, Integer age) {
		return testService.testPayload(appId, version, new UserModel(name, age));
	}

	@PostMapping(value = "version/{version/appId/{appId}/test/testForm")
	public List<UserModel> testForm(String appId, double version, String name, Integer age) {
		return testService.testForm(appId, version, new UserModel(name, age));
	}

	@PostMapping(value = "version/{version/appId/{appId}/test/testHttpMethod")
	public List<UserModel> testHttpMethod(String appId, double version, String name, Integer age) {
		return testService.testHttpMethod(appId, version, new UserModel(name, age));
	}
}
