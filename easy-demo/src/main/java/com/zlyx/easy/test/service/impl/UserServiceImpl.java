package com.zlyx.easy.test.service.impl;

import com.zlyx.easy.test.entity.User;
import com.zlyx.easy.test.mapper.UserMapper;
import com.zlyx.easy.test.service.UserService;
import com.zlyx.easy.web.web.service.impl.AbstractServiceImpl;

import com.zlyx.easy.web.web.annotations.SpringService;

/**
 * 用户表 业务实现类
 * @author 赵光
 * @since 2020-05-23
 */
@SpringService(todo = "用户表业务实现类")
public class UserServiceImpl extends AbstractServiceImpl<UserMapper, User> implements UserService {

}
