package com.zlyx.easy.test.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMethod;

import com.zlyx.easy.core.http.HttpResponse;
import com.zlyx.easy.swagger.annotations.SpringController;
import com.zlyx.easy.swagger.annotations.SpringMapping;
import com.zlyx.easy.test.entity.User;
import com.zlyx.easy.test.service.UserService;
import com.zlyx.easy.web.web.controller.AbstractController;

/**
 * 用户表相关接口
 * 
 * @author 赵光
 * @date 2020-05-23
 */
@SpringController(value = { "/user" }, todo = { "用户表相关接口" })
public class UserController extends AbstractController<UserService, User> {

	@Autowired
	protected UserService baseService;

	@SpringMapping(value = { "/test" }, todo = { "调用service层示例" }, method = RequestMethod.GET)
	public int callService(String name, Integer age) {
		System.out.println(name + "*" + this.baseService.hashCode());
		return this.baseService.hashCode();
	}

	@SpringMapping(value = { "/throwException" }, todo = { "测试异常处理" })
	public HttpResponse throwException(String name, Integer age) throws Exception {
		throw new Exception("测试接口是否正常返回");
	}
}
