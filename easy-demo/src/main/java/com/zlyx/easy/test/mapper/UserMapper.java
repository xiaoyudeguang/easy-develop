package com.zlyx.easy.test.mapper;

import com.zlyx.easy.mybatis.annotations.EasyMapper;

import com.zlyx.easy.test.entity.User;
import com.zlyx.easy.web.web.mybatis.AbstractMapper;

/**
 * 用户表Mapper接口
 * @author 赵光
 * @since 2020-05-23
 */
@EasyMapper(todo = "用户表Mapper接口")
public interface UserMapper extends AbstractMapper<User> {

}
