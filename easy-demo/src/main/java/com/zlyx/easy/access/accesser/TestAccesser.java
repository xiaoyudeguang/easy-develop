package com.zlyx.easy.access.accesser;

import java.util.List;

import com.zlyx.easy.access.annotations.EasyAccesser;
import com.zlyx.easy.access.defaults.annotations.Select;
import com.zlyx.easy.access.domain.User;
import com.zlyx.easy.database.annotations.EasySelect;

/**
 * @Auth 赵光
 * @Describle
 * @2019年12月31日
 */
@EasyAccesser(todo = { "测试Accesser" })
public interface TestAccesser {

	@Select("select * from user")
	public List<User> findList();

	@EasySelect(tableCls = User.class, results = { "id", "name" }, where = { "name like '%${name}%'" })
	public List<User> listUserModel(String id, String name);
}
