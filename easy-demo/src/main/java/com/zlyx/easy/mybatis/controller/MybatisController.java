package com.zlyx.easy.mybatis.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.zlyx.easy.access.domain.User;
import com.zlyx.easy.mybatis.mapper.TestMapper;

@RestController
@RequestMapping("/mybatis")
public class MybatisController {

	@Autowired
	private TestMapper testMapper;

	@PostMapping("/findList")
	public List<User> findlist(String ids) {
		return testMapper.findlist(ids);
	}

	@GetMapping("/easyList")
	public List<User> listUserModel(String id, String name) {
		return testMapper.listUserModel(id, name);
	}

	@GetMapping("/page")
	public List<User> listUserModel(String id, String name, Integer pageNum, Integer pageSize) {
		return testMapper.listUserModel(id, name, pageNum, pageSize);
	}

	@GetMapping("/mybatis-plus")
	public List<User> listUserModelByPlus(String id, String name, Integer pageNum, Integer pageSize) {
		return testMapper.selectByMap(null);
	}
}
