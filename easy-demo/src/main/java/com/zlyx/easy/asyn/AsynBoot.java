/**
 * 
 * @Auth 赵光
 * @Describle
 * @2019年1月3日 下午2:54:41
 */
package com.zlyx.easy.asyn;

import org.springframework.context.ApplicationContext;

import com.zlyx.easy.asyn.annotations.MsgCustomer;
import com.zlyx.easy.asyn.interfaces.AsynMsgCustomer;
import com.zlyx.easy.asyn.utils.MsgUtils;
import com.zlyx.easy.core.console.Console;
import com.zlyx.easy.core.refresh.IHandlerOnRefreshed;

/**
 * 
 * @Auth 赵光
 * @Describle
 * @2019年1月3日
 */
@MsgCustomer(channels = { MsgUtils.DEFAULT_CHANNEL })
public class AsynBoot implements AsynMsgCustomer<String>, IHandlerOnRefreshed {
//
//	@Autowired
//	private JmsTemplate kmsTemplate;

	@Override
	public void doOnRefreshed(ApplicationContext context) throws Exception {
		MsgUtils.doTopic(MsgUtils.DEFAULT_CHANNEL, "发送消息");
//		ThreadUtils.execute(new Thread(() -> {
//			System.out.println(kmsTemplate.getConnectionFactory());
//			ConnectionFactory factory = new ActiveMQConnectionFactory(
//					URI.create("vm://localhost?broker.persistent=false"));
//			JmsTemplate jmsTemplate = new JmsTemplate(factory);
//			Destination destination = new ActiveMQQueue("easy-asyn-channel");
//			jmsTemplate.convertAndSend(destination, MsgUtils.newMessage(MsgUtils.DEFAULT_CHANNEL, "jmsTemplate"));
//		}));
//		RedisUtils.publish("easy-redis-channel", AsynMessage.newMessage(MsgUtils.DEFAULT_CHANNEL, "RedisUtils"));
//		stringRedisTemplate.convertAndSend("easy-redis-channel",
//				AsynMessage.newMessage(MsgUtils.DEFAULT_CHANNEL, "stringRedisTemplate"));
	}

	@Override
	public void subscribe(String channel, String msgBody) {
		Console.log(channel, msgBody);
	}

}
