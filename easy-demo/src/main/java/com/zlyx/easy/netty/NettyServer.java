package com.zlyx.easy.netty;

import com.zlyx.easy.core.buffer.EasyBuffer;
import com.zlyx.easy.core.loggers.Logger;
import com.zlyx.easy.netty.server.annotations.Server;
import com.zlyx.easy.netty.server.core.AbstractNettyServer;

/**
 * @Auth 赵光
 * @Describle
 * @2019年1月25日 下午2:52:23
 */
@Server(port = 9000, todo = "netty测试服务")
public class NettyServer extends AbstractNettyServer<String> {

	@Override
	public String doHandle(String data) throws Exception {
		Logger.info(data);
		return EasyBuffer.at("服务端响应消息", data);
	}
}
