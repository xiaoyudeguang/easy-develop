package com.zlyx.easy.drools;

import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.zlyx.easy.drools.builder.RuleBuilder;
import com.zlyx.easy.drools.domain.RuleContextDomain;
import com.zlyx.easy.drools.executor.RuleExecutor;
import com.zlyx.easy.drools.validate.RuleReader;
import com.zlyx.easy.drools.validate.RuleValidator;

/**
 * 导入规则并验证
 *
 * @Author 赵光
 * @Desc DroolsTest
 * @Date 2020年5月11日
 */
public class TestRuleExecutor extends RuleExecutor<RuleContextDomain> {

	@Override
	public RuleBuilder.RuleBody doFilter(RuleBuilder.RuleBody ruleBody) {
		// 导入类
		ruleBody.importClass(RuleContextDomain.class);
		ruleBody.importClass(Arrays.class);
		ruleBody.importClass(Logger.class);
		ruleBody.importClass(LoggerFactory.class);
		ruleBody.importClass(List.class);

		// 将常量替换为$符号引用的变量
		ruleBody.placeholders("10", "$count");

		// 将$符号引用的变量替换为常量
		ruleBody.props("count", "6");
		ruleBody.props("message", "执行完毕");
		return ruleBody;
	}

	@Override
	public RuleContextDomain initContext() throws Exception {
		return new RuleContextDomain();
	}

	public static void main(String[] args) throws Exception {
		// 验证规则
		RuleValidator.validate("测试测试", new TestRuleExecutor());
		// 规则导入为java类
		RuleReader.toJava("测试测试", new TestRuleExecutor());
	}

}
