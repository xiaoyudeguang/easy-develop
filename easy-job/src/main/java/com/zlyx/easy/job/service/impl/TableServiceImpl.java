package com.zlyx.easy.job.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zlyx.easy.core.sql.SqlBuilder;
import com.zlyx.easy.core.sql.TableEntity;
import com.zlyx.easy.job.service.ITableService;
import com.zlyx.easy.mybatis.mapper.MybatisMapper;

@Service
public class TableServiceImpl implements ITableService {

	@Autowired
	private MybatisMapper mybatisMapper;

	@Override
	public Object create(TableEntity te) throws Exception {
		return mybatisMapper.update(SqlBuilder.createSQL(te));
	}

}
