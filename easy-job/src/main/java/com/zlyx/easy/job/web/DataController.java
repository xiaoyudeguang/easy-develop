package com.zlyx.easy.job.web;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.zlyx.easy.core.http.HttpResponse;
import com.zlyx.easy.core.map.Maps;
import com.zlyx.easy.database.local.Page;
import com.zlyx.easy.job.service.IDataService;

import springfox.documentation.annotations.ApiIgnore;

@ApiIgnore
@RestController
@RequestMapping("/${application.simple-name:}")
public class DataController {

	private Logger logger = LoggerFactory.getLogger(DataController.class);

	@Autowired
	private IDataService dataService;

	@PostMapping("/{table_name}/save")
	public HttpResponse save(@PathVariable(name = "table_name") String tableName,
			@RequestBody Map<String, Object> dataMap) throws Exception {
		logger.debug("tableName.{}, dataMap = {}", tableName, dataMap);
		return HttpResponse.success(dataService.save(tableName, dataMap));
	}

	@DeleteMapping("/{table_name}/{data_id}")
	public HttpResponse remove(@PathVariable(name = "table_name") String tableName,
			@PathVariable(name = "data_id") String dataId) throws Exception {
		logger.debug("tableName.{}, dataId = {}", tableName, dataId);
		return HttpResponse.success(dataService.remove(tableName, Maps.newMap("id", dataId)));
	}

	@PutMapping("/{table_name}/{data_id}")
	public HttpResponse edit(@PathVariable(name = "table_name") String tableName,
			@PathVariable(name = "data_id") String dataId, @RequestBody Map<String, Object> updateMap)
			throws Exception {
		logger.debug("tableName.{}, dataId = {}, requestMap = {}", tableName, dataId, updateMap);
		return HttpResponse.success(dataService.edit(tableName, updateMap, Maps.newMap("id", dataId)));
	}

	@GetMapping("/{table_name}/{data_id}")
	public HttpResponse getOne(@PathVariable(name = "table_name") String tableName,
			@PathVariable(name = "data_id") String dataId) throws Exception {
		logger.debug("tableName.{}, dataId = {}", tableName, dataId);
		return HttpResponse.success(dataService.getOne(tableName, Maps.newMap("id", dataId)));
	}

	@PostMapping("/{table_name}/list")
	public HttpResponse list(@PathVariable(name = "table_name") String tableName,
			@RequestBody(required = false) Map<String, Object> parameterMap) throws Exception {
		logger.debug("tableName.{}, parameterMap = {}", tableName, parameterMap);
		return HttpResponse.success(dataService.list(tableName, parameterMap));
	}

	@PostMapping("/{table_name}/page")
	public HttpResponse page(@PathVariable(name = "table_name") String tableName,
			@RequestBody(required = false) Page<Map<String, Object>> page) throws Exception {
		logger.debug("tableName.{}, page = {}", tableName, page);
		return HttpResponse.success(dataService.page(tableName, page));
	}
}
