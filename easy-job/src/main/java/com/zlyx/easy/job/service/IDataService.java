package com.zlyx.easy.job.service;

import java.util.Map;

import com.zlyx.easy.database.local.Page;

public interface IDataService {

	Object save(String tableName, Map<String, Object> dataMap) throws Exception;

	Object remove(String tableName, Map<String, Object> conditionMap) throws Exception;

	Object edit(String tableName, Map<String, Object> updateMap, Map<String, Object> conditionMap) throws Exception;

	Object getOne(String tableName, Map<String, Object> parameterMap) throws Exception;

	Object list(String tableName, Map<String, Object> parameterMap) throws Exception;

	Object page(String tableName, Page<Map<String, Object>> page) throws Exception;

}
