package com.zlyx.easy.job.service;

import com.zlyx.easy.core.sql.TableEntity;

public interface ITableService {

	Object create(TableEntity te) throws Exception;

}
