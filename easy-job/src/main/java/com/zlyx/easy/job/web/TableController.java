package com.zlyx.easy.job.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.zlyx.easy.core.http.HttpResponse;
import com.zlyx.easy.core.sql.TableEntity;
import com.zlyx.easy.job.service.ITableService;

import springfox.documentation.annotations.ApiIgnore;

@ApiIgnore
@RestController
@RequestMapping("/${application.simple-name:}")
public class TableController {

	private Logger logger = LoggerFactory.getLogger(TableController.class);

	@Autowired
	private ITableService tableService;

	@PostMapping("/table/create")
	public HttpResponse save(@RequestBody TableEntity te) throws Exception {
		logger.debug("tableEntity.{}", te);
		if (te.getTableName() == null) {
			throw new Exception("表名称不能为空");
		}
		if (te.getColumns() == null) {
			throw new Exception("表字段不能为空");
		}
		return HttpResponse.success(tableService.create(te));
	}
}
