//package com.zlyx.easy.elasticsearch.service;
//
//import java.util.Iterator;
//import java.util.List;
//
//import org.springframework.data.domain.Page;
//
//import com.zlyx.easy.elasticsearch.entity.DocBean;
//
//public interface IElasticService {
//
//	void save(DocBean docBean);
//
//	void saveAll(List<DocBean> list);
//
//	Iterator<DocBean> findAll();
//
//	Page<DocBean> findByContent(String content);
//
//	Page<DocBean> findByFirstCode(String firstCode);
//
//	Page<DocBean> findBySecordCode(String secordCode);
//
//	Page<DocBean> query(String key);
//}
