package com.zlyx.easy.elasticsearch;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import de.codecentric.boot.admin.server.config.EnableAdminServer;

/**
 * 
 * @Auth 赵光
 * @Describle
 * @2019年12月30日
 */
@EnableAutoConfiguration
@EnableAdminServer
@SpringBootApplication
public class StartApp {

	public static void main(String[] args) throws Exception {
		SpringApplication.run(StartApp.class, args);
	}

}
