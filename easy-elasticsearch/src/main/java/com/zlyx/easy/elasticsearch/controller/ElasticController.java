//package com.zlyx.easy.elasticsearch.controller;
//
//import java.util.Iterator;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//
//import com.zlyx.easy.elasticsearch.entity.DocBean;
//import com.zlyx.easy.elasticsearch.service.IElasticService;
//
//@RestController
//@RequestMapping("/elastic")
//public class ElasticController {
//
//	@Autowired
//	private IElasticService elasticService;
//
//	@GetMapping("/all")
//	public Iterator<DocBean> all() {
//		return elasticService.findAll();
//	}
//
//}
