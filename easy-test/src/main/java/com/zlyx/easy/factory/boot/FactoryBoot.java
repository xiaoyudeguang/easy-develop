package com.zlyx.easy.factory.boot;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import com.zlyx.easy.core.refresh.IHandlerOnRefreshed;
import com.zlyx.easy.factory.CallTest;

/**
 * @Auth 赵光
 * @Describle
 * @2019年6月18日 下午9:10:43
 */
@Component
public class FactoryBoot implements IHandlerOnRefreshed {

	@Autowired
	private CallTest test;

	@Override
	public void doOnRefreshed(ApplicationContext context) {
		test.test("默认工厂模式", 15);
	}
}
