package com.zlyx.easy.factory;

import java.lang.reflect.Method;

import org.springframework.stereotype.Component;

import com.zlyx.easy.core.factory.interfaces.FactoryBeanDefiner;
import com.zlyx.easy.core.loggers.Logger;
import com.zlyx.easy.log.annotations.EasyLog;

/**
 * @Auth 赵光
 * @Describle
 * @2019年6月18日 下午9:07:38
 */
@Component
public class TestHandler implements FactoryBeanDefiner {

	@Override
	@EasyLog(todo = { "测试" })
	public Object excute(Class<?> proxyClass, Method method, Object[] args) {
		Logger.info(args);
		return "success";
	}
}
