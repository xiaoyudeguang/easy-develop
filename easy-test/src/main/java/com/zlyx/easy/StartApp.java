package com.zlyx.easy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 
 * @Auth 赵光
 * @Describle
 * @2019年12月30日
 */
@SpringBootApplication
public class StartApp {

	public static void main(String[] args) throws Exception {
		SpringApplication.run(StartApp.class, args);
//		ClassMocker.mockAll();
	}

}
