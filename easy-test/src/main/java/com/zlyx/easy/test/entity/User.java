package com.zlyx.easy.test.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import javax.validation.constraints.NotNull;

/**
 * 用户表
 * @author 赵光
 * @since 2020-05-23
 */
@TableName("`user`")
public class User implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 用户id
     */
    @TableId("`id`")
    private Integer id;

    /**
     * 用户姓名
     */
    @NotNull  
    @TableField("`name`")
    private String name;

    /**
     * 用户年龄
     */
    @NotNull  
    @TableField("`age`")
    private Integer age;

    @NotNull  
    @TableField("`user_job`")
    private String userJob;

    @NotNull  
    @TableField("`t_name`")
    private String tName;

    @NotNull  
    @TableField("`address`")
    private String address;

    public Integer getId() {
        return id;
    }

    public User setId(Integer id) {
        this.id = id;
        return this;
    }
    public String getName() {
        return name;
    }

    public User setName(String name) {
        this.name = name;
        return this;
    }
    public Integer getAge() {
        return age;
    }

    public User setAge(Integer age) {
        this.age = age;
        return this;
    }
    public String getUserJob() {
        return userJob;
    }

    public User setUserJob(String userJob) {
        this.userJob = userJob;
        return this;
    }
    public String gettName() {
        return tName;
    }

    public User settName(String tName) {
        this.tName = tName;
        return this;
    }
    public String getAddress() {
        return address;
    }

    public User setAddress(String address) {
        this.address = address;
        return this;
    }

    @Override
    public String toString() {
        return "User{" +
        "id=" + id +
        ", name=" + name +
        ", age=" + age +
        ", userJob=" + userJob +
        ", tName=" + tName +
        ", address=" + address +
        "}";
    }
}
