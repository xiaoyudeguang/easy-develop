package com.zlyx.easy.test.service;

import com.zlyx.easy.test.entity.User;
import com.zlyx.easy.test.mapper.UserMapper;
import com.zlyx.easy.web.web.service.AbstractService;

/**
 * 用户表 服务类
 * @author 赵光
 * @Date 2020-05-23
 */
public interface UserService extends AbstractService<UserMapper,User> {

}
