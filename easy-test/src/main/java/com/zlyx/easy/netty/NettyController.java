package com.zlyx.easy.netty;

import com.zlyx.easy.core.http.HttpResponse;
import com.zlyx.easy.netty.client.NettyUtils;
import com.zlyx.easy.swagger.annotations.SpringController;
import com.zlyx.easy.swagger.annotations.SpringMapping;

/**
 * @Auth 赵光
 * @Describle
 * @2019年2月12日 下午4:21:40
 */
@SpringController(todo = { "netty客户端测试" }, value = { "netty" })
public class NettyController {

	@SpringMapping(todo = { "发送消息" }, value = { "sendMsg" })
	public HttpResponse sendMsg(String msg) throws InterruptedException {
		return HttpResponse.success(NettyUtils.getInstance("127.0.0.1", 9000).sendMsg(msg, 300));
	}
}
