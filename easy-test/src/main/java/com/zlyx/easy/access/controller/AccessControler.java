package com.zlyx.easy.access.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.zlyx.easy.access.accesser.TestAccesser;
import com.zlyx.easy.access.domain.User;
import com.zlyx.easy.log.annotations.EasyLog;

/**
 * @Auth 赵光
 * @Describle
 * @2019年12月31日
 */
@EasyLog(todo = { "Access框架" })
@RestController
@RequestMapping(value = { "/easy/access" })
public class AccessControler {

	@Autowired
	private TestAccesser testAccesser;

	@EasyLog(value = "/findList", todo = { "Access框架查询列表" })
	@GetMapping("/findList")
	public List<User> findList(String id) {
		return testAccesser.findList();
	}

	@GetMapping("/list")
	public List<User> listUserModel(String id, String name) {
		return testAccesser.listUserModel(id, name);
	}
}
