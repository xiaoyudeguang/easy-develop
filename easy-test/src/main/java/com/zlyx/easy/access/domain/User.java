package com.zlyx.easy.access.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;

import com.baomidou.mybatisplus.annotation.TableName;

/**
 * @Auth 赵光
 * @Desc 描述
 * @Date 2019年7月27日
 */
@TableName("user")
@Entity(name = "user")
public class User implements Serializable {

	private static final long serialVersionUID = 1L;

	@Column
	private Integer id;

	private String name;

	private Integer age;

	public User() {
	}

	public User(String name) {
		this.name = name;
	}

	public User(Integer age) {
		this.age = age;
	}

	public User(String name, Integer age) {
		this.name = name;
		this.age = age;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public static User create(String name, int age) {
		return new User(name, age);
	}

}
