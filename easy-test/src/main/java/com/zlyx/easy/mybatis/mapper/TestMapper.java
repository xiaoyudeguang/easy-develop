package com.zlyx.easy.mybatis.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.zlyx.easy.access.domain.User;
import com.zlyx.easy.database.annotations.EasySelect;
import com.zlyx.easy.mybatis.annotations.EasyMapper;
import com.zlyx.easy.web.web.mybatis.AbstractMapper;

@EasyMapper(todo = "")
public interface TestMapper extends AbstractMapper<User> {

	@Select("select * from user where id in (${id})")
	public List<User> findlist(@Param("id") String id);

	@EasySelect(tableCls = User.class, results = { "id", "name" }, where = {
			"id in (${id}) and name like '%{name}%'" })
	public List<User> listUserModel(String id, String name);

	@EasySelect(tableCls = User.class, results = { "id", "name" }, where = {
			"id in (${id}) and name like '%{name}%'" }, page = true)
	public List<User> listUserModel(String id, String name, Integer pageNum, int pageSize);

}
