package com.zlyx.easy.socket;

import com.zlyx.easy.core.loggers.Logger;
import com.zlyx.easy.socket.annotations.SocketHandler;
import com.zlyx.easy.socket.annotations.SocketHandler.MsgType;
import com.zlyx.easy.socket.interfaces.IMsgHandler;

/**
 * @Auth 赵光
 * @Describle
 * @2018年12月22日 下午5:24:41
 */
@SocketHandler(type = MsgType.ATM, port = 8002, todo = { "AIO_TCP使用案例" })
public class AIO_TCPDemo implements IMsgHandler<String> {

	@Override
	public String doHandle(String data) throws Exception {
		Logger.info("接收到消息 -> " + data);
		return new String(this.hashCode() + ":" + data);
	}
}
