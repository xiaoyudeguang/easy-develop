package com.zlyx.easy.drools;

import com.zlyx.easy.drools.builder.RuleBuilder;
import com.zlyx.easy.drools.executor.RuleExecutor;
import com.zlyx.easy.drools.validate.RuleValidator;
import com.zlyx.easy.drools.domain.RuleContextDomain;
import java.util.Arrays;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *  @prop message string 结束标记
 */ 
public class Rule extends RuleExecutor<RuleContextDomain> {

	/**
	 * rule
	 *
	 * @param context context
	 *
	 * @throws Exception
	 */
	public void rule(RuleContextDomain context) {
		Logger log = LoggerFactory.getLogger("$rule_name");
	try {
		List<String> list = Arrays.asList("a", "b");
		for (int i = 0; i < 6; i++) {
			System.out.println("第" + (i + 1) + "次输出 ->" + list.get(i % 2));
		}
	} catch (Exception e) {
		log.error(e.getMessage(), e);
	}
	log.info("{}", "$message");
	}
	
	@Override
	public RuleContextDomain initContext() throws Exception {
		return new RuleContextDomain();
	}

	public static void main(String[] args) throws Exception {
		RuleBuilder.RuleBody ruleBody = RuleBuilder.newBuilder("测试测试");
		ruleBody.when("$ctx: RuleContextDomain();"); // 规则条件
		ruleBody.then(Rule .class, "rule"); // 规则内容
		//导入包
		ruleBody.importClass(RuleContextDomain .class);
		ruleBody.importClass(Logger.class);
		ruleBody.importClass(LoggerFactory.class);
		//设置属性
		ruleBody.props("message", "执行完成");
		RuleValidator.validate(ruleBody, new Rule());
	}


}