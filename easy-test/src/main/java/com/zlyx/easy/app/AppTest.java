package com.zlyx.easy.app;

import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import com.zlyx.easy.core.utils.EnvUtils;

/**
 * @Auth 赵光
 * @Describle
 * @2020年1月12日
 */
@Component
public class AppTest implements CommandLineRunner {

	@Override
	public void run(String... args) throws Exception {
		System.out.println(EnvUtils.getProperty("server.port"));
	}
}
