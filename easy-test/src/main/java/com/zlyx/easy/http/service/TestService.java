package com.zlyx.easy.http.service;

import java.util.List;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.zlyx.easy.core.model.UserModel;
import com.zlyx.easy.http.annotations.HttpMethod;
import com.zlyx.easy.http.annotations.HttpProvider;
import com.zlyx.easy.http.annotations.HttpService;

@HttpService(url = "${http.service.url:http://localhost:8080}", providerClass = TestProvider.class)
public interface TestService {

	@PostMapping(value = "/version/{version}/appId/{appId}/test/doService")
	public List<UserModel> testPost(String appId, double version, String name, int age);

	@GetMapping(value = "version/{version}/appId/{appId}/test/doService?name={name}&age={age}")
	public List<UserModel> testGet(String appId, double version, String name, int age);

	@PutMapping(value = "version/{version}/appId/{appId}/test/doService")
	public List<UserModel> testPut(String appId, double version, String name, int age);

	@DeleteMapping(value = "version/{version}/appId/{appId}/test/doService?name={name}&age={age}")
	public List<UserModel> testDelete(String appId, double version, String name, int age);

	@RequestMapping(value = "version/{version}/appId/{appId}/test/doService")
	public List<UserModel> testRequest(String appId, double version, String name, int age);

	@GetMapping(value = "version/{version}/appId/:appId}/test/doService?name={name}&age={age}")
	public void testNoReturn(String appId, double version, String name, int age);

	@PostMapping(value = "version/{version}/appId/{appId}/test/testPayload", consumes = {
			MediaType.APPLICATION_JSON_UTF8_VALUE })
	public List<UserModel> testPayload(String appId, double version, UserModel model);

	@HttpProvider(providerMethod = "testFormParams")
	@PostMapping(value = "version/{version/appId/{appId}/test/testForm")
	public List<UserModel> testForm(String appId, double version, UserModel model);

	@HttpMethod(value = "version/{version/appId/{appId}/test/testForm", method = RequestMethod.POST, providerMethod = "testForm")
	public List<UserModel> testHttpMethod(String appId, double version, UserModel model);

}
