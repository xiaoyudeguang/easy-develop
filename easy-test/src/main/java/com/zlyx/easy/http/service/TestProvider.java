/**
 *
 * @Author 赵光
 * @Desc TestProvider
 * @Date 2020年6月6日
 */
package com.zlyx.easy.http.service;

import java.util.HashMap;
import java.util.Map;

import com.zlyx.easy.core.model.UserModel;
import com.zlyx.easy.core.utils.JsonUtils;

/**
 *
 * @Author 赵光
 * @Desc TestProvider
 * @Date 2020年6月6日
 */
public class TestProvider {

	public String testForm(String appId, double version, UserModel model) {
		Map<String, Object> paramsMap = new HashMap<String, Object>();
		paramsMap.put("appId", appId);
		paramsMap.put("version", version);
		paramsMap.put("model", model);
		return JsonUtils.toJson(paramsMap);
	}
}
