package com.zlyx.easy.http.controller;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.zlyx.easy.core.http.HttpUtils.HttpClient;
import com.zlyx.easy.core.loggers.Logger;
import com.zlyx.easy.core.model.UserModel;
import com.zlyx.easy.core.utils.RequestUtils;
import com.zlyx.easy.http.headers.Headers;
import com.zlyx.easy.http.service.TestService;
import com.zlyx.easy.log.annotations.EasyLog;

@EasyLog(todo = { "" })
@RestController
@RequestMapping("version/{version}")
public class TestController {

	@Autowired
	private TestService testService;

	@RequestMapping("appId/{appId}/test/doService")
	public List<UserModel> doService(String name, int age) {
		Logger.info("name = {}, age = {}", name, age);
		return Arrays.asList(new UserModel(name), new UserModel(age));
	}

	@PostMapping(value = "appId/{appId}/test/testPayload")
	public List<UserModel> testPayload(@RequestBody String body) {
		Logger.info("model = {},", body);
		return Arrays.asList(new UserModel("testPayload"), new UserModel(18));
	}

	@PostMapping(value = "appId/{appId}/test/testForm")
	public List<UserModel> testForm(@RequestParam UserModel model) {
		Logger.info("params = {}", RequestUtils.getParamsMap());
		return Arrays.asList(new UserModel("testForm"), new UserModel(18));
	}

	@GetMapping("appId/{appId}/test/testGet")
	public String testGet(String name, int age) throws Exception {
		Logger.info("params = {}", RequestUtils.getParamsMap());
		return HttpClient
				.url("http://localhost:8080/version/1.0/appId/123456/test/doService?name={name}&age={age}", name, age)
				.getString();
	}

	@PostMapping("appId/{appId}/test/testPost")
	public String testPost(String name, int age) throws Exception {
		Logger.info("params = {}", RequestUtils.getParamsMap());
		return HttpClient
				.url("http://localhost:8080/version/1.0/appId/123456/test/doService", RequestUtils.getParameterMap())
				.postString();
	}

	@GetMapping("appId/{appId}/test/testHttpClient")
	public String testHttpClient(String name, int age) throws Exception {
		Logger.info("params = {}", RequestUtils.getParamsMap());
		return HttpClient.url("http://localhost:8080/version/1.0/appId/123456/test/testPayload")
				.headers(Headers.APPLICATION_JSON_UTF8_HEADERS).param("name", name).param("age", age)
				.post(String.class);
	}

	@GetMapping("appId/{appId}/test/testHttpService")
	public List<UserModel> testHttpService(String name, int age) throws Exception {
		return testService.testPost("111", 111, name, age);
	}

}
