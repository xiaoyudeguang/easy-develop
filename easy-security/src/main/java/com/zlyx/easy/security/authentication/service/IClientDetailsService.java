package com.zlyx.easy.security.authentication.service;

import org.springframework.security.oauth2.provider.ClientDetailsService;

/**
 * <p>
 * 客户端信息查询接口
 * </p>
 *
 * @author 赵光
 * @version v1.0
 */
public interface IClientDetailsService extends ClientDetailsService {

}
