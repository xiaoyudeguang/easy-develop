package com.zlyx.easy.security.authentication.config;

import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

import com.zlyx.easy.security.authentication.handler.DefaultAuthenticationFailureHandler;
import com.zlyx.easy.security.authentication.handler.DefaultAuthenticationSuccessHandler;

/**
 * <p>
 * 权限验证Bean配置
 * </p>
 *
 * @author 赵光
 * @version v1.0
 */
@Configuration
public class AuthorizationBeanConfig {

	@Bean
	@ConditionalOnMissingBean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}

	@Bean
	@ConditionalOnMissingBean
	public AuthenticationFailureHandler configAuthenticationFailureHandler() {
		return new DefaultAuthenticationFailureHandler();
	}

	@Bean
	@ConditionalOnMissingBean
	public AuthenticationSuccessHandler configAuthenticationSuccessHandler() {
		return new DefaultAuthenticationSuccessHandler();
	}

}
