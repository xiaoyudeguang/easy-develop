package com.zlyx.easy.security.authentication.handler;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;

import com.zlyx.easy.core.utils.ResponseUtils;

/**
 * <p>
 * 登录失败回调
 * </p>
 *
 * @author zhaog
 * @version v1.0
 */
public class DefaultAuthenticationFailureHandler implements AuthenticationFailureHandler {

	protected Logger logger = LoggerFactory.getLogger(AuthenticationFailureHandler.class);

	@Override
	public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response,
			AuthenticationException e) throws IOException, ServletException {
		write(response, e);
	}

	/**
	 * 返回
	 * 
	 * @param response          响应
	 * @param oAuth2AccessToken token
	 * @throws IOException
	 */
	public void write(HttpServletResponse response, AuthenticationException e) throws IOException {
		logger.error(e.getMessage(), e);
		ResponseUtils.write(response, e.getMessage());
	}
}
