package com.zlyx.easy.security.tokenstore.jdbc.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.springframework.context.annotation.Import;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;

import com.zlyx.easy.security.tokenstore.jdbc.config.JdbcTokenStoreConfig;

/**
 * <p>
 * 启用Jdbc作为TokenStore
 * </p>
 *
 * @author 赵光
 * @version v1.0
 */
@Documented
@Inherited
@EnableResourceServer
@Target({ ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
@Import({ JdbcTokenStoreConfig.class })
public @interface EnableJdbcTokenStore {
}
