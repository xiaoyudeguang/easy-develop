package com.zlyx.easy.security.defaults.annoations;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.springframework.stereotype.Component;

/**
 * <p>
 * 标注授权类型
 * </p>
 *
 * @author 赵光
 * @version v1.0
 */
@Documented
@Component
@Target({ ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
public @interface AuthorizationServer {

	/**
	 * 授权类型
	 * 
	 * @return
	 */
	String[] value();
}
