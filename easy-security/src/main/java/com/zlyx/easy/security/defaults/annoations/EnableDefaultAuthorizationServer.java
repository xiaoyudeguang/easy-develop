package com.zlyx.easy.security.defaults.annoations;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.springframework.context.annotation.Import;

import com.zlyx.easy.security.defaults.config.DefaultAuthorizationConfig;

/**
 * <p>
 * 启用Redis作为TokenStore
 * </p>
 *
 * @author 赵光
 * @version v1.0
 */
@Documented
@Inherited
@Target({ ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
@Import({ DefaultAuthorizationConfig.class, AuthorizationConfigRegistrar.class })
public @interface EnableDefaultAuthorizationServer {

	/**
	 * 支持的授权类型
	 */
	public static final String DEFAULT_GRANT_TYPE = "login";

	/**
	 * grantType
	 * 
	 * @return
	 */
	String value() default DEFAULT_GRANT_TYPE;
}
