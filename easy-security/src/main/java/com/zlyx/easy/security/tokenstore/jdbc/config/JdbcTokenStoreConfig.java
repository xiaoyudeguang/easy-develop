package com.zlyx.easy.security.tokenstore.jdbc.config;

import javax.sql.DataSource;

import org.springframework.context.annotation.Bean;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JdbcTokenStore;

/**
 * <p>
 * 基于Jdbc作为TokenStore配置
 * </p>
 *
 * @author 赵光
 * @version v1.0
 */
public class JdbcTokenStoreConfig {

	@Bean
	public TokenStore tokenStore(DataSource dataSource) {
		return new JdbcTokenStore(dataSource);
	}
}
