package com.zlyx.easy.security.defaults.annoations;

import java.util.Map;

import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.context.annotation.ImportBeanDefinitionRegistrar;
import org.springframework.core.type.AnnotationMetadata;
import org.springframework.security.oauth2.common.util.OAuth2Utils;

/**
 * <p>
 * 授权类型解析
 * </p>
 *
 * @author 赵光
 * @version v1.0
 */
public class AuthorizationConfigRegistrar implements ImportBeanDefinitionRegistrar {

	@Override
	public void registerBeanDefinitions(AnnotationMetadata importingClassMetadata, BeanDefinitionRegistry registry) {
		Map<String, Object> annotationAttributes = importingClassMetadata
				.getAnnotationAttributes(EnableDefaultAuthorizationServer.class.getName());
		System.setProperty(OAuth2Utils.GRANT_TYPE, (String) annotationAttributes.get("value"));
	}

}
