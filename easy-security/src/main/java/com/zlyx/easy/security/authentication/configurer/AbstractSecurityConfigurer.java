package com.zlyx.easy.security.authentication.configurer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.SecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.web.DefaultSecurityFilterChain;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.zlyx.easy.core.exception.BusinessException;
import com.zlyx.easy.security.defaults.filter.DefaultAuthenticationFilter;

/**
 * <p>
 * 抽象授权配置类
 * </p>
 *
 * @author 赵光
 * @version v1.0
 */
public abstract class AbstractSecurityConfigurer
		extends SecurityConfigurerAdapter<DefaultSecurityFilterChain, HttpSecurity> {

	protected Logger logger = LoggerFactory.getLogger(AbstractSecurityConfigurer.class);

	/**
	 * 授权过滤器
	 */
	protected AbstractAuthenticationProcessingFilter authenticationFilter;

	/**
	 * 授权校验器
	 */
	protected AuthenticationProvider authenticationProvider;

	@Autowired
	protected AuthenticationSuccessHandler authenticationLoginSuccessHandler;

	@Autowired
	protected AuthenticationFailureHandler authenticationLoginFailureHandler;

	public AbstractSecurityConfigurer(AuthenticationProvider authenticationProvider) {
		this(new DefaultAuthenticationFilter(), authenticationProvider);
	}

	public AbstractSecurityConfigurer(AbstractAuthenticationProcessingFilter authenticationFilter,
			AuthenticationProvider authenticationProvider) {
		this.authenticationFilter = authenticationFilter;
		this.authenticationProvider = authenticationProvider;
	}

	@Override
	public void configure(HttpSecurity http) throws Exception {
		if (authenticationFilter == null) {
			throw new BusinessException("AbstractAuthenticationProcessingFilter can not be null");
		}
		if (authenticationProvider == null) {
			throw new BusinessException("AuthenticationProvider can not be null");
		}
		if (authenticationLoginSuccessHandler == null) {
			throw new BusinessException("AuthenticationSuccessHandler can not be null");
		}
		authenticationFilter.setAuthenticationManager(http.getSharedObject(AuthenticationManager.class));
		authenticationFilter.setAuthenticationSuccessHandler(authenticationLoginSuccessHandler);
		authenticationFilter.setAuthenticationFailureHandler(authenticationLoginFailureHandler);
		http.authenticationProvider(authenticationProvider).addFilterAfter(authenticationFilter,
				UsernamePasswordAuthenticationFilter.class);
	}

}
