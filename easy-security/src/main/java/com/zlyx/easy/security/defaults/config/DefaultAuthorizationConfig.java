package com.zlyx.easy.security.defaults.config;

import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;

import com.zlyx.easy.security.authentication.configurer.AbstractSecurityConfigurer;
import com.zlyx.easy.security.authentication.service.IClientDetailsService;
import com.zlyx.easy.security.authentication.service.IUserDetailsService;
import com.zlyx.easy.security.defaults.provider.DefaultAuthenticationProvider;
import com.zlyx.easy.security.defaults.service.DefaultClientDetailsService;
import com.zlyx.easy.security.defaults.service.DefaultUserDetailsService;

/**
 * <p>
 * 默认授权配置
 * </p>
 *
 * @author 赵光
 * @version v1.0
 */
public class DefaultAuthorizationConfig {

	@Bean
	@ConditionalOnMissingBean
	public IUserDetailsService defaultUserDetailsService() {
		return new DefaultUserDetailsService();
	}

	@Bean
	@ConditionalOnMissingBean
	public IClientDetailsService defaultClientDetailsService() {
		return new DefaultClientDetailsService();
	}

	@Bean
	public DefaultAuthenticationProvider defaultAuthenticationProvider() {
		return new DefaultAuthenticationProvider();
	}

	@Bean
	public AbstractSecurityConfigurer defaultSecurityConfigurer(
			DefaultAuthenticationProvider defaultAuthenticationProvider) {
		return new AbstractSecurityConfigurer(defaultAuthenticationProvider) {
		};
	}

}
