package com.zlyx.easy.security.tokenstore.redis.annotation;

import java.util.Map;

import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.context.annotation.ImportBeanDefinitionRegistrar;
import org.springframework.core.type.AnnotationMetadata;

import com.zlyx.easy.security.tokenstore.redis.config.RedisTokenStoreConfig;

/**
 * <p>
 * 基于Redis作为TokenStore配置
 * </p>
 *
 * @author 赵光
 * @version v1.0
 */
public class RedisTokenStoreRegistrar implements ImportBeanDefinitionRegistrar {

	@Override
	public void registerBeanDefinitions(AnnotationMetadata importingClassMetadata, BeanDefinitionRegistry registry) {
		Map<String, Object> annotationAttributes = importingClassMetadata
				.getAnnotationAttributes(EnableRedisTokenStore.class.getName());
		System.setProperty(RedisTokenStoreConfig.REDIS_TOKEN_PREFIX, (String) annotationAttributes.get("value"));
	}

}