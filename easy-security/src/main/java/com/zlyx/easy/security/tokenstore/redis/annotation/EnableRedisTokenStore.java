package com.zlyx.easy.security.tokenstore.redis.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.springframework.context.annotation.Import;

import com.zlyx.easy.security.tokenstore.redis.config.RedisTokenStoreConfig;

/**
 * <p>
 * 启用Redis作为TokenStore
 * </p>
 *
 * @author 赵光
 * @version v1.0
 */
@Documented
@Inherited
@Target({ ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
@Import({ RedisTokenStoreRegistrar.class, RedisTokenStoreConfig.class })
public @interface EnableRedisTokenStore {

	/**
	 * tokenStrore前缀
	 * 
	 * @return
	 */
	String value() default "";
}
