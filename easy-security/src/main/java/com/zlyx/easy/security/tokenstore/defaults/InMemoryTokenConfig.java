package com.zlyx.easy.security.tokenstore.defaults;

import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.InMemoryTokenStore;

/**
 * <p>
 * 基于内存作为TokenStore配置
 * </p>
 *
 * @author 赵光
 * @version v1.0
 * @since 2020/12/30
 */
@Configuration
public class InMemoryTokenConfig {

	@Bean
	@ConditionalOnMissingBean
	public TokenStore tokenStore() {
		return new InMemoryTokenStore();
	}
}
