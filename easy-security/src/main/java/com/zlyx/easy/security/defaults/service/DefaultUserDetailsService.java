package com.zlyx.easy.security.defaults.service;

import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.zlyx.easy.security.authentication.service.IUserDetailsService;

/**
 * <p>
 * 默认用户查询接口实现
 * </p>
 *
 * @author 赵光
 * @version v1.0
 */
public class DefaultUserDetailsService implements IUserDetailsService {

	protected static final Logger logger = LoggerFactory.getLogger(IUserDetailsService.class);

	protected static final PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		return new User(username, passwordEncoder.encode(username), new ArrayList<>());
	}

}
