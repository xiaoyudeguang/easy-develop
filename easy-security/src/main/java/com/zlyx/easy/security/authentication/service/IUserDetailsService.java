package com.zlyx.easy.security.authentication.service;

import org.springframework.security.core.userdetails.UserDetailsService;

/**
 * <p>
 * 用户信息查询接口
 * </p>
 *
 * @author 赵光
 * @version v1.0
 */
public interface IUserDetailsService extends UserDetailsService {

}
