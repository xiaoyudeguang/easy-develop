package com.zlyx.easy.netty.server.annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.springframework.stereotype.Component;

/**
 * @Auth 赵光
 * @Describle
 * @2019年1月25日 下午1:55:44
 */
@Documented
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Component
public @interface Server {

	/**
	 * 端口
	 * @return
	 */
	int port();
	
	/**
	 * 功能描述
	 * @return
	 */
	String todo();
	
	/**
	 * 是否开启
	 * @return
	 */
	boolean isOpen() default true;
}
