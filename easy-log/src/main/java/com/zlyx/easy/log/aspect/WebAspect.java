package com.zlyx.easy.log.aspect;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

import com.zlyx.easy.core.utils.RequestUtils;
import com.zlyx.easy.log.annotations.EasyLog;
import com.zlyx.easy.log.aspect.abstracts.AbstractLogAspect;

@Aspect
@Component
public class WebAspect extends AbstractLogAspect {

	@Pointcut("@annotation(org.springframework.web.bind.annotation.RequestMapping) ||"
			+ "@annotation(org.springframework.web.bind.annotation.PostMapping) ||"
			+ "@annotation(org.springframework.web.bind.annotation.PutMapping) ||"
			+ "@annotation(org.springframework.web.bind.annotation.DeleteMapping) ||"
			+ "@annotation(org.springframework.web.bind.annotation.GetMapping)")
	public void web() {
	}

	@Around("web()")
	public Object doAround(ProceedingJoinPoint pjp) throws Throwable {
		HttpServletRequest request = RequestUtils.getRequest();
		if (request == null) {
			logger.debug("request is null");
			return pjp.proceed();
		}
		super.init(pjp);
		// 如果EasyLog注解存在，那么优先执行EasyLog注解切面
		if (realMethod.isAnnotationPresent(EasyLog.class)) {
			return pjp.proceed();
		}
		if (realClass.isInterface()) {
			logger.debug("interface is ignore");
			return pjp.proceed();
		}
		return doAround(pjp, request.getRequestURI(), realMethod.getName());
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		if (excludeUrls == null) {
			excludeUrls = new ArrayList<>();
		}
		excludeUrls.add("/error");
		excludeUrls.add("/swagger-resources");
	}
}
