package com.zlyx.easy.log.annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @Auth 赵光
 * @Describe
 * @Date 2018年12月6日
 */
@Documented
@Target({ ElementType.METHOD, ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface EasyLog {

	/**
	 * 请求路径
	 * 
	 * @return
	 */
	String value() default "";

	/**
	 * 作用描述
	 * 
	 * @return
	 */
	String[] todo();

	/**
	 * 是否输出切面日志
	 */
	boolean isLog() default true;
}
