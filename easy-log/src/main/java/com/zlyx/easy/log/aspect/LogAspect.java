package com.zlyx.easy.log.aspect;

import java.util.ArrayList;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

import com.zlyx.easy.log.annotations.EasyLog;
import com.zlyx.easy.log.aspect.abstracts.AbstractLogAspect;

@Aspect
@Component
public class LogAspect extends AbstractLogAspect {

	@Pointcut("@annotation(com.zlyx.easy.log.annotations.EasyLog)")
	public void log() {
	}

	@Around("log()")
	public Object doAround(ProceedingJoinPoint pjp) throws Throwable {
		super.init(pjp);
		EasyLog easyLog = realMethod.getAnnotation(EasyLog.class);
		// 方法类级别关闭
		if (Boolean.FALSE == easyLog.isLog()) {
			return pjp.proceed();
		}
		return doAround(pjp, easyLog.value(), easyLog.todo());
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		if (excludeUrls == null) {
			excludeUrls = new ArrayList<>();
		}
		excludeUrls.add("/error");
		excludeUrls.add("/swagger-resources");
	}
}
