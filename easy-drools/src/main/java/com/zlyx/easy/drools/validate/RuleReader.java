package com.zlyx.easy.drools.validate;

import java.util.Map;

import com.zlyx.easy.core.app.AppUtils;
import com.zlyx.easy.core.map.Maps;
import com.zlyx.easy.core.utils.ClassUtils;
import com.zlyx.easy.core.utils.FileUtils;
import com.zlyx.easy.drools.builder.RuleBuilder;
import com.zlyx.easy.drools.domain.RuleContextDomain;
import com.zlyx.easy.drools.executor.ReadExecutor;
import com.zlyx.easy.drools.util.RuleUtils;

/**
 * 规则导入工具(规则转java)
 * 
 * @Author 赵光
 * @Desc DroolsTest
 * @Date 2020年5月11日
 */
public class RuleReader {

	public static final String DEFAULT_RULE_PATH = "rule/rule.drl";

	public static final String DEFAULT_RULE_NAME = "规则引擎测试";

	public static final String DEFAULT_PACKAGE_NAME = AppUtils.getMainPackagePath();

	public static final String DEFAULT_CLASS_NAME = "Rule";

	public static final String DEFAULT_METHOD_NAME = "rule";

	/**
	 * 导入规则并生成java文件
	 * 
	 * @return
	 * @throws Exception
	 */
	public static String toJava(String ruleName) throws Exception {
		return toJava(DEFAULT_RULE_PATH, DEFAULT_RULE_NAME);
	}

	/**
	 * 导入规则并生成java文件
	 * 
	 * @param rulePath     规则路径
	 * @param ruleImporter 规则导入执行器
	 * @return
	 * @throws Exception
	 */
	public static <T> String toJava(String ruleName, ReadExecutor<T> executor) throws Exception {
		return toJava(DEFAULT_RULE_PATH, ruleName, executor);
	}

	/**
	 * 导入规则并生成java文件
	 * 
	 * @param <T>
	 * @param rulePath 规则路径
	 * @param ruleName 规则名称
	 * @return
	 * @throws Exception
	 */
	public static <T> String toJava(String rulePath, String ruleName) throws Exception {
		return toJava(rulePath, ruleName, null);
	}

	/**
	 * 导入规则并生成java文件
	 * 
	 * @param <T>
	 * @param rulePath     规则路径
	 * @param ruleName     规则名称
	 * @param ruleImporter 规则导入执行器
	 * @return
	 * @throws Exception
	 */
	public static <T> String toJava(String rulePath, String ruleName, ReadExecutor<T> executor) throws Exception {
		return toJava(rulePath, ruleName, DEFAULT_PACKAGE_NAME, executor);
	}

	/**
	 * 导入规则并生成java文件
	 * 
	 * @param <T>
	 * @param rulePath     规则路径
	 * @param ruleName     规则名称
	 * @param packagePath  生成类包路径
	 * @param ruleImporter 规则导入执行器
	 * @return
	 * @throws Exception
	 */
	public static <T> String toJava(String rulePath, String ruleName, String packagePath, ReadExecutor<T> executor)
			throws Exception {
		return toJava(rulePath, ruleName, packagePath, DEFAULT_CLASS_NAME, executor);
	}

	/**
	 * 导入规则并生成java文件
	 * 
	 * @param <T>
	 * @param rulePath     规则路径
	 * @param ruleName     规则名称
	 * @param packagePath  生成类包路径
	 * @param className    生成类名
	 * @param ruleImporter 规则导入执行器
	 * @return
	 * @throws Exception
	 */
	public static <T> String toJava(String rulePath, String ruleName, String packagePath, String className,
			ReadExecutor<T> executor) throws Exception {
		return toJava(rulePath, ruleName, packagePath, className, DEFAULT_METHOD_NAME, executor);
	}

	/**
	 * 导入规则并生成java文件
	 * 
	 * @param <T>
	 * @param rulePath    规则路径
	 * @param ruleName    规则名称
	 * @param packagePath 生成类包路径
	 * @param className   生成类名
	 * @param methodName  生成方法名
	 * @param executor    规则导入执行器
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public static <T> String toJava(String rulePath, String ruleName, String packagePath, String className,
			String methodName, ReadExecutor<T> executor) throws Exception {
		RuleBuilder.RuleBody ruleBody = RuleUtils.readRule(rulePath);
		Map<String, String> props = Maps.newMap();
		props.put("class_template_path", "rule/rule.vm");
		props.put("package_name", packagePath);
		props.put("import_package", ruleBody.header());
		props.put("class_name", className);
		props.put("method_name", methodName);
		props.put("method_body", ruleBody.then().trim());
		String path = ClassUtils.getPath() + "\\src\\main\\java\\" + packagePath.replace(".", "\\");
		if (executor == null) {
			executor = (ReadExecutor<T>) new ReadExecutor<RuleContextDomain>() {
			};
		}
		String classTemplate = executor.renderTemplate(ruleBody, ruleName, props);
		return FileUtils.writeAsFile(path, className + ".java", classTemplate);
	}

}
