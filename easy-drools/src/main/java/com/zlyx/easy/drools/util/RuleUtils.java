package com.zlyx.easy.drools.util;

import com.zlyx.easy.core.utils.FileUtils;
import com.zlyx.easy.core.utils.StringUtils;
import com.zlyx.easy.drools.builder.RuleBuilder;
import com.zlyx.easy.drools.builder.RuleBuilder.RuleBody;

/**
 * 规则导入工具
 * 
 * @Author 赵光
 * @Desc RuleUtils
 * @Date 2020年5月30日
 */
public class RuleUtils {

	/**
	 * 导入规则
	 * 
	 * @param rulePath
	 * @return
	 * @throws Exception
	 */
	public static RuleBody readRule(String rulePath) throws Exception {
		String rule = FileUtils.readResource(rulePath);
		if (StringUtils.isBlank(rule)) {
			throw new Exception("规则脚本内容为空");
		}
		return parseRule(rule);
	}

	/**
	 * 解析规则
	 * 
	 * @param rule
	 * @return
	 * @throws Exception
	 */
	public static RuleBody parseRule(String rule) throws Exception {
		String packageName = "";
		rule = rule.trim();
		if (rule.contains("package ")) {
			rule = rule.substring(rule.indexOf("package "));
			packageName = rule.substring(0, rule.indexOf(";") + 1);
			rule = rule.substring(packageName.length());
		}
		String header = rule.substring(0, rule.indexOf("rule"));
		rule = rule.substring(header.length());
		rule = rule.substring("rule".length());
		String name = rule.substring(0, rule.indexOf("when"));
		rule = rule.substring(name.length() + "when".length());
		String when = rule.substring(0, rule.indexOf("then"));
		rule = rule.substring(when.length() + "then".length());
		String then = rule.substring(0, rule.indexOf("end"));
		return RuleBuilder.newBuilder(name, when, then).header(header).packageName(packageName);
	}

}
