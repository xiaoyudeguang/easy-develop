package com.zlyx.easy.swagger.aspect;

import java.util.ArrayList;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

import com.zlyx.easy.core.utils.RequestUtils;
import com.zlyx.easy.log.annotations.EasyLog;
import com.zlyx.easy.log.aspect.abstracts.AbstractLogAspect;
import com.zlyx.easy.swagger.annotations.SpringMapping;

@Aspect
@Component
public class SwaggerAspect extends AbstractLogAspect {

	@Pointcut("@annotation(com.zlyx.easy.swagger.annotations.SpringMapping)")
	public void swagger() {
	}

	@Around("swagger()")
	public Object doAround(ProceedingJoinPoint pjp) throws Throwable {
		super.init(pjp);
		// 如果EasyLog注解存在，那么优先执行EasyLog注解切面
		if (realMethod.isAnnotationPresent(EasyLog.class)) {
			return pjp.proceed();
		}
		String url = RequestUtils.getRequest().getRequestURI();
		SpringMapping springMapping = realMethod.getAnnotation(SpringMapping.class);
		return doAround(pjp, url, springMapping.todo());
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		if (excludeUrls == null) {
			excludeUrls = new ArrayList<>();
		}
		excludeUrls.add("/error");
		excludeUrls.add("/swagger-resources");
	}
}
