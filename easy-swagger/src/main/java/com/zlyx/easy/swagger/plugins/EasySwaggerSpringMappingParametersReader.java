package com.zlyx.easy.swagger.plugins;

import static springfox.documentation.schema.Types.isBaseType;
import static springfox.documentation.swagger.common.SwaggerPluginSupport.SWAGGER_PLUGIN_ORDER;
import static springfox.documentation.swagger.common.SwaggerPluginSupport.pluginDoesApply;
import static springfox.documentation.swagger.readers.parameter.Examples.examples;
import static springfox.documentation.swagger.schema.ApiModelProperties.allowableValueFromString;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import com.zlyx.easy.core.collections.Lists;
import com.zlyx.easy.core.utils.StringUtils;
import com.zlyx.easy.swagger.annotations.SpringMapping;

import io.swagger.annotations.ApiImplicitParam;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.AllowableValues;
import springfox.documentation.service.Parameter;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.OperationBuilderPlugin;
import springfox.documentation.spi.service.contexts.OperationContext;
import springfox.documentation.spring.web.DescriptionResolver;
import springfox.documentation.swagger.common.SwaggerPluginSupport;

@Component
@Order(SwaggerPluginSupport.SWAGGER_PLUGIN_ORDER + SwaggerPluginSupport.SWAGGER_PLUGIN_ORDER)
public class EasySwaggerSpringMappingParametersReader implements OperationBuilderPlugin {

	private final DescriptionResolver descriptions;

	@Autowired
	public EasySwaggerSpringMappingParametersReader(DescriptionResolver descriptions) {
		this.descriptions = descriptions;
	}

	@Override
	public void apply(OperationContext context) {
		context.operationBuilder().parameters(readParameters(context));
	}

	@Override
	public boolean supports(DocumentationType delimiter) {
		return pluginDoesApply(delimiter);
	}

	private List<Parameter> readParameters(OperationContext context) {
		List<Parameter> parameters = Lists.newList();
		Optional<SpringMapping> annotation = context.findAnnotation(SpringMapping.class);
		if (annotation.isPresent()) {
			for (ApiImplicitParam param : annotation.get().apiImplicitParams()) {
				parameters.add(implicitParameter(descriptions, param));
			}
		}
		return parameters;
	}

	private ModelRef maybeGetModelRef(ApiImplicitParam param) {
		String dataType = StringUtils.getOrDefault(param.dataType(), "string");
		AllowableValues allowableValues = null;
		if (isBaseType(dataType)) {
			allowableValues = allowableValueFromString(param.allowableValues());
		}
		if (param.allowMultiple()) {
			return new ModelRef("", new ModelRef(dataType, allowableValues));
		}
		return new ModelRef(dataType, allowableValues);
	}

	public Parameter implicitParameter(DescriptionResolver descriptions, ApiImplicitParam param) {
		ModelRef modelRef = maybeGetModelRef(param);
		return new ParameterBuilder().name(param.name()).description(descriptions.resolve(param.value()))
				.defaultValue(param.defaultValue()).required(param.required()).allowMultiple(param.allowMultiple())
				.modelRef(modelRef).allowableValues(allowableValueFromString(param.allowableValues()))
				.parameterType(StringUtils.emptyOfNull(param.paramType())).parameterAccess(param.access())
				.order(SWAGGER_PLUGIN_ORDER).scalarExample(param.example()).complexExamples(examples(param.examples()))
				.build();
	}

}
