package com.zlyx.easy.swagger.config;

import com.zlyx.easy.core.app.AppUtils;

/**
 * swagger配置
 *
 */
public class SwaggerProperties {

	public static SwaggerProperties DEFAULT = new SwaggerProperties();

	/**
	 * 文档分组名称
	 */
	private String groupName = "default";

	/**
	 * 文档分组路由
	 */
	private String groupPath = "/**";

	/**
	 * 文档分组路径
	 */
	private String groupPackage = AppUtils.getMainPackagePath();

	public SwaggerProperties() {
	}

	public SwaggerProperties(String groupName) {
		this.groupName = groupName;
	}

	public SwaggerProperties(String groupName, String groupPackage) {
		super();
		this.groupName = groupName;
		this.groupPackage = groupPackage;
	}

	public SwaggerProperties(String groupName, String groupPath, String groupPackage) {
		super();
		this.groupName = groupName;
		this.groupPath = groupPath;
		this.groupPackage = groupPackage;
	}

	public String getGroupName() {
		return groupName;
	}

	public SwaggerProperties setGroupName(String groupName) {
		this.groupName = groupName;
		return this;
	}

	public String getGroupPath() {
		return groupPath;
	}

	public void setGroupPath(String groupPath) {
		this.groupPath = groupPath;
	}

	public String getGroupPackage() {
		return groupPackage;
	}

	public void setGroupPackage(String groupPackage) {
		this.groupPackage = groupPackage;
	}

}