package com.zlyx.easy.swagger.plugins;

import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import springfox.documentation.annotations.ApiIgnore;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.schema.ModelPropertyBuilderPlugin;
import springfox.documentation.spi.schema.contexts.ModelPropertyContext;
import springfox.documentation.swagger.common.SwaggerPluginSupport;

@ApiIgnore
@Component
@Order(SwaggerPluginSupport.SWAGGER_PLUGIN_ORDER + 1)
public class EasySwaggerApiModelPropertyPropertyBuilder implements ModelPropertyBuilderPlugin {

	@Override
	public void apply(ModelPropertyContext context) {
		if (context.getBuilder().build().getDescription() == null) {
			context.getBuilder().description(context.getBuilder().build().getName());
		}
	}

	@Override
	public boolean supports(DocumentationType delimiter) {
		return SwaggerPluginSupport.pluginDoesApply(delimiter);
	}
}
