package com.zlyx.easy.swagger.annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.springframework.core.annotation.AliasFor;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ResponseHeader;

/**
 * SpringMapping
 * 
 * @Auth 赵光
 * @Describle
 * @2019年1月3日 下午2:50:01
 */
@Target({ ElementType.METHOD, ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
@Documented
@RequestMapping
public @interface SpringMapping {

	/**
	 * 名称
	 * 
	 * @return
	 */
	@AliasFor(value = "name", annotation = RequestMapping.class)
	String name() default "";

	/**
	 * 路径
	 * 
	 * @return
	 */
	@AliasFor(value = "value", annotation = RequestMapping.class)
	String[] value();

	/**
	 * The response type of the operation.
	 * 
	 * @return
	 */
	Class<?> response() default Void.class;

	/**
	 * responseContainer
	 * 
	 * @return
	 */
	String responseContainer() default "";

	/**
	 * ApiResponse
	 * 
	 * @return
	 */
	ApiResponse[] apiResponses() default {};

	/**
	 * ApiResponse
	 * code码(一次注册处处引用(需在SwaggerRegister接口实现类中通过registerReponseModels()方法注册))
	 * 
	 * @return
	 */
	int[] codes() default {};

	/**
	 * ApiImplicitParam
	 * name值(一次注册处处引用(需在SwaggerRegister接口实现类中通过registerParamterModels()方法注册))
	 * 
	 * @return
	 */
	String[] names() default {};

	/**
	 * ApiImplicitParam
	 * 
	 * @return
	 */
	ApiImplicitParam[] apiImplicitParams() default {};

	/**
	 * ResponseHeader
	 * 
	 * @return
	 */
	ResponseHeader[] responseHeaders() default {};

	/**
	 * 返回数据的类型以及编码
	 * 
	 * @return
	 */
	@AliasFor(value = "produces", annotation = RequestMapping.class)
	String[] produces() default {};

	/**
	 * 接收数据的类型以及编码
	 * 
	 * @return
	 */
	@AliasFor(value = "consumes", annotation = RequestMapping.class)
	String[] consumes() default {};

	/**
	 * 请求类型: GET, HEAD, POST, PUT, PATCH, DELETE, OPTIONS, TRACE
	 * 
	 * @return
	 */
	@AliasFor(value = "method", annotation = RequestMapping.class)
	RequestMethod[] method() default RequestMethod.POST;

	/**
	 * 作用描述
	 * 
	 * @return
	 */
	String[] todo();

	/**
	 * 作用描述
	 * 
	 * @return
	 */
	String notes() default "";

	/**
	 * 是否隐藏此接口
	 * 
	 * @return
	 */
	boolean hidden() default false;

	/**
	 * 是否输出切面日志
	 * 
	 * @return
	 */
	boolean isLog() default true;

}