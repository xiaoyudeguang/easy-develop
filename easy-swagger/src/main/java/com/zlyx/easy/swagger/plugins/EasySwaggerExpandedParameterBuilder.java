package com.zlyx.easy.swagger.plugins;

import java.util.Optional;

import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.ExpandedParameterBuilderPlugin;
import springfox.documentation.spi.service.contexts.ParameterExpansionContext;
import springfox.documentation.swagger.common.SwaggerPluginSupport;

@Component
@Order(SwaggerPluginSupport.SWAGGER_PLUGIN_ORDER)
public class EasySwaggerExpandedParameterBuilder implements ExpandedParameterBuilderPlugin {

	@Override
	public void apply(ParameterExpansionContext context) {
		Optional<ApiModelProperty> apiModelPropertyOptional = context.findAnnotation(ApiModelProperty.class);
		Optional<ApiParam> apiParamOptional = context.findAnnotation(ApiParam.class);
		if (!apiModelPropertyOptional.isPresent() && !apiParamOptional.isPresent()) {
			context.getParameterBuilder().description(context.getFieldName());
		}
	}

	@Override
	public boolean supports(DocumentationType delimiter) {
		return SwaggerPluginSupport.pluginDoesApply(delimiter);
	}
}
