package com.zlyx.easy.swagger.annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.springframework.core.annotation.AliasFor;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 赵光
 * @version 创建时间：2018年10月29日 上午8:55:45
 * @ClassName 类名称
 * @Description 类描述
 */
@Documented
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@RequestMapping
@RestController
@CrossOrigin("*")
public @interface SpringController {

	/**
	 * BeanName
	 * 
	 * @return
	 */
	@AliasFor(annotation = RestController.class, attribute = "value")
	String name() default "";

	/**
	 * 请求路径
	 * 
	 * @return
	 */
	@AliasFor(annotation = RequestMapping.class, attribute = "value")
	String[] value();

	/**
	 * 分组名称
	 * 
	 * @return
	 */
	String groupName() default "";

	/**
	 * 描述
	 * 
	 * @return
	 */
	String[] todo();

	/**
	 * 显示级别(在swagger中只显示大于application.peoperties文件中配置的level的接口)
	 * 
	 * @return
	 */
	int level() default 5;

}
