package com.zlyx.easy.core.map;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.zlyx.easy.core.collections.EasyList;
import com.zlyx.easy.core.collections.Lists;
import com.zlyx.easy.core.utils.JsonUtils;
import com.zlyx.easy.core.utils.ObjectUtils;

/**
 * <p>
 * HashMap增强
 * </p>
 *
 * @author 赵光
 * @since 2020年1月10日
 */
public class EasyMap<K, V> extends HashMap<K, V> {

	private static final long serialVersionUID = 1L;

	public EasyMap() {
	}

	public EasyMap(Map<K, V> map) {
		super(map);
	}

	/**
	 * addValue
	 * 
	 * @param key
	 * @param value
	 * @return
	 */
	public EasyMap<K, V> addValue(K key, V value) {
		put(key, value);
		return this;
	}

	/**
	 * addValue
	 * 
	 * @param map
	 * @return
	 */
	public EasyMap<K, V> addValue(Map<K, V> map) {
		putAll(map);
		return this;
	}

	/**
	 * 
	 * @param key
	 * @param value
	 * @return
	 */
	public boolean putNoNull(K key, V value) {
		if (ObjectUtils.anyEmpty(key, value)) {
			return false;
		}
		this.put(key, value);
		return true;
	}

	/**
	 * 返回Integer
	 *
	 * @param key
	 * @return
	 */
	public Integer getInteger(String key) throws IOException {
		return get(key) == null ? null : (Integer) get(key);
	}

	/**
	 * 返回Long
	 *
	 * @param key
	 * @return
	 */
	public Long getLong(String key) throws IOException {
		return get(key) == null ? null : (Long) get(key);
	}

	/**
	 * 返回Double
	 *
	 * @param key
	 * @return
	 */
	public Double getDouble(String key) throws IOException {
		return get(key) == null ? null : (Double) get(key);
	}

	/**
	 * 返回字符串
	 *
	 * @param key
	 * @return
	 */
	public String getString(String key) throws IOException {
		return get(key) == null ? null : (String) get(key);
	}

	/**
	 * getMap
	 * 
	 * @param key
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public <M, N> EasyMap<M, N> getMap(String key) throws Exception {
		Object value = get(key);
		if (value == null) {
			return null;
		}
		if (Map.class.isAssignableFrom(value.getClass())) {
			return Maps.newMap((Map<M, N>) value);
		}
		return JsonUtils.toMap(JsonUtils.toJson(value));
	}

	/**
	 * getList
	 * 
	 * @param <T>
	 * @param key
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public <T> EasyList<T> getList(String key) throws Exception {
		Object value = get(key);
		if (value == null) {
			return null;
		}
		if (List.class.isAssignableFrom(value.getClass())) {
			return Lists.newList((List<T>) value);
		}
		return JsonUtils.toList(JsonUtils.toJson(value));
	}

	/**
	 * getValue
	 * 
	 * @param <R>
	 * @param key
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public <T> T getValue(String key) {
		Object value = get(key);
		return value == null ? null : (T) value;
	}

	/**
	 * getValue
	 * 
	 * @param <R>
	 * @param key
	 * @param cls
	 * @return
	 * @throws Exception
	 */
	public <R> R getValue(String key, Class<R> cls) throws Exception {
		return JsonUtils.fromJson(get(key), cls);
	}

	/**
	 * withoutKey
	 * 
	 * @param key
	 * @return
	 */
	public boolean withoutKey(String key) {
		return !containsKey(key);
	}

	/**
	 * withoutKey
	 * 
	 * @param key
	 * @return
	 */
	public boolean withoutValue(V value) {
		return !containsValue(value);
	}

	/**
	 * notEmpty
	 * 
	 * @return
	 */
	public boolean isNotEmpty() {
		return !isEmpty();
	}

	/**
	 * 根据value取key
	 * 
	 * @param <K>
	 * @param <V>
	 * @param map
	 * @param value
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public K getKey(V value) {
		if (containsValue(value)) {
			Object[] values = values().toArray();
			for (int i = 0; i < values.length; i++) {
				if (values[i].equals(value)) {
					return (K) keySet().toArray()[i];
				}
			}
		}
		return null;
	}

	/**
	 * 反转map
	 * 
	 * @param map
	 * @return
	 */
	public EasyMap<V, K> revertMap() {
		EasyMap<V, K> revertMap = new EasyMap<>();
		for (Entry<K, V> entry : entrySet()) {
			revertMap.put(entry.getValue(), entry.getKey());
		}
		return revertMap;
	}

}
