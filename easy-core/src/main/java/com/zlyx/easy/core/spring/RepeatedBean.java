package com.zlyx.easy.core.spring;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * <p>
 * 可重复bean
 * </p>
 *
 * @author 赵光
 * @since 2018年12月10日
 */
public class RepeatedBean<T> {

	protected Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired(required = false)
	protected List<T> beans;

}