package com.zlyx.easy.core.thread;

import com.zlyx.easy.core.loggers.Logger;

/**
 * <p>
 * 阻塞式线程
 * </p>
 *
 * @author 赵光
 * @since 2019年5月11日
 */
public abstract class BlockRunnable implements Runnable {

	private String name;
	private int blocktime;
	private boolean isInterrupt;

	public BlockRunnable(String name, int blocktime) {
		this.setName(name);
		this.blocktime = blocktime;
		this.isInterrupt = false;
	}

	public BlockRunnable(String name) {
		this(name, 100);
	}

	public BlockRunnable(int blocktime) {
		this.blocktime = blocktime;
	}

	public BlockRunnable() {
		this(100);
	}

	@Override
	public void run() {
		try {
			while (!isInterrupt) {
				if (!doBlock()) {
					break;
				}
				Thread.sleep(blocktime);
			}
		} catch (Exception e) {
			Logger.err(e);
		} finally {
			try {
				doFinal();
			} catch (Exception e) {
				Logger.err(e);
			}
		}
	}

	/**
	 * finally方法
	 * 
	 * @throws Exception
	 */
	public void doFinal() throws Exception {
	}

	/**
	 * 阻塞方法(返回false将退出循环)
	 * 
	 * @return
	 * @throws Exception
	 */
	public abstract boolean doBlock() throws Exception;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isInterrupt() {
		return isInterrupt;
	}

	public void interrupt() {
		this.isInterrupt = true;
	}
}