package com.zlyx.easy.core.sql;

import java.util.List;

public class TableEntity {

	/**
	 * 表名称
	 */
	private String tableName;

	/**
	 * 表字段
	 */
	private List<TableColumn> columns;

	public List<TableColumn> getColumns() {
		return columns;
	}

	public void setColumns(List<TableColumn> columns) {
		this.columns = columns;
	}

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

}
