package com.zlyx.easy.core.app;

/**
 * <p>
 * 启动类工具
 * </p>
 *
 * @author 赵光
 * @since 2020年5月18日
 */
public final class AppUtils {

	private static Class<?> mainClass;

	/**
	 * 获取启动类
	 * 
	 * @return
	 */
	public static Class<?> getMainClass() {
		if (mainClass == null) {
			try {
				StackTraceElement[] stackTrace = new RuntimeException().getStackTrace();
				for (StackTraceElement stackTraceElement : stackTrace) {
					if ("main".equals(stackTraceElement.getMethodName())) {
						mainClass = Class.forName(stackTraceElement.getClassName());
						return mainClass;
					}
				}
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
		}
		return mainClass;
	}

	/**
	 * 获取启动类所在包
	 * 
	 * @return
	 */
	public static String getMainPackagePath() {
		return getMainClass().getPackage().getName();
	}

}
