package com.zlyx.easy.core.utils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;

import com.zlyx.easy.core.collections.Lists;

/**
 * <p>
 * 文件操作工具
 * </p>
 *
 * @author 赵光
 * @since 2019年8月3日
 */
public class FileUtils {

	private static Logger logger = LoggerFactory.getLogger(FileUtils.class);

	/**
	 * 获取指定类的java文件
	 * 
	 * @param cls
	 * @return
	 */
	public static File getJavaFile(Class<?> cls) {
		return new File(ClassUtils.getClassPath(cls));
	}

	/**
	 * 递归获取指定目录下面所有的Java文件,包括子目录中的
	 * 
	 * @param path 文件路径
	 * @return
	 */
	public static List<String> getJavaFiles(String path) {
		return getJavaFiles(new File(path));
	}

	/**
	 * 递归获取指定目录下面所有的Java文件,包括子目录中的
	 * 
	 * @param file 文件目录
	 * @return 所有java文件
	 */
	public static List<String> getJavaFiles(File file) {
		if (!file.exists()) {
			return new ArrayList<>(0);
		}

		if (file.isFile()) {
			if (file.getName().lastIndexOf(".java") > 0) {
				return Lists.newList(file.getAbsolutePath());
			} else {
				return Lists.newList();
			}
		}

		List<String> list = Lists.newList();
		if (file.isDirectory()) {
			File[] files = file.listFiles();
			if (files != null) {
				for (File f : files) {
					list.addAll(getJavaFiles(f));
				}
			}
		}
		return list;
	}

	/**
	 * 写出文件
	 * 
	 * @param filePath 文件路径
	 * @param fileName 文件名称
	 * @param text     文本内容
	 * @param append   是否追加到
	 * @return
	 * @throws IOException
	 */
	public static String writeAsFile(String filePath, String fileName, String text) throws Exception {
		return writeAsFile(filePath, fileName, text, false);
	}

	/**
	 * 写出文件
	 * 
	 * @param filePath 文件路径
	 * @param fileName 文件名称
	 * @param text     文本内容
	 * @param rewrite  是否强制重写文件
	 * @return
	 * @throws IOException
	 */
	public static String writeAsFile(String filePath, String fileName, String text, boolean rewrite) throws Exception {
		logger.info("开始写文件: {}", filePath + "/" + fileName);
		mkdirFolder(filePath);
		File file = new File(filePath + "/" + fileName);
		if (file.exists() && !rewrite) { // 如果文件已存在且不强制重写，直接返回
			logger.info("文件已存在: {}", filePath + "/" + fileName);
			return text;
		}
		if (!file.exists()) {
			file.createNewFile(); // 创建文件
		}
		BufferedWriter bufferWritter = new BufferedWriter(new FileWriter(file));
		bufferWritter.write(text);
		bufferWritter.close();
		logger.info("写文件完成：{}", filePath + "\\" + fileName + ":\n\n" + text);
		return text;
	}

	/**
	 * 创建文件
	 * 
	 * @param Path    文件路径
	 * @param text    文本内容
	 * @param rewrite 是否强制重写文件
	 * @return
	 * @throws IOException
	 */
	public static File createFile(String path, String fileName) throws IOException {
		File file = new File(path + "/" + fileName);
		if (!file.exists()) {
			file.createNewFile(); // 创建文件
		}
		return file;
	}

	/**
	 * 创建多级文件夹
	 * 
	 * @param path
	 * @return
	 */
	private static File mkdirFolder(String path) throws Exception {
		File file = new File(path);
		if (!file.exists()) {
			file.mkdirs();
		}
		return file;
	}

	/**
	 * 读取文件
	 * 
	 * @param path
	 * @return
	 */
	public static String readFile(String path) {
		return getResource(new File(path));
	}

	/**
	 * 读取src/main/resources路径下的文件
	 * 
	 * @param filePath 文件路径
	 * @return
	 * @throws IOException
	 */
	public static String readResource(String filePath) throws IOException {
		return getResource(new ClassPathResource(filePath).getFile());
	}

	/**
	 * 读取文件
	 * 
	 * @param path
	 * @return
	 */
	public static String getResource(File file) {
		FileReader fileReader = null;
		Reader reader = null;
		try {
			fileReader = new FileReader(file);
			reader = new InputStreamReader(new FileInputStream(file), "utf-8");
			int ch;
			StringBuffer sb = new StringBuffer();
			while ((ch = reader.read()) != -1) {
				sb.append((char) ch);
			}
			fileReader.close();
			reader.close();
			return sb.toString();
		} catch (IOException e) {
			logger.error(e.getMessage(), e);
		} finally {
			if (fileReader != null) {
				try {
					fileReader.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return null;
	}
}
