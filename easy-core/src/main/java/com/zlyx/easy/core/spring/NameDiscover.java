package com.zlyx.easy.core.spring;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;

import org.springframework.core.LocalVariableTableParameterNameDiscoverer;
import org.springframework.core.ParameterNameDiscoverer;
import org.springframework.core.PrioritizedParameterNameDiscoverer;
import org.springframework.core.StandardReflectionParameterNameDiscoverer;

/**
 * <p>
 * 方法参数名称解析工具
 * </p>
 *
 * @author 赵光
 * @since 2018年12月10日
 */
public final class NameDiscover {

	private static final ParameterNameDiscoverer parameterNameDiscover;

	static {
		parameterNameDiscover = new PrioritizedParameterNameDiscoverer();
		((PrioritizedParameterNameDiscoverer) parameterNameDiscover)
				.addDiscoverer(new LocalVariableTableParameterNameDiscoverer());
		((PrioritizedParameterNameDiscoverer) parameterNameDiscover)
				.addDiscoverer(new StandardReflectionParameterNameDiscoverer());
	}

	public static String[] getParameterNames(Method method) {
		return parameterNameDiscover.getParameterNames(method);
	}

	public static String[] getParameterNames(Constructor<?> ctor) {
		return parameterNameDiscover.getParameterNames(ctor);
	}
}
