package com.zlyx.easy.core.collections;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * <p>
 * 集合操作工具类
 * </p>
 *
 * @author 赵光
 * @since 2018年11月27日
 */
public final class Lists {

	/**
	 * 创建 list
	 * 
	 * @param es
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static <E> EasyList<E> newList(E... es) {
		return new EasyList<E>().addAll(es);
	}

	/**
	 * 创建 list
	 * 
	 * @param list
	 * @return
	 */
	public static <E> EasyList<E> newList(Collection<E> list) {
		return new EasyList<E>(list);
	}

	/**
	 * @return
	 */
	public static <E> List<E> newArrayList() {
		return new ArrayList<>();
	}

	/**
	 * 创建set
	 * 
	 * @param es
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static <E> Set<E> newHashSet(E... es) {
		return new HashSet<>(Arrays.asList(es));
	}

	/**
	 * 创建set
	 * 
	 * @param list
	 * @return
	 */
	public static <E> Set<E> newHashSet(Collection<E> list) {
		return new HashSet<>(list);
	}

}
