package com.zlyx.easy.core.exception;

/**
 * <p>
 * 业务异常类
 * </p>
 *
 * @author 赵光
 * @since 2018年11月27日
 */
public class BusinessException extends Exception {

	private static final long serialVersionUID = 1L;

	/**
	 * 异常返回码
	 */
	public static final int EXCEPTION = -1;

	/**
	 * 响应码
	 */
	private int code;

	public BusinessException() {
		super();
	}

	public BusinessException(String message) {
		super(message);
		this.code = EXCEPTION;
	}

	public BusinessException(int code, String message) {
		super(message);
		this.code = code;
	}

	/**
	 * 实例化业务异常
	 * 
	 * @param message
	 * @return
	 */
	public static BusinessException newException(String message) {
		return new BusinessException(EXCEPTION, message);
	}

	/**
	 * 实例化业务异常
	 * 
	 * @param code
	 * @param message
	 * @return
	 */
	public static BusinessException newException(int code, String message) {
		return new BusinessException(code, message);
	}

	/**
	 * 实例化业务异常
	 * 
	 * @param t
	 * @return
	 */
	public static BusinessException newException(Throwable t) {
		return new BusinessException(EXCEPTION, t.getMessage());
	}

	/**
	 * 抛出业务异常
	 * 
	 * @param t
	 * @return
	 * @throws BusinessException
	 */
	public static void throwException(Throwable t) throws BusinessException {
		throw new BusinessException(EXCEPTION, t.getMessage());
	}

	/**
	 * 抛出业务异常
	 * 
	 * @param message
	 * @return
	 * @throws BusinessException
	 */
	public static void throwException(String message) throws BusinessException {
		throw new BusinessException(EXCEPTION, message);
	}

	/**
	 * 抛出业务异常
	 * 
	 * @param code
	 * @param message
	 * @return
	 * @throws BusinessException
	 */
	public static void throwException(int code, String message) throws BusinessException {
		throw new BusinessException(code, message);
	}

	/**
	 * 抛出业务异常
	 * 
	 * @param message
	 * @return
	 * @throws BusinessException
	 */
	public static void throwAndLogException(String message) throws BusinessException {
		throw new BusinessException(EXCEPTION, message);
	}

	/**
	 * @return the code
	 */
	public int getCode() {
		return code;
	}

	/**
	 * @param code the code to set
	 */
	public void setCode(int code) {
		this.code = code;
	}
}
