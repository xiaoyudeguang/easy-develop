package com.zlyx.easy.core.model;

import com.zlyx.easy.core.utils.JsonUtils;
import com.zlyx.easy.core.utils.RandomUtils;

/**
 * <p>
 * 用户实体
 * </p>
 *
 * @author 赵光
 * @since 2019年1月10日
 */
public class UserModel {

	private String id;

	private String name;

	private Integer age;

	public UserModel() {
	}

	public UserModel(String name) {
		this(name, null);
	}

	public UserModel(Integer age) {
		this(null, age);
	}

	public UserModel(String name, Integer age) {
		this.id = RandomUtils.randomID();
		this.name = name;
		this.age = age;
	}

	public String getName() {
		return name;
	}

	public UserModel setName(String name) {
		this.name = name;
		return this;
	}

	public Integer getAge() {
		return age;
	}

	public UserModel setAge(Integer age) {
		this.age = age;
		return this;
	}

	public String getId() {
		return id;
	}

	public UserModel setId(String id) {
		this.id = id;
		return this;
	}

	@Override
	public String toString() {
		return JsonUtils.toJson(this);
	}

}
