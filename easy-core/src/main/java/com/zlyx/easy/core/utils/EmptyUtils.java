/**
 *
 * @Author 赵光
 * @Desc NoNullUtils
 * @Date 2020年5月23日
 */
package com.zlyx.easy.core.utils;

import java.util.List;
import java.util.Map;

import com.zlyx.easy.core.collections.Lists;
import com.zlyx.easy.core.loggers.Logger;
import com.zlyx.easy.core.map.Maps;

/**
 * <p>
 * 空对象转Empty转换工具
 * </p>
 *
 * @author 赵光
 * @since 2019年1月4日
 */
public class EmptyUtils {

	/**
	 * 对象非空
	 * 
	 * @param <T>
	 * @param obj
	 * @param cls
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public static <T> T nullOfEmpty(T obj, Class<T> cls) {
		Logger.warn("obj is null");
		try {
			return obj != null ? obj : cls.getConstructor().newInstance();
		} catch (Exception e) {
			Logger.err(e);
			return (T) new Object();
		}
	}

	/**
	 * 对象非空
	 * 
	 * @param <T>
	 * @param obj
	 * @param cls
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public static <T> T nullOfEmpty(T obj) throws Exception {
		Logger.warn("obj is null");
		return obj != null ? obj : (T) new Object();
	}

	/**
	 * list非空
	 * 
	 * @param <E>
	 * @param list
	 * @return
	 */
	public static <E> List<E> nullOfEmpty(List<E> list) {
		Logger.warn("list is null");
		return list != null ? list : Lists.newArrayList();
	}

	/**
	 * map非空
	 * 
	 * @param <K>
	 * @param <V>
	 * @param map
	 * @return
	 */
	public static <K, V> Map<K, V> nullOfEmpty(Map<K, V> map) {
		Logger.warn("map is null");
		return map != null ? map : Maps.newHashMap();
	}
}
