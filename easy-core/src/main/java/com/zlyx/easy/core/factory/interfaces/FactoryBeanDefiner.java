package com.zlyx.easy.core.factory.interfaces;

import java.lang.reflect.Method;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.BeanDefinitionRegistryPostProcessor;

import com.zlyx.easy.core.factory.FactoryBeanConfigurer;

/**
 * @Auth 赵光
 * @Describle 工厂Bean定义
 * @2019年6月15日 上午10:42:33
 */
public interface FactoryBeanDefiner extends BeanDefinitionRegistryPostProcessor, InitializingBean {

	/**
	 * 自定义处理方法
	 * 
	 * @param proxy
	 * @param defaultMap
	 * @param method
	 * @return
	 * @throws Exception
	 */
	Object excute(Class<?> proxyClass, Method method, Object[] args) throws Exception;

	@Override
	default void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {
	};

	@Override
	default void postProcessBeanDefinitionRegistry(BeanDefinitionRegistry registry) throws BeansException {

	}

	@Override
	public default void afterPropertiesSet() throws Exception {
		FactoryBeanConfigurer.addFactoryBeanHandler(this);
	}
}
