package com.zlyx.easy.core.sql;

import com.zlyx.easy.core.utils.StringUtils;

public class TableColumn {

	/**
	 * 字段名称
	 */
	private String name;

	/**
	 * 字段类型
	 */
	private String type;

	/**
	 * 字段描述
	 */
	private String desc;

	/**
	 * 默认值
	 */
	private String defaultValue;

	/**
	 * 是否允许为空
	 */
	private boolean allowBlank;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public String getDefaultValue() {
		return defaultValue;
	}

	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}

	public boolean isAllowBlank() {
		return allowBlank;
	}

	public void setAllowBlank(boolean allowBlank) {
		this.allowBlank = allowBlank;
	}

	public String toString() {
		if ("id".equals(name)) {
			return "";
		}
		return name + " " + type + " " + (allowBlank ? "" : " NOT NULL")
				+ (StringUtils.isBlank(defaultValue) ? "" : " DEFAULT " + defaultValue);
	}
}
