package com.zlyx.easy.core.tool;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import com.zlyx.easy.core.buffer.EasyBuffer;
import com.zlyx.easy.core.utils.ObjectUtils;

/**
 * <p>
 * 字符串格式化工具
 * </p>
 *
 * @author 赵光
 * @since 2019年4月19日
 */
public class StringFormat {

	public static String DEFAULT_FORMAT = "&%s=%s";

	/**
	 * 格式化map
	 *
	 * @param params  参数map
	 * @param formats 支持两个参数，第一个format代表格式化后字符串的前缀(可以不传或传空)，第二个参数是参数格式
	 * @return
	 */
	public static String format(Map<String, String> params, String... formats) {
		if (params == null || params.size() == 0) {
			return "";
		}
		String format = DEFAULT_FORMAT;
		String prefix = "";
		if (formats != null) {
			if (formats.length > 0) {
				prefix = formats[0] != null ? formats[0] : prefix;
			}
			if (formats.length > 1) {
				format = formats[1] != null ? formats[1] : format;
			}
		}
		StringBuilder stream = new StringBuilder();
		for (Map.Entry<String, String> entry : params.entrySet()) {
			if (entry.getValue() != null) {
				stream.append(String.format(format, entry.getKey(), entry.getValue()));
			}
		}
		return prefix + stream.toString().substring(1);
	}

	/**
	 * 将map转化为以指定分隔符分隔的字符串
	 * 
	 * @param map
	 * @param separator
	 * @return
	 */
	public static String format(Map<?, ?> map, String separator) {
		EasyBuffer sb = new EasyBuffer();
		map.forEach((key, value) -> {
			sb.append(key).append("=").append(map.get(key)).append(separator);
		});
		return sb.clear(separator.length());
	}

	/**
	 * 将map转化为以","作为分隔符的字符串
	 * 
	 * @param map
	 * @return
	 */
	public static String format(Map<?, ?> map) {
		return format(map, ",");
	}

	/**
	 * 将list转化为以指定分隔符分隔的字符串
	 * 
	 * @param es
	 * @param separator
	 * @return
	 */
	public static String format(List<?> es, String separator) {
		EasyBuffer sb = new EasyBuffer();
		es.stream().forEach(e -> {
			if (ObjectUtils.isNotEmpty(e)) {
				sb.append(e).append(separator);
			}
		});
		return sb.clear(separator.length());

	}

	/**
	 * 将list转化为以","作为分隔符的字符串
	 * 
	 * @param es
	 * @return
	 */
	public static String format(List<?> es) {
		return format(es, ",");
	}

	/**
	 * 将数组转化为指定分隔符分隔的字符串
	 * 
	 * @param es
	 * @param separator
	 * @return
	 */
	public static String format(Object[] es, String separator) {
		return format(Arrays.asList(es), separator);
	}

	/**
	 * 将数组转化为以","作为分隔符分隔的字符串
	 * 
	 * @param es
	 * @return
	 */
	public static String format(Object[] es) {
		return format(es, ",");
	}
}
