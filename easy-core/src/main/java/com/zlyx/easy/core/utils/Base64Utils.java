package com.zlyx.easy.core.utils;

import java.nio.charset.StandardCharsets;
import java.util.Base64;

/**
 * <p>
 * Base64操作工具类
 * </p>
 *
 * @author 赵光
 * @since 2019年12月5日
 */
public class Base64Utils {

	/**
	 * 加密字符串
	 *
	 * @param source
	 * @return
	 */
	public static String encode(String src) {
		return Base64.getEncoder().encodeToString(src.getBytes());
	}

	/**
	 * 解密字符串
	 *
	 * @param src
	 * @return
	 */
	public static String decode(String src) {
		return decode(src.getBytes(StandardCharsets.UTF_8));
	}

	/**
	 * 解密数组
	 *
	 * @param in
	 * @return
	 */
	public static String decode(byte[] src) {
		return new String(Base64.getDecoder().decode(src), StandardCharsets.UTF_8);
	}
}
