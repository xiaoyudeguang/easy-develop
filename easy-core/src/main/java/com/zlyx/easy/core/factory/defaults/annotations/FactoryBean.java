package com.zlyx.easy.core.factory.defaults.annotations;

import java.lang.annotation.Annotation;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.springframework.stereotype.Component;

/**
 * <p>
 * FactoryBean工厂类声明注解
 * </p>
 *
 * @author 赵光
 * @since 2019年1月16日
 */
@Documented
@Target({ ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
@Component
public @interface FactoryBean {

	/**
	 * 接口注解
	 * 
	 * @return
	 */
	Class<? extends Annotation> annotationClass();

	/**
	 * 扫描注解
	 * 
	 * @return
	 */
	Class<? extends Annotation> scanAnnotation() default Annotation.class;

	/**
	 * 作用描述
	 * 
	 * @return
	 */
	String todo();
}
