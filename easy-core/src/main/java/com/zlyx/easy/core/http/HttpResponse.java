package com.zlyx.easy.core.http;

import java.util.List;
import java.util.Map;

import com.zlyx.easy.core.collections.EasyList;
import com.zlyx.easy.core.collections.Lists;
import com.zlyx.easy.core.map.EasyMap;
import com.zlyx.easy.core.map.Maps;
import com.zlyx.easy.core.utils.JsonUtils;

/**
 * <p>
 * http响应
 * </p>
 *
 * @author 赵光
 * @since 2019年4月8日
 */
public class HttpResponse extends EasyMap<String, Object> {

	private static final long serialVersionUID = 1L;

	private static String SUCCESS = "succcess";

	private static String FAILURE = "failure";

	public HttpResponse() {
	}

	public HttpResponse(int code, Object data, String msg) {
		put("code", code);
		put("data", data);
		put("msg", msg);
	}

	/**
	 * getCode
	 * 
	 * @return
	 */
	public int getCode() {
		return (int) get("code");
	}

	/**
	 * getMsg
	 * 
	 * @return
	 */
	public String getMsg() {
		return (String) get("msg");
	}

	/**
	 * getData
	 * 
	 * @return
	 */
	public Object getData() {
		return get("data");
	}

	/**
	 * get
	 * 
	 * @param <T>
	 * @param key
	 * @param cls
	 * @return
	 * @throws Exception
	 */
	public <T> T get(String key, Class<T> cls) throws Exception {
		return JsonUtils.fromJson(get(key), cls);
	}

	/**
	 * getMap
	 * 
	 * @param key
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public <M, N> EasyMap<M, N> getMap(String key) throws Exception {
		Object value = get(key);
		if (value == null) {
			return null;
		}
		if (Map.class.isAssignableFrom(value.getClass())) {
			return Maps.newMap((Map<M, N>) value);
		}
		return JsonUtils.toMap(JsonUtils.toJson(value));
	}

	/**
	 * getList
	 * 
	 * @param <T>
	 * @param key
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public <T> EasyList<T> getList(String key) throws Exception {
		Object value = get(key);
		if (value == null) {
			return null;
		}
		if (List.class.isAssignableFrom(value.getClass())) {
			return Lists.newList((List<T>) value);
		}
		return JsonUtils.toList(JsonUtils.toJson(value));
	}

	public static HttpResponse response(int code, Object data, String msg) {
		return new HttpResponse(code, data, msg);
	}

	public static HttpResponse success() {
		return success(1);
	}

	public static HttpResponse success(int code) {
		return success(code, null);
	}

	public static HttpResponse success(Object data) {
		return response(1, data, SUCCESS);
	}

	public static HttpResponse success(int code, Object data) {
		return response(code, data, SUCCESS);
	}

	public static HttpResponse failure() {
		return failure(0);
	}

	public static HttpResponse failure(int code) {
		return failure(code, FAILURE);
	}

	public static HttpResponse failure(String msg) {
		return failure(0, msg);
	}

	public static HttpResponse failure(int code, String msg) {
		return response(code, null, msg);
	}

	public static HttpResponse failure(Throwable t) {
		return failure(0, t);
	}

	public static HttpResponse failure(int code, Throwable t) {
		return failure(code, t.getMessage());
	}

}