package com.zlyx.easy.core.handler.annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.springframework.core.annotation.AliasFor;
import org.springframework.stereotype.Component;

/**
 * <p>
 * 分支注解
 * </p>
 *
 * @author 赵光
 * @since 2019年1月16日
 */
@Documented
@Target({ ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
@Component
public @interface Brancher {

	@AliasFor(annotation = Component.class, attribute = "value")
	String value();

	/**
	 * 作用
	 * 
	 * @return
	 */
	String[] todo();

	/**
	 * 备用参数(定制化)
	 */
	String[] params() default "";
}