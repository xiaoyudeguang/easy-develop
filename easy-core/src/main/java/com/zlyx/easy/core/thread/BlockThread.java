package com.zlyx.easy.core.thread;

import com.zlyx.easy.core.loggers.Logger;

/**
 * <p>
 * 阻塞式线程
 * </p>
 *
 * @author 赵光
 * @since 2019年5月11日
 */
public abstract class BlockThread extends Thread {

	private int blocktime;

	public BlockThread(String name, int blocktime) {
		this.setName(name);
		this.blocktime = blocktime;
	}

	public BlockThread(String name) {
		this(name, 5000);
	}

	public BlockThread(int blocktime) {
		this.blocktime = blocktime;
	}

	public BlockThread() {
		this(5000);
	}

	@Override
	public void run() {
		try {
			while (!isInterrupted()) {
				if (!doBlock()) {
					break;
				}
				Thread.sleep(blocktime);
			}
		} catch (Exception e) {
			Logger.err(e);
		} finally {
			try {
				doFinal();
			} catch (Exception e) {
				Logger.err(e);
			}
		}
	}

	/**
	 * finally方法
	 * 
	 * @throws Exception
	 */
	public void doFinal() throws Exception {
	}

	/**
	 * 阻塞方法(返回false将退出循环)
	 * 
	 * @return
	 * @throws Exception
	 */
	public abstract boolean doBlock() throws Exception;
}