package com.zlyx.easy.core.tool;

import com.zlyx.easy.core.utils.StringUtils;

/**
 * <p>
 * java基本类型判断工具
 * </p>
 *
 * @author 赵光
 * @since 2019年8月22日
 */
public class BaseType {

	/**
	 * 是否基础类型
	 * 
	 * @param cls
	 * @return
	 */
	public static boolean isJavaType(Class<?> cls) {
		return isJavaType(cls.getName());
	}

	/**
	 * 是否基础类型
	 * 
	 * @param className
	 * @return
	 */
	public static boolean isJavaType(String className) {
		return StringUtils.ncontains(className, ".") || className.contains("java.lang");
	}
}
