package com.zlyx.easy.core.handler;

import java.lang.reflect.ParameterizedType;

import com.zlyx.easy.core.spring.SpringUtils;

/**
 * <p>
 * 流程控制器
 * </p>
 *
 * @author 赵光
 * @since 2019年1月16日
 */
public interface IHandler<T> {

	/**
	 * 控制方法
	 * 
	 * @param data
	 * @return
	 */
	Object doHandle(T data) throws Exception;

	/**
	 * 转发业务
	 * 
	 * @param beanName
	 * @param data
	 * @return
	 * @throws Exception
	 */
	static <T> Object call(String beanName, T data) throws Exception {
		return SpringUtils.<IHandler<T>>getBean(beanName).doHandle(data);
	}

	/**
	 * 读取抽象类
	 * 
	 * @return
	 */
	@SuppressWarnings("unchecked")
	default Class<T> getTClass() {
		return (Class<T>) ((ParameterizedType) this.getClass().getGenericInterfaces()[0]).getActualTypeArguments()[0];
	}
}
