/**
 *
 * @Author 赵光
 * @Desc BaseAspect
 * @Date 2020年5月18日
 */
package com.zlyx.easy.core.aop;

import java.lang.reflect.Method;
import java.lang.reflect.Parameter;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.lang.NonNull;

import com.zlyx.easy.core.utils.MethodUtils;
import com.zlyx.easy.core.utils.ObjectUtils;
import com.zlyx.easy.core.utils.RandomUtils;

/**
 * <p>
 * 顶级切面父类
 * </p>
 *
 * @author 赵光
 * @since 2020年5月18日
 */
public abstract class AopAspect implements InitializingBean {

	protected Logger logger = LoggerFactory.getLogger(getClass());

	/**
	 * 请求id
	 */
	protected String requestId;

	/**
	 * 被切面真实类
	 */
	protected Class<?> realClass;

	/**
	 * 被切面方法
	 */
	protected Method realMethod;

	/**
	 * 返回类型
	 */
	protected Class<?> returnType;

	/**
	 * 开始时间
	 */
	protected long startTime;

	/**
	 * 初始化切面上下文
	 * 
	 * @param pjp
	 */
	protected void init(ProceedingJoinPoint pjp) {
		Signature signature = pjp.getSignature();
		MethodSignature methodSignature = (MethodSignature) signature;
		Method targetMethod = methodSignature.getMethod();
		this.realClass = targetMethod.getDeclaringClass();
		this.realMethod = MethodUtils.getRealMethod(realClass, targetMethod);
		this.returnType = realMethod.getReturnType();
		this.startTime = System.currentTimeMillis();
		this.requestId = RandomUtils.randomID();
	}

	/**
	 * 校验参数是否为空
	 * 
	 * @param method 方法
	 * @param args   参数
	 * @return
	 */
	protected boolean doValidate(Method method, Object[] args) {
		Parameter[] paramters = method.getParameters();
		if (paramters != null && paramters.length > 0) {
			Parameter parameter = null;
			Object arg = null;
			for (int i = 0; i < paramters.length; i++) {
				parameter = paramters[i];
				arg = args[i];
				if (Boolean.FALSE == doValidate(parameter, arg)) {
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * 校验参数是否为空
	 * 
	 * @param parameter
	 * @param arg
	 * @return
	 */
	protected boolean doValidate(Parameter parameter, Object arg) {
		return parameter.getAnnotation(NonNull.class) == null || ObjectUtils.isNotEmpty(arg);
	}

}
