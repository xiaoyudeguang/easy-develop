package com.zlyx.easy.core.map;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * 等价于Map《String, Map《String,V》》, 功能增强
 * </p>
 *
 * @author 赵光
 * @since 2020年1月10日
 */
public class TableMap<V> extends EasyMap<String, Map<String, V>> {

	private static final long serialVersionUID = 1L;

	public TableMap() {
	}

	public TableMap<V> addValue(String k, String s, V v) {
		return addValue(k, Maps.newMap(s, v));
	}

	@Override
	public TableMap<V> addValue(String k, Map<String, V> map) {
		Map<String, V> element = get(k);
		if (element == null) {
			element = new HashMap<>();
		}
		element.putAll(map);
		put(k, element);
		return this;
	}

	/**
	 * newMap
	 * 
	 * @param <V>
	 * @return
	 */
	public static <V> TableMap<V> newMap() {
		return new TableMap<V>();
	}

	/**
	 * newMap
	 * 
	 * @param <V>
	 * @return
	 */
	public static <V> TableMap<V> newMap(String k, String s, V v) {
		return new TableMap<V>().addValue(k, s, v);
	}

	/**
	 * newMap
	 * 
	 * @param <V>
	 * @return
	 */
	public static <V> TableMap<V> newMap(String k, Map<String, V> map) {
		return new TableMap<V>().addValue(k, map);
	}
}