package com.zlyx.easy.core.refresh;

import org.springframework.context.ApplicationContext;

/**
 * <p>
 * 热加载事件接口
 * </p>
 *
 * @author 赵光
 * @since 2018年6月10日
 */
public interface IHandlerOnChanged {

	/**
	 * 当热加载事件发生时执行
	 * 
	 * @param context
	 * @throws Exception
	 */
	void doOnChanged(ApplicationContext context) throws Exception;

}
