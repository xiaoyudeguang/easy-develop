package com.zlyx.easy.core.utils;

import java.util.Properties;

import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.support.PropertiesLoaderUtils;

/**
 * <p>
 * 配置文件操作工具
 * </p>
 *
 * @author 赵光
 * @since 2019年4月8日
 */
public class PropertyUtils {

	/**
	 * 获取指定属性
	 * 
	 * @param filePath     文件路径
	 * @param propertyName 属性名
	 * @return
	 * @throws Exception
	 */
	public static String getProperty(String filePath, String propertyName) throws Exception {
		return PropertiesLoaderUtils.loadProperties(new ClassPathResource(filePath)).getProperty(propertyName);
	}

	/**
	 * 读取文件
	 * 
	 * @param resourcePath 路径
	 * @return
	 * @throws Exception
	 */
	public static Properties getPropertys(String resourcePath) throws Exception {
		return PropertiesLoaderUtils.loadProperties(new ClassPathResource(resourcePath));
	}

}
