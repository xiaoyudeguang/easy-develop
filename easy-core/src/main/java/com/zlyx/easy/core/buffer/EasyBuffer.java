package com.zlyx.easy.core.buffer;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;

import com.zlyx.easy.core.jackson.JSONObject;
import com.zlyx.easy.core.utils.ObjectUtils;

/**
 * <p>
 * 字符串操作类
 * </p>
 *
 * @author 赵光
 * @since 2018年12月28日
 */
public final class EasyBuffer {

	private StringBuilder easyBuffer = null;

	public EasyBuffer() {
		this.easyBuffer = new StringBuilder();
	}

	/**
	 * 追加一个对象
	 * 
	 * @param o
	 * @return
	 */
	public EasyBuffer append(Object o) {
		this.easyBuffer.append(o);
		return this;
	}

	/**
	 * 追加对象数组
	 * 
	 * @param objs
	 * @return
	 */
	public EasyBuffer append(Object... objs) {
		append(Arrays.asList(objs));
		return this;
	}

	/**
	 * 追加集合
	 * 
	 * @param c
	 * @return
	 */
	public EasyBuffer append(Collection<?> c) {
		if (c == null) {
			return this;
		}
		for (Object obj : c) {
			append(obj);
		}
		return this;
	}

	/**
	 * 返回长度
	 * 
	 * @return
	 */
	public int length() {
		return this.easyBuffer.length();
	}

	@Override
	public String toString() {
		return this.easyBuffer.toString();
	}

	/**
	 * 是否包含字符串
	 * 
	 * @param s
	 * @return
	 */
	public boolean contains(String s) {
		return this.easyBuffer.indexOf(s) > -1;
	}

	/**
	 * 清除指定大小的尾巴
	 * 
	 * @param size 指定大小
	 * @return
	 */
	public String clear(int size) {
		return length() < size ? toString() : toString().substring(0, length() - size);
	}

	/**
	 * 从指定位置开始截取
	 * 
	 * @param offset 指定位置
	 * @return
	 */
	public String substring(int offset) {
		return toString().substring(offset, length());
	}

	/**
	 * 从指定位置开始删除
	 * 
	 * @param end
	 * @return
	 */
	public String delete(int offset) {
		return this.easyBuffer.delete(offset, length()).toString();
	}

	/**
	 * 删除区间外字符
	 * 
	 * @param start 起点
	 * @param end   结束点
	 * @return
	 */
	public String substring(int start, int end) {
		return toString().substring(start, end);
	}

	/**
	 * 删除区间内字符
	 * 
	 * @param start 起点
	 * @param end   结束点
	 * @return
	 */
	public EasyBuffer delete(int start, int end) {
		this.easyBuffer.delete(start, end);
		return this;
	}

	public static EasyBuffer newBuffer() {
		return new EasyBuffer();
	}

	public static EasyBuffer newBuffer(Object... objs) {
		return new EasyBuffer().append(objs);
	}

	/**
	 * 将所有参数拼接为字符串返回
	 * 
	 * @param objs
	 * @return
	 */
	public static String newString(Object... objs) {
		return newBuffer(objs).toString();
	}

	/**
	 * 将所有参数拼接为字符串返回
	 * 
	 * @param objs
	 * @return
	 */
	public static String wrapper(Object... objs) {
		return newString(objs);
	}

	/**
	 * 键@值
	 * 
	 * @param key
	 * @param value
	 * @return
	 * @throws IOException
	 */
	public static String at(Object key, Object value) throws IOException {
		if (ObjectUtils.isEmpty(value)) {
			return key.toString();
		}
		if (!value.getClass().getName().contains("java")) {
			value = JSONObject.formatObject(value);
		}
		if (ObjectUtils.isEmpty(key)) {
			return value.toString();
		}
		return EasyBuffer.wrapper(key, "@", value);
	}
}
