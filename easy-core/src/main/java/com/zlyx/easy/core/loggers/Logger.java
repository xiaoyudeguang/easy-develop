package com.zlyx.easy.core.loggers;

import org.slf4j.LoggerFactory;

import com.zlyx.easy.core.utils.StringUtils;

/**
 * <p>
 * 静态日志工具类
 * </p>
 *
 * @author 赵光
 * @since 2020年1月10日
 */
public class Logger {

	/**
	 * debug
	 * 
	 * @param o
	 * @param arguments
	 */
	public static void debug(Object o, Object... arguments) {
		String format = StringUtils.valueOf(o);
		getLogger().debug(format, arguments);
	}

	/**
	 * info
	 * 
	 * @param o
	 * @param arguments
	 */
	public static void info(Object o, Object... arguments) {
		String format = StringUtils.valueOf(o);
		getLogger().info(format, arguments);
	}

	/**
	 * warn
	 * 
	 * @param o
	 * @param arguments
	 */
	public static void warn(Object o, Object... arguments) {
		String format = StringUtils.valueOf(o);
		getLogger().warn(format, arguments);
	}

	/**
	 * error
	 * 
	 * @param o
	 * @param arguments
	 */
	public static void err(Object o, Object... arguments) {
		String format = StringUtils.valueOf(o);
		getLogger().error(format, arguments);
	}

	/**
	 * error
	 * 
	 * @param e
	 */
	public static void error(Throwable e) {
		getLogger().error(e.getMessage(), e);
	}

	/**
	 * error
	 *
	 * @param msg
	 * @param e
	 */
	public static void error(String msg, Throwable e) {
		getLogger().error(msg, e);
	}

	/**
	 * debug
	 * 
	 * @param cls
	 * @param o
	 * @param arguments
	 */
	public static void debug(Class<?> cls, Object o, Object... arguments) {
		String format = StringUtils.valueOf(o);
		getLogger(cls).debug(format, arguments);
	}

	/**
	 * info
	 * 
	 * @param cls
	 * @param o
	 * @param arguments
	 */
	public static void info(Class<?> cls, Object o, Object... arguments) {
		String format = StringUtils.valueOf(o);
		getLogger(cls).info(format, arguments);
	}

	/**
	 * warn
	 * 
	 * @param cls
	 * @param o
	 * @param arguments
	 */
	public static void warn(Class<?> cls, Object o, Object... arguments) {
		String format = StringUtils.valueOf(o);
		getLogger(cls).warn(format, arguments);
	}

	/**
	 * error
	 * 
	 * @param cls
	 * @param e
	 */
	public static void err(Class<?> cls, Throwable e) {
		getLogger(cls).error(e.getMessage(), e);
	}

	/**
	 * error
	 * 
	 * @param cls
	 * @param msg
	 */
	public static void err(Class<?> cls, Object o, Object... arguments) {
		String format = StringUtils.valueOf(o);
		getLogger(cls).error(format, arguments);
	}

	/**
	 * getLogger
	 *
	 * @return
	 */
	public static org.slf4j.Logger getLogger(Class<?> cls) {
		return LoggerFactory.getLogger(cls);
	}

	/**
	 * getLogger
	 *
	 * @return
	 */
	public static org.slf4j.Logger getLogger() {
		return LoggerFactory.getLogger(Thread.currentThread().getStackTrace()[3].getClassName());
	}

}
