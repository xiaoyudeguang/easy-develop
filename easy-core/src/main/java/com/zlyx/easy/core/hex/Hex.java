package com.zlyx.easy.core.hex;

/**
 * <p>
 * 十六进制
 * </p>
 *
 * @author 赵光
 * @since 2019年7月27日
 */
public class Hex {

	private String hex;

	public Hex(String hex) {
		this.hex = hex;
	}

	public String getHex() {
		return hex;
	}

	public void setHex(String hex) {
		this.hex = hex;
	}

	@Override
	public String toString() {
		return hex;
	}

	public static Hex newHex(String hex) {
		return new Hex(hex);
	}
}