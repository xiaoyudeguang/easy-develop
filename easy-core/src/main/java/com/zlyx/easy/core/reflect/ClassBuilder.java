package com.zlyx.easy.core.reflect;

import java.util.Arrays;
import java.util.Collection;

/**
 * <p>
 * 类构建器
 * </p>
 *
 * @author 赵光
 * @since 2020年6月10日
 */
public class ClassBuilder {

	/**
	 * 实例化类构建器
	 *
	 * @param cls         被重写的类
	 * @param importClass 导入类
	 * @return
	 */
	public static ClassBody newBody(Class<?> cls, Class<?>... importClass) {
		return newBody(cls.getPackage().getName(), cls.getSimpleName(), importClass);
	}

	/**
	 * 实例化类构建器
	 *
	 * @param packageName 包路径
	 * @param className   类名
	 * @param importClass 导入类
	 * @return
	 */
	public static ClassBody newBody(String packageName, String className, Class<?>... importClass) {
		return newBody(packageName, className, Arrays.asList(importClass));
	}

	/**
	 * 实例化类构建器
	 *
	 * @param packageName 包路径
	 * @param importClass 导入类
	 * @return
	 */
	public static ClassBody newBody(String packageName, String className, Collection<Class<?>> importClass) {
		return new ClassBody(packageName, className, importClass);
	}

}
