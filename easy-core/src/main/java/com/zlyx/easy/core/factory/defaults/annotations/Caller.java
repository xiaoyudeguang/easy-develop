package com.zlyx.easy.core.factory.defaults.annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * <p>
 * 调用者接口注解
 * </p>
 *
 * @author 赵光
 * @since 2019年1月16日
 */
@Documented
@Target({ ElementType.METHOD, ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
public @interface Caller {

	/**
	 * 被调用者beanName名称(类上不需要，方法上必须设置)
	 * 
	 * @return
	 */
	String value() default "";

	/**
	 * 预留参数
	 */
	String[] args() default "";

	/**
	 * 作用描述
	 * 
	 * @return
	 */
	String todo();
}
