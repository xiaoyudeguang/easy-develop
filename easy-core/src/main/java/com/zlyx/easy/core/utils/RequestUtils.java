package com.zlyx.easy.core.utils;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.zlyx.easy.core.map.Maps;

/**
 * <p>
 * 请求操作工具类
 * </p>
 *
 * @author 赵光
 * @since 2019年4月8日
 */
public final class RequestUtils {

	public static final String UNKNOWN_IP = "unknown";

	/**
	 * 获取当前服务器ip地址
	 * 
	 * @param request
	 * @return
	 */
	public static String getIpAddress(HttpServletRequest request) {
		String ip = request.getHeader("x-forwarded-for");
		if (ip == null || ip.length() == 0 || UNKNOWN_IP.equalsIgnoreCase(ip)) {
			ip = request.getHeader("Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || UNKNOWN_IP.equalsIgnoreCase(ip)) {
			ip = request.getHeader("WL-Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || UNKNOWN_IP.equalsIgnoreCase(ip)) {
			ip = request.getHeader("HTTP_CLIENT_IP");
		}
		if (ip == null || ip.length() == 0 || UNKNOWN_IP.equalsIgnoreCase(ip)) {
			ip = request.getHeader("HTTP_X_FORWARDED_FOR");
		}
		if (ip == null || ip.length() == 0 || UNKNOWN_IP.equalsIgnoreCase(ip)) {
			ip = request.getRemoteAddr();
		}
		return ip;
	}

	/**
	 * 获取当前服务器ip
	 * 
	 * @return
	 */
	public static String getIp() {
		String ip = getIpAddress(getRequest());
		if (StringUtils.isNotEmpty(ip)) {
			return ip.split(",")[0];
		}
		return null;
	}

	/**
	 * 获取当前服务器域名
	 * 
	 * @return
	 */
	public static String getHost() {
		return getRequest().getRequestURL().toString().replace(getRequest().getRequestURI(), "/");
	}

	/**
	 * 获取当前服务器地址
	 * 
	 * @return
	 */
	public static String getClient() {
		return "http://" + getIp() + ":" + getRequest().getRemotePort();
	}

	/**
	 * 获取当前请求
	 * 
	 * @return
	 */
	public static HttpServletRequest getRequest() {
		try {
			return ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * 解析请求头参数
	 * 
	 * @param request
	 * @return
	 */
	public Map<String, String> getHeaderParams() {
		return getHeaderParams(getRequest());
	}

	/**
	 * 解析请求头参数
	 * 
	 * @param request
	 * @return
	 */
	public Map<String, String> getHeaderParams(HttpServletRequest request) {
		Map<String, String> headers = new HashMap<>();
		if (request != null) {
			Enumeration<String> enu = request.getHeaderNames();
			while (enu.hasMoreElements()) {
				String element = enu.nextElement();
				headers.put(element, request.getHeader(element));
			}
		}
		return headers;
	}

	/**
	 * 解析请求参数
	 * 
	 * @return
	 */
	public static Map<String, String[]> getParameterMap() {
		return getRequest().getParameterMap();
	}

	/**
	 * 解析请求参数
	 * 
	 * @return
	 */
	public static Map<String, Object> getParamsMap() {
		Map<String, String[]> arraysMap = getParameterMap();
		Map<String, Object> params = Maps.newMap();
		for (String name : arraysMap.keySet()) {
			String[] values = arraysMap.get(name);
			if (ObjectUtils.isNotEmpty(values)) {
				params.put(name, values[0]);
			}
		}
		return params;
	}

}
