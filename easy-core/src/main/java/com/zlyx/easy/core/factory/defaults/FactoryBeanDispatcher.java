package com.zlyx.easy.core.factory.defaults;

import java.lang.reflect.Method;

import com.zlyx.easy.core.factory.defaults.annotations.Caller;
import com.zlyx.easy.core.factory.defaults.annotations.FactoryBean;
import com.zlyx.easy.core.factory.interfaces.FactoryBeanDefiner;
import com.zlyx.easy.core.spring.SpringUtils;
import com.zlyx.easy.core.utils.StringUtils;

/**
 * <p>
 * factoryBean转发器
 * </p>
 *
 * @author 赵光
 * @since 2019年1月16日
 */
@FactoryBean(annotationClass = Caller.class, todo = "factoryBean转发器")
public class FactoryBeanDispatcher implements FactoryBeanDefiner {

	@Override
	public Object excute(Class<?> proxyClass, Method method, Object[] args) throws Exception {
		Caller caller = method.getAnnotation(Caller.class);
		if (caller != null && StringUtils.isNotEmpty(caller.value())) {
			FactoryBeanDefiner definer = SpringUtils.getBean(caller.value());
			if (definer != null) {
				return definer.excute(proxyClass, method, args);
			}
		}
		return null;
	}
}
