package com.zlyx.easy.core.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * <p>
 * 日期操作工具
 * </p>
 *
 * @author 赵光
 * @since 2019年1月4日
 */
public class DateUtils {

	private static final String FORMAT_PATTERN = "yyyy-MM-dd HH:mm:ss";

	private static SimpleDateFormat sdf = new SimpleDateFormat(FORMAT_PATTERN);

	/**
	 * 获取字符串格式的当前日期
	 * 
	 * @return
	 */
	public static String getMillis() {
		return StringUtils.valueOf(System.currentTimeMillis());
	}

	/**
	 * 获取字符串格式的当前日期
	 * 
	 * @return
	 */
	public static String getNow(String... patterns) {
		return format(new Date(), patterns);
	}

	/**
	 * 获取字符串格式的当前日期
	 * 
	 * @return
	 */
	public static String getNowMs() {
		return format(new Date(), "yyyy-MM-dd HH:mm:ss:ms");
	}

	/**
	 * date转字符串
	 * 
	 * @param date
	 * @param pattern
	 * @return
	 */
	public static String format(Object date, String... patterns) {
		return applyPattern(patterns).format(date);
	}

	/**
	 * 字符串转date
	 * 
	 * @param source
	 * @return
	 */
	public static Date parse(String source, String... patterns) {
		try {
			return applyPattern(patterns).parse(source);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 返回规则
	 * 
	 * @param patterns
	 * @return
	 */
	public static SimpleDateFormat applyPattern(String... patterns) {
		if (patterns != null && patterns.length > 0) {
			return new SimpleDateFormat(patterns[0]);
		}
		return sdf;
	}
}
