package com.zlyx.easy.core.reflect;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 类属性操作工具
 * </p>
 *
 * @author 赵光
 * @since 2020年6月10日
 */
public class FieldUtils {

	private static Map<Class<?>, FieldMap> fieldMaps = new HashMap<Class<?>, FieldMap>();

	/**
	 * 取出参数类所有属性
	 * 
	 * @param tClass
	 * @return
	 */
	public static List<Field> getFields(Class<?> cls) {
		return Arrays.asList(cls.getDeclaredFields());
	}

	/**
	 * 取出参数类所有属性(包含父类)
	 * 
	 * @param tClass
	 * @return
	 */
	public static List<Field> getAllFields(Class<?> cls) {
		return getFieldMap(cls).fields();
	}

	/**
	 * 返回参数类的
	 * 
	 * @param cls
	 * @return
	 */
	public static synchronized FieldMap getFieldMap(Class<?> cls) {
		FieldMap fieldMap = fieldMaps.get(cls);
		if (fieldMap == null) {
			fieldMap = new FieldMap(cls);
			fieldMaps.put(cls, fieldMap);
		}
		return fieldMap;
	}

}
