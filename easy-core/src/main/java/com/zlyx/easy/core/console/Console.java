package com.zlyx.easy.core.console;

import com.zlyx.easy.core.utils.DateUtils;
import com.zlyx.easy.core.utils.StringUtils;

/**
 * <p>
 * 控制台打印工具类
 * </p>
 *
 * @author 赵光
 * @since 2018年11月27日
 */
public final class Console {

	public static void at(Object key, Object value) {
		println(key + " @.@ " + value);
	}

	public static void current() {
		println(System.currentTimeMillis());
	}

	public static void date() {
		println(DateUtils.getNow());
	}

	public static void dateMs() {
		println(DateUtils.getNowMs());
	}

	public static void log(Object o) {
		println(StringUtils.valueOf(o));
	}

	public static void log(Throwable t) {
		at(t.getMessage(), t);
	}

	public static void log(Object key, Object o) {
		at(key, StringUtils.valueOf(o));
	}

	private static void println(Object msg) {
		System.err.println("\n" + msg);
	}
}
