package com.zlyx.easy.core.utils;

import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;

import com.zlyx.easy.core.jackson.JSONObject;

/**
 * <p>
 * 客户端输出工具
 * </p>
 *
 * @author zg
 * @since 2019/12/31
 */
public class ResponseUtils {

	private static Logger logger = LoggerFactory.getLogger(ResponseUtils.class);

	/**
	 * 写出响应
	 *
	 * @param response 请求响应
	 * @param message  返回内容
	 * @throws IOException
	 */
	public static void write(HttpServletResponse response, Object message) throws IOException {
		if (message == null) {
			logger.warn("message is null");
			return;
		}
		response.setCharacterEncoding(StandardCharsets.UTF_8.toString());
		response.setContentType(MediaType.APPLICATION_JSON_VALUE);
		PrintWriter printWriter = response.getWriter();
		printWriter.append(JSONObject.toJSONString(message));
	}
}
