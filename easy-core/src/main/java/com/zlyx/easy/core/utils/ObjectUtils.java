package com.zlyx.easy.core.utils;

import java.lang.reflect.Array;
import java.util.Collection;
import java.util.Map;

import com.zlyx.easy.core.exception.BusinessException;

/**
 * <p>
 * 对象操作工具
 * </p>
 *
 * @author 赵光
 * @since 2019年4月8日
 */
public final class ObjectUtils {

	/**
	 * 获取当前对象的的全量类名
	 * 
	 * @param o
	 * @return
	 */
	public static String className(Object obj) {
		return obj == null ? null : obj.getClass().getName();
	}

	/**
	 * 获取当前对象的的精简类名
	 * 
	 * @param o
	 * @return
	 */
	public static String simpleClassName(Object obj) {
		return obj == null ? null : obj.getClass().getSimpleName();
	}

	/**
	 * 对象空判断
	 */
	@SuppressWarnings("rawtypes")
	public static boolean isEmpty(Object obj) {
		if (obj == null) {
			return true;
		}
		if (obj.getClass() == String.class) {
			return "".equals(obj);
		}
		if (obj.getClass().isArray()) {
			return Array.getLength(obj) == 0;
		}
		if (obj instanceof CharSequence) {
			return ((CharSequence) obj).length() == 0;
		}
		if (obj instanceof Collection) {
			return ((Collection) obj).isEmpty();
		}
		if (obj instanceof Map) {
			return ((Map) obj).isEmpty();
		}
		return false;
	}

	/**
	 * 判断参数对象是否不为空
	 * 
	 * @param es
	 * @return
	 */
	public static boolean isNotEmpty(Object o) {
		return Boolean.FALSE == isEmpty(o);
	}

	/**
	 * 判断对象数组是否包含空或空字符串
	 * 
	 * @param sArray
	 * @return
	 */
	public static boolean anyEmpty(Object... oArray) {
		if (oArray == null || oArray.length == 0) {
			return true;
		}
		for (Object o : oArray) {
			if (isEmpty(o)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 判断对象数组是否都不为空或空字符串
	 * 
	 * @param es
	 * @return
	 */
	public static boolean notEmpty(Object... oArray) {
		return Boolean.FALSE == anyEmpty(oArray);
	}

	/**
	 * 是否全部相等
	 * 
	 * @param s
	 * @param target
	 * @return
	 */
	public static boolean equals(Object source, Object... targets) {
		if (isEmpty(source) || targets == null || targets.length == 0) {
			return false;
		}
		for (Object target : targets) {
			if (!source.equals(target)) {
				return false;
			}
		}
		return true;
	}

	/**
	 * 是否和其中一个相等
	 * 
	 * @param s
	 * @param target
	 * @return
	 */
	public static boolean in(Object source, Object... targets) {
		for (Object target : targets) {
			if (equals(source, target)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 是否全部不相等
	 * 
	 * @param s
	 * @param target
	 * @return
	 */
	public static boolean nequals(Object source, Object... targets) {
		return !equals(source, targets);
	}

	/**
	 * 是否和任意一个不相等
	 * 
	 * @param s
	 * @param target
	 * @return
	 */
	public static boolean nin(Object source, Object... targets) {
		return !in(source, targets);
	}

	/**
	 * 参数校验(出现空值抛异常)
	 * 
	 * @param objs 参数列表
	 * @return
	 * @throws Exception
	 */
	public static boolean doValidate(Object... objs) throws Exception {
		if (objs == null || objs.length == 0) {
			BusinessException.throwException("参数校验失败");
		}
		for (int i = 0; i < objs.length; i++) {
			if (isEmpty(objs[i])) {
				BusinessException.throwException("第" + (i + 1) + "个参数为空");
			}
		}
		return true;
	}

	/**
	 * 获取参数对象的hashcode
	 * 
	 * @param e
	 * @return
	 */
	public static int hashCode(Object obj) {
		return obj == null ? null : obj.hashCode();
	}
}
