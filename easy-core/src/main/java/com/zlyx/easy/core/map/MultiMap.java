package com.zlyx.easy.core.map;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import com.zlyx.easy.core.collections.Lists;
import com.zlyx.easy.core.utils.StringUtils;

/**
 * <p>
 * 等价于Map《String, Collection《V》》, 功能增强
 * </p>
 *
 * @author 赵光
 * @since 2020年1月10日
 */
public class MultiMap<V> extends EasyMap<String, List<V>> {

	private static final long serialVersionUID = 1L;

	public MultiMap() {

	}

	public MultiMap<V> addValues(String key, @SuppressWarnings("unchecked") V... values) {
		addValues(key, Arrays.asList(values));
		return this;
	}

	@SuppressWarnings("unchecked")
	public MultiMap<V> addValues(String key, Collection<V> values) {
		List<V> list = get(key);
		if (list == null) {
			list = Lists.newList();
		}
		list.addAll(values);
		put(key, list);
		return this;
	}

	/**
	 * newMap
	 * 
	 * @param <V>
	 * @return
	 */
	public static <V> MultiMap<V> newMap() {
		return new MultiMap<V>();
	}

	/**
	 * newMap
	 * 
	 * @param key
	 * @param values
	 */
	public static <V> MultiMap<V> newMap(String key, @SuppressWarnings("unchecked") V... values) {
		return new MultiMap<V>().addValues(key, Arrays.asList(values));
	}

	/**
	 * newMap
	 * 
	 * @param <V>
	 * @param key
	 * @param vList
	 * @return
	 */
	public static <V> MultiMap<V> newMap(String key, List<V> vList) {
		return new MultiMap<V>().addValues(key, vList);
	}

	/**
	 * newMap
	 * 
	 * @param params
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static <K, V> MultiMap<V> newMap(Map<K, V> params) {
		MultiMap<V> map = new MultiMap<>();
		params.forEach((key, value) -> {
			map.addValues(StringUtils.valueOf(key), value);
		});
		return map;
	}

}