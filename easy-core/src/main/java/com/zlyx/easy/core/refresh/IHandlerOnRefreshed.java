package com.zlyx.easy.core.refresh;

import org.springframework.context.ApplicationContext;

/**
 * <p>
 * 系统刷新事件定义接口
 * </p>
 *
 * @author 赵光
 * @since 2018年6月10日
 */
public interface IHandlerOnRefreshed {

	/**
	 * 系统刷新事件(尽可能不要抛异常，在doOnRefreshed()方法里处理掉异常)
	 * 
	 * @param context
	 * @throws Exception
	 */
	void doOnRefreshed(ApplicationContext context) throws Exception;
}
