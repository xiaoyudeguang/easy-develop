package com.zlyx.easy.core.utils;

import java.lang.annotation.Annotation;
import java.util.Collection;
import java.util.Map;
import java.util.Set;

import com.zlyx.easy.core.map.Maps;

/**
 * <p>
 * 注解操作工具类
 * </p>
 *
 * @author 赵光
 * @since 2019年1月5日
 */
public final class AnnoUtils {

	/**
	 * 解析获取参数类上所有注解
	 * 
	 * @param cls
	 * @return
	 */
	public static Map<String, Class<? extends Annotation>> findAnnotationsWithName(Class<?> cls) {
		Map<String, Class<? extends Annotation>> annotationMap = Maps.newMap();
		for (Annotation annotation : cls.getAnnotations()) {
			annotationMap.put(annotation.annotationType().getSimpleName(), annotation.annotationType());
		}
		return annotationMap;
	}

	/**
	 * 解析获取参数类上所有注解
	 * 
	 * @param cls
	 * @return
	 */
	public static Collection<Class<? extends Annotation>> findAnnotations(Class<?> cls) {
		Map<String, Class<? extends Annotation>> annotationMap = findAnnotationsWithName(cls);
		return annotationMap == null ? null : annotationMap.values();
	}

	/**
	 * 解析获取参数类上所有注解名称
	 * 
	 * @param cls
	 * @return
	 */
	public static Set<String> findAnnotationNames(Class<?> cls) {
		Map<String, Class<? extends Annotation>> annotationMap = findAnnotationsWithName(cls);
		return annotationMap == null ? null : annotationMap.keySet();
	}

}
