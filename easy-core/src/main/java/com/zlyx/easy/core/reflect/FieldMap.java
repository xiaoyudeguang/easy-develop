package com.zlyx.easy.core.reflect;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

/**
 * <p>
 * 类属性视图
 * </p>
 *
 * @author 赵光
 * @since 2020年6月10日
 */
public class FieldMap extends HashMap<String, Field> {

	private static final long serialVersionUID = 1L;

	private Class<?> cls;

	/**
	 * @param cls
	 */
	public FieldMap(Class<?> cls) {
		this.cls = cls;
		while (cls != null) {
			addsClass(cls);
			cls = cls.getSuperclass();
		}
	}

	/**
	 * 添加属性(自动排除静态和final修饰的属性)
	 * 
	 * @param field
	 */
	public void addField(Field field) {
		if (!Modifier.isStatic(field.getModifiers()) && !Modifier.isFinal(field.getModifiers())) {
			put(field.getName(), field);
		}
	}

	/**
	 * 添加父类属性
	 * 
	 * @param field
	 */
	public void addsClass(Class<?> cls) {
		if (cls.isAssignableFrom(this.cls)) {
			List<Field> fields = FieldUtils.getFields(cls);
			if (fields != null) {
				for (Field field : fields) {
					addField(field);
				}
			}
		}
	}

	/**
	 * 判断是否包含属性
	 * 
	 * @param fieldName
	 * @return
	 */
	public boolean contains(String name) {
		return containsKey(name);
	}

	/**
	 * 返回所有属性名
	 * 
	 * @return
	 */
	public Set<String> names() {
		return keySet();
	}

	/**
	 * 返回所有属性
	 * 
	 * @return
	 */
	public List<Field> fields() {
		return new ArrayList<Field>(values());
	}

	/**
	 * @return the cls
	 */
	public Class<?> getCls() {
		return cls;
	}

	/**
	 * @param cls the cls to set
	 */
	public void setCls(Class<?> cls) {
		this.cls = cls;
	}
}