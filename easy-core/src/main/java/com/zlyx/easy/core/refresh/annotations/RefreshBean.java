package com.zlyx.easy.core.refresh.annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.springframework.core.annotation.AliasFor;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * <p>
 * 系统刷新事件Bean
 * </p>
 *
 * @author 赵光
 * @since 2018年6月10日
 */
@Documented
@Target({ ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
@Component
@Order(5)
@Inherited
public @interface RefreshBean {

	/**
	 * bean名称
	 * 
	 * @return
	 */
	@AliasFor(annotation = Component.class, attribute = "value")
	String value() default "";

	/**
	 * 启动优先级(可重复但不能大于10)
	 * 
	 * @return
	 */
	@AliasFor(annotation = Order.class, attribute = "value")
	int order() default 5;

	/**
	 * 作用
	 * 
	 * @return
	 */
	String[] todo();

}
