package com.zlyx.easy.core.utils;

import java.util.Random;
import java.util.UUID;

/**
 * <p>
 * 随机数工具类
 * </p>
 *
 * @author 赵光
 * @since 2019年4月8日
 */
public class RandomUtils {
	/**
	 * 原始UUID
	 * 
	 * @return
	 */
	public static String randomUUID() {
		return UUID.randomUUID().toString();
	}

	/**
	 * 简化UUID
	 * 
	 * @return
	 */
	public static String randomID() {
		return randomUUID().replace("-", "");
	}

	/**
	 * 生成随机数
	 * 
	 * @param count
	 * @return
	 */
	public static int randomNum(int bound) {
		return new Random().nextInt(bound);
	}

	/**
	 * 生成随机数
	 * 
	 * @param count
	 * @return
	 */
	public static int randomNum() {
		Random r = new Random(System.currentTimeMillis());
		return r.nextInt(Integer.MAX_VALUE);
	}
}
