package com.zlyx.easy.core.collections;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

/**
 * <p>
 * List封装类
 * </p>
 *
 * @author 赵光
 * @since 2018年11月27日
 */
public class EasyList<E> extends ArrayList<E> {

	private static final long serialVersionUID = 1L;

	public EasyList() {
	}

	public EasyList(Collection<E> c) {
		super(c);
	}

	@SuppressWarnings("unchecked")
	public EasyList<E> addAll(E... es) {
		this.addAll(Arrays.asList(es));
		return this;
	}

	/**
	 * 不包含
	 * 
	 * @param o
	 * @return
	 */
	public boolean without(Object o) {
		return !contains(o);
	}

	/**
	 * 非空
	 * 
	 * @return
	 */
	public boolean isNotEmpty() {
		return !isEmpty();
	}

}
