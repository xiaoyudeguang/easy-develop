package com.zlyx.easy.core.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

/**
 * <p>
 * 容器工具类
 * </p>
 *
 * @author 赵光
 * @since 2018年11月27日
 */
@Configuration
@EnableAspectJAutoProxy
@ComponentScan("com.zlyx.easy")
public class AppConfig {

	private static String name;

	public AppConfig(@Value("${spring.application.name:}") String name) {
		AppConfig.name = name;
	}

	/**
	 * @return the name
	 */
	public static String getName() {
		return name;
	}

}
