package com.zlyx.easy.access.interfaces;

import java.lang.reflect.Method;

/**
 * @Auth 赵光
 * @Describle 数据层交互接口
 * @2019年1月11日 下午5:37:20
 */
public interface IDataAccesser {

	/**
	 * 数据层交互方法
	 * 
	 * @param tClass 泛型类
	 * @param method 接口方法
	 * @param params 接口参数
	 * @return
	 * @throws Exception
	 */
	Object doAccess(Class<?> tClass, Method method, Object[] args) throws Exception;
}
