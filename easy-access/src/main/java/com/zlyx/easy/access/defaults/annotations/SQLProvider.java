package com.zlyx.easy.access.defaults.annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @Auth 赵光
 * @Describle
 * @2019年1月13日 下午6:47:45
 */
@Documented
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface SQLProvider {

	/**
	 * 方法
	 * @return
	 */
	String method();

	/**
	 * 类
	 * @return
	 */
	Class<?> type();

	
}
