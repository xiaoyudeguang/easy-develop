package com.zlyx.easy.access.interceptors;

import java.lang.reflect.Method;

/**
 * @Auth 赵光
 * @Describle SQL执行前过滤器(方法级别)
 * @2019年1月11日 上午11:16:10
 */
public interface SqlInterceptor {

	/**
	 * SQL拦截器
	 * @param method
	 * @param sql
	 * @return
	 */
	public String doInterceptor(Method method, String sql);
}
