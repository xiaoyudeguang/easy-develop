package com.zlyx.easy.access.accesser.provider;

import com.zlyx.easy.core.buffer.EasyBuffer;

/**
 * @Auth 赵光
 * @Describle
 * @2019年1月13日 下午6:49:46
 */
public class AccessSqlProvider {

	public final static String PAGE = "page";

	public final static String SQL = "sql";

	public String page(String sql, int pageNum, int pageSize) {
		return EasyBuffer.wrapper(sql, " limit ", (pageNum - 1) * pageSize, ",", pageNum * pageSize);
	}

	public String sql(String sql) {
		return sql;
	}
}
