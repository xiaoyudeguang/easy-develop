package com.zlyx.easy.access.abstracts;

import java.lang.reflect.Method;
import java.sql.Connection;

import org.springframework.beans.factory.annotation.Autowired;

import com.zlyx.easy.access.defaults.ConnectionManager;
import com.zlyx.easy.access.interfaces.IDataAccesser;
import com.zlyx.easy.core.loggers.Logger;
import com.zlyx.easy.core.utils.StringUtils;

/**
 * @Auth 赵光
 * @Describle
 * @2019年1月12日 下午12:28:43
 */
public abstract class AbstractDataAccesser implements IDataAccesser {

	@Autowired
	private ConnectionManager manager;

	protected AbstractSqlAssembler assembler;

	@Override
	public Object doAccess(Class<?> tClass, Method method, Object[] args) throws Exception {
		this.assembler = getAssembler(tClass, method, args);
		if (StringUtils.isEmpty(assembler.getSql())) {
			Logger.err(method.getDeclaringClass(), "SQL语句为Null,无法继续执行!");
			return null;
		}
		return getSqlExcutor(manager.getConnection()).excute(tClass, assembler);
	}

	/**
	 * 获取SQL编译对象
	 * 
	 * @param method
	 * @param args
	 * @return
	 * @throws Exception
	 */
	public abstract AbstractSqlAssembler getAssembler(Class<?> tClass, Method method, Object[] args) throws Exception;

	/**
	 * 获取SQL执行对象
	 * 
	 * @param conn
	 * @return
	 */
	public abstract AbstractSqlExcutor getSqlExcutor(Connection conn);

}
