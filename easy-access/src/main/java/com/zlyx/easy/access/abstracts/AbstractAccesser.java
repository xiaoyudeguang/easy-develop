package com.zlyx.easy.access.abstracts;

import java.util.List;
import java.util.Map;

import com.zlyx.easy.access.defaults.annotations.Delete;
import com.zlyx.easy.access.defaults.annotations.Insert;
import com.zlyx.easy.access.defaults.annotations.SQL;
import com.zlyx.easy.access.defaults.annotations.Select;
import com.zlyx.easy.access.defaults.annotations.Update;

/**
 * @Auth 赵光
 * @Describle
 * @2019年1月12日 下午4:54:33
 */
public interface AbstractAccesser<T> {

	@Insert(value = SQL.PLACEHOLDER)
	public boolean insert(T t);

	@Insert(value = SQL.PLACEHOLDER)
	public boolean insertBatch(List<T> ts);

	@Update(value = SQL.PLACEHOLDER)
	public boolean updateById(T t);

	@Update(value = SQL.PLACEHOLDER)
	public boolean updateBatchById(List<T> ts);

	@Delete(value = SQL.PLACEHOLDER)
	public boolean deleteById(Object id);

	@Delete(value = SQL.PLACEHOLDER)
	public boolean deleteByIds(List<Object> ids);

	@Select(value = SQL.PLACEHOLDER)
	public T selectOne(Map<String, Object> params);

	@Select(value = SQL.PLACEHOLDER)
	public List<T> selectList(Map<String, Object> params);

}
