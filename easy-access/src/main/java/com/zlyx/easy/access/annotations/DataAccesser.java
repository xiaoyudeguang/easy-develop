package com.zlyx.easy.access.annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.springframework.core.annotation.AliasFor;
import org.springframework.stereotype.Component;

/**
 * @Auth 赵光
 * @Describle 自定义数据交互层实现注解
 * @2019年1月11日 下午4:49:40
 */
@Documented
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Component
public @interface DataAccesser {

   /**
    * 数据访问层唯一名称
    * @return
    */
   @AliasFor(annotation = Component.class, attribute = "value")
   String value();
   
   /**
    * 作用描述
    * @return
    */
   String[] todo();
}
