package com.zlyx.easy.access.annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @Auth 赵光
 * @Describle 数据交互接口(类似于Mybatis的Mapper接口)
 * @2019年1月11日 下午4:49:40
 */
@Documented
@Target({ ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
public @interface EasyAccesser {

	/**
	 * 声明使用哪个数据交互层实现 如果系统中只有一个自定义数据交互层实现，可以不设置value值； 如果系统中没有自定义数据交互层实现，将使用默认数据交互层实现
	 * 
	 * @return
	 */
	String value() default "";

	/**
	 * 作用描述
	 * 
	 * @return
	 */
	String[] todo();
}
