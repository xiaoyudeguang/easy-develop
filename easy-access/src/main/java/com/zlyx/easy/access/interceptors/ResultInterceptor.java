package com.zlyx.easy.access.interceptors;

import com.zlyx.easy.access.abstracts.AbstractSqlAssembler;

/**
 * @Auth 赵光
 * @Describle
 * @2019年1月12日 上午11:53:05
 */
public interface ResultInterceptor {

	/**
	 * SQL拦截器
	 * @param assembler SQL打包器
	 * @param res  结果集对象
	 * @return
	 */
	public String doInterceptor(AbstractSqlAssembler assembler, Object res);
}
