package com.zlyx.easy.access.defaults;

import java.lang.reflect.Method;
import java.sql.Connection;

import com.zlyx.easy.access.abstracts.AbstractDataAccesser;
import com.zlyx.easy.access.abstracts.AbstractSqlAssembler;
import com.zlyx.easy.access.abstracts.AbstractSqlExcutor;
import com.zlyx.easy.access.annotations.DataAccesser;

/**
 * @Auth 赵光
 * @Describle
 * @2019年1月12日 上午8:57:46
 */
@DataAccesser(value = "defaultAccesser", todo = { "默认数据交互层实现" })
public class DefaultDataAccesser extends AbstractDataAccesser {

	@Override
	public AbstractSqlAssembler getAssembler(Class<?> tClass, Method method, Object[] args) throws Exception {
		return new DefaultSqlAssembler(tClass, method, args);
	}

	@Override
	public AbstractSqlExcutor getSqlExcutor(Connection conn) {
		return new DefaultSqlExcutor(conn);
	}
}
