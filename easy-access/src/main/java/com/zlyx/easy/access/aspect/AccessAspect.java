package com.zlyx.easy.access.aspect;

import java.lang.reflect.Method;
import java.util.List;

import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.stereotype.Component;

import com.zlyx.easy.access.accesser.AccessAccesser;
import com.zlyx.easy.access.annotations.EasyAccesser;
import com.zlyx.easy.core.loggers.Logger;
import com.zlyx.easy.core.map.EasyMap;
import com.zlyx.easy.core.utils.ObjectUtils;
import com.zlyx.easy.database.annotations.EasySelect;
import com.zlyx.easy.database.aspects.AbstractSelectAspect;
import com.zlyx.easy.database.local.Page;
import com.zlyx.easy.database.local.ReturnType;

@Aspect
@Component
public class AccessAspect extends AbstractSelectAspect {

	@Autowired
	private AccessAccesser accessAccesser;

	@Override
	public Object doAspect(Method method, boolean isPage, String sql, EasyMap<String, Object> paramsMap) {
		if (isPage) {
			if (method.getReturnType() == Page.class) {
				String countSql = sql.replace(sql.substring(sql.indexOf("select") + 6, sql.indexOf("from")),
						" count(*) ");
				ReturnType.clear();
				Object count = accessAccesser.selectOne(countSql);
				if (ObjectUtils.isEmpty(count) || (Long) count == 0) {
					Logger.warn(method.getDeclaringClass(), "No results return!");
					return null;
				}
				ReturnType.setType(method);
				List<?> data = accessAccesser.page(sql, paramsMap.getValue(EasySelect.PAGENUM),
						paramsMap.getValue(EasySelect.PAGSIZE));
				return new Page<>(data, (Long) count);
			} else {
				return accessAccesser.page(sql, paramsMap.getValue(EasySelect.PAGENUM),
						paramsMap.getValue(EasySelect.PAGSIZE));
			}
		}
		return accessAccesser.select(sql);
	}

	@Override
	public boolean isAspect(Method method) {
		return AnnotationUtils.findAnnotation(method.getDeclaringClass(), EasyAccesser.class) != null;
	}
}
