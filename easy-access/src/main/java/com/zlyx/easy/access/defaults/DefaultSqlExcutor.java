package com.zlyx.easy.access.defaults;

import java.sql.Connection;

import com.zlyx.easy.access.abstracts.AbstractSqlExcutor;

public class DefaultSqlExcutor extends AbstractSqlExcutor {

	public DefaultSqlExcutor(Connection conn) {
		super(conn);
	}
}