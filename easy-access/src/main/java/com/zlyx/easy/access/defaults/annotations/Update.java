package com.zlyx.easy.access.defaults.annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @Auth 赵光
 * @Describle 
 * @2019年1月11日 下午4:49:40
 */
@Documented
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface Update {

	/**
	 *  
	 * @return sql
	 */
   String value();
}
