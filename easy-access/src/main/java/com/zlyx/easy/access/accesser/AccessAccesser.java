package com.zlyx.easy.access.accesser;

import java.util.List;

import com.zlyx.easy.access.accesser.provider.AccessSqlProvider;
import com.zlyx.easy.access.annotations.EasyAccesser;
import com.zlyx.easy.access.defaults.annotations.SQLProvider;

/**
 * 数据访问层
 * 
 * @Auth 赵光
 * @Describle
 * @2019年1月13日 下午5:43:31
 */
@EasyAccesser(todo = { "数据交互通用访问接口" })
public interface AccessAccesser {

	/**
	 * 分页
	 * 
	 * @param sql
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	@SQLProvider(method = AccessSqlProvider.PAGE, type = AccessSqlProvider.class)
	public List<?> page(String sql, int pageNum, int pageSize);

	/**
	 * 查询
	 * 
	 * @param sql
	 * @return
	 */
	@SQLProvider(method = AccessSqlProvider.SQL, type = AccessSqlProvider.class)
	public List<?> select(String sql);

	/**
	 * 查询
	 * 
	 * @param sql
	 * @return
	 */
	@SQLProvider(method = AccessSqlProvider.SQL, type = AccessSqlProvider.class)
	public Object selectOne(String sql);

	/**
	 * 更新
	 * 
	 * @param sql
	 * @return
	 */
	@SQLProvider(method = AccessSqlProvider.SQL, type = AccessSqlProvider.class)
	public int update(String sql);

	/**
	 * 删除
	 * 
	 * @param sql
	 * @return
	 */
	@SQLProvider(method = AccessSqlProvider.SQL, type = AccessSqlProvider.class)
	public int delete(String sql);

	/**
	 * 新增
	 * 
	 * @param sql
	 * @return
	 */
	@SQLProvider(method = AccessSqlProvider.SQL, type = AccessSqlProvider.class)
	public int insert(String sql);
}
