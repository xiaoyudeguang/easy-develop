package com.zlyx.easy.access.supports;

/**
 * @Auth 赵光
 * @Describle 原子方法
 * @2019年1月12日 下午6:06:56
 */
public class CurdMethod{

	public final static String insert = "insert";

	public final static String insertBatch = "insertBatch";

	public final static String updateById = "updateById";

	public final static String updateBatchById = "updateBatchById";

	public final static String deleteById = "deleteById";

	public final static String deleteByIds = "deleteByIds";

	public final static String selectOne = "selectOne";
	
	public final static String selectList = "selectList";

	public final static String selectPage = "selectPage";
}
