package com.zlyx.easy.access.interfaces;

import java.sql.Connection;

/**
 * @Auth 赵光
 * @Describle 数据库连接接口
 * @2019年1月12日 上午10:12:32
 */
public interface DBConnection {

	/**
	 * 获取数据库连接
	 * @return
	 */
	Connection getConn();
}
