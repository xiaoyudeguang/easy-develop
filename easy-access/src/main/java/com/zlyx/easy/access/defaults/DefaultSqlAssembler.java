package com.zlyx.easy.access.defaults;

import java.lang.reflect.Method;

import com.zlyx.easy.access.abstracts.AbstractSqlAssembler;
import com.zlyx.easy.core.buffer.EasyBuffer;
import com.zlyx.easy.core.map.EasyMap;

public class DefaultSqlAssembler extends AbstractSqlAssembler {

	public DefaultSqlAssembler(Class<?> tClass, Method method, Object[] args) throws Exception {
		super(tClass, method, args);
	}

	@Override
	public String assemble(EasyMap<String, Object> paramsMap, String sql) {
		for (String key : paramsMap.keySet()) {
			sql = sql.replace(EasyBuffer.wrapper("${", key, "}"), key);
			sql = sql.replace(EasyBuffer.wrapper("#{", key, "}"), key);
		}
		return sql;
	}
}