package com.zlyx.easy.mybatis.configuration;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

/**
 * @Auth 赵光
 * @Describle
 * @2019年1月7日 下午1:48:45
 */
@Configuration
@MapperScan("com.zlyx.easy.mybatis.mapper")
public class MybatisConfiguration {

}
