package com.zlyx.easy.mybatis.configuration;

import org.apache.ibatis.annotations.Mapper;
import org.mybatis.spring.mapper.ClassPathMapperScanner;
import org.mybatis.spring.mapper.MapperFactoryBean;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.BeanDefinitionRegistryPostProcessor;
import org.springframework.context.ResourceLoaderAware;
import org.springframework.context.annotation.AnnotationBeanNameGenerator;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;

import com.zlyx.easy.core.app.AppUtils;

/**
 * 
 * 
 * @Auth 赵光
 * @Describle
 * @2019年1月3日 下午2:54:41
 */
@Component
public class DefaultMapperScannerRegistrar implements BeanDefinitionRegistryPostProcessor, ResourceLoaderAware {

	private ResourceLoader resourceLoader;

	@SuppressWarnings("deprecation")
	@Override
	public void postProcessBeanDefinitionRegistry(BeanDefinitionRegistry registry) throws BeansException {
		ClassPathMapperScanner scanner = new ClassPathMapperScanner(registry);
		scanner.setResourceLoader(resourceLoader);
		scanner.setAnnotationClass(Mapper.class);
		scanner.setBeanNameGenerator(BeanUtils.instantiateClass(AnnotationBeanNameGenerator.class));
		scanner.setMapperFactoryBean(BeanUtils.instantiateClass(MapperFactoryBean.class));
		scanner.registerFilters();
		scanner.doScan(AppUtils.getMainPackagePath());
	}

	@Override
	public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setResourceLoader(ResourceLoader resourceLoader) {
		this.resourceLoader = resourceLoader;
	}

}
