package com.zlyx.easy.mybatis.annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.apache.ibatis.annotations.Mapper;

/**
 * 增强型@Mapper注解
 * 
 * @author 赵光
 * @version 创建时间：2018年10月29日 上午8:55:45
 * @ClassName 类名称
 * @Description 类描述
 */
@Documented
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Mapper
public @interface EasyMapper {

	/**
	 * 作用
	 * 
	 * @return
	 */
	String todo();
}
