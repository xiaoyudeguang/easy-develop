package com.zlyx.easy.mybatis.mapper;

import java.util.List;

import org.apache.ibatis.annotations.DeleteProvider;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.UpdateProvider;

import com.zlyx.easy.mybatis.sql.MybatisProvider;

/**
 * @Auth 赵光
 * @Describle 拥有直接执行SQL语句的通用Mapper接口
 * @2019年1月7日 下午12:55:56
 */
@Mapper
public interface MybatisMapper {

	@SelectProvider(method = "pageSql", type = MybatisProvider.class)
	public List<?> page(String sql, int pageNum, int pageSize);

	@SelectProvider(method = "sql", type = MybatisProvider.class)
	public List<?> select(String sql);

	@SelectProvider(method = "sql", type = MybatisProvider.class)
	public Object selectOne(String sql);

	@UpdateProvider(method = "sql", type = MybatisProvider.class)
	public int update(String sql);

	@DeleteProvider(method = "sql", type = MybatisProvider.class)
	public int delete(String sql);

	@InsertProvider(method = "sql", type = MybatisProvider.class)
	public int insert(String sql);

//	default String pageSql(String sql, int pageNum, int pageSize) {
//		return EasyBuffer.wrapper(sql, " limit ", (pageNum - 1) * pageSize, ",", pageNum * pageSize);
//	}
//
//	default String sql(String sql) {
//		return sql;
//	}
}
