package com.zlyx.easy.mybatis.sql;

import com.zlyx.easy.core.buffer.EasyBuffer;

/**
 * @Auth 赵光
 * @Describle 拥有直接执行SQL语句的通用Mapper接口
 * @2019年1月7日 下午12:55:56
 */
public class MybatisProvider {

	public String pageSql(String sql, int pageNum, int pageSize) {
		pageNum = pageNum < 0 ? 0 : pageNum;
		if (pageSize == -1) {
			return sql;
		}
		return EasyBuffer.wrapper(sql, " limit ", (pageNum - 1) * pageSize, ",", pageNum * pageSize);
	}

	public String sql(String sql) {
		return sql;
	}

}
