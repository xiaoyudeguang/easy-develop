package com.zlyx.easy.api.store;

/**
 * 文档缓存接口
 * 
 * @Auth 赵光
 * @Describle
 */
public interface ApiCache {

	/**
	 * 是否启用文档缓存(启用时将不重新扫描文件，直接以缓存内容作为文档)
	 * 
	 * @return
	 */
	public boolean isUse();

	/**
	 * 缓存文档
	 * 
	 * @param input
	 */
	public void saveApi(String input);

	/**
	 * 读取缓存文档
	 * 
	 * @return
	 */
	public String getApis();

}
