package com.zlyx.easy.api.context;

import java.util.List;

import io.swagger.models.Swagger;
import io.swagger.models.parameters.Parameter;

public class ApiContext {

	private final String parentPath = "method-api";

	private Swagger swagger;
	private Class<?> refCls;
	private Class<?> interfaceCls;
	private String parentHttpMethod;
	private boolean readHidden;
	private List<String> parentConsumes;
	private List<String> parentProduces;
	private List<String> parentTags;
	private List<Parameter> parentParameters;

	public ApiContext(Swagger swagger, Class<?> refCls, Class<?> interfaceCls, String parentHttpMethod,
			boolean readHidden, List<String> parentConsumes, List<String> parentProduces, List<String> parentTags,
			List<Parameter> parentParameters) {
		setSwagger(swagger);
		setRefCls(refCls);
		setInterfaceCls(interfaceCls);
		setParentHttpMethod(parentHttpMethod);
		setReadHidden(readHidden);
		setParentConsumes(parentConsumes);
		setParentProduces(parentProduces);
		setParentTags(parentTags);
		setParentParameters(parentParameters);
	}

	public Swagger getSwagger() {
		return swagger;
	}

	public void setSwagger(Swagger swagger) {
		this.swagger = swagger;
	}

	public Class<?> getRefCls() {
		return refCls;
	}

	public void setRefCls(Class<?> cls) {
		this.refCls = cls;
	}

	public Class<?> getInterfaceCls() {
		return interfaceCls;
	}

	public Class<?> getCls() {
		return refCls;
	}

	public void setInterfaceCls(Class<?> interfaceCls) {
		this.interfaceCls = interfaceCls;
	}

	public String getParentPath() {
		return parentPath;
	}

	public String getParentHttpMethod() {
		return parentHttpMethod;
	}

	public void setParentHttpMethod(String parentHttpMethod) {
		this.parentHttpMethod = parentHttpMethod;
	}

	public boolean isReadHidden() {
		return readHidden;
	}

	public void setReadHidden(boolean readHidden) {
		this.readHidden = readHidden;
	}

	public List<String> getParentConsumes() {
		return parentConsumes;
	}

	public void setParentConsumes(List<String> parentConsumes) {
		this.parentConsumes = parentConsumes;
	}

	public List<String> getParentProduces() {
		return parentProduces;
	}

	public void setParentProduces(List<String> parentProduces) {
		this.parentProduces = parentProduces;
	}

	public List<String> getParentTags() {
		return parentTags;
	}

	public void setParentTags(List<String> parentTags) {
		this.parentTags = parentTags;
	}

	public List<Parameter> getParentParameters() {
		return parentParameters;
	}

	public void setParentParameters(List<Parameter> parentParameters) {
		this.parentParameters = parentParameters;
	}

}
