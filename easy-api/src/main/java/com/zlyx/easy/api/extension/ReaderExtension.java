package com.zlyx.easy.api.extension;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.lang.reflect.Type;

import com.zlyx.easy.api.context.ApiContext;
import com.zlyx.easy.api.model.ApiModule;

/**
 * 文档解析1 文档解析2 文档解析3
 * 
 * @Auth 赵光
 * @Describle
 */
public interface ReaderExtension {

	/**
	 * Checks that a resource should be scanned.
	 *
	 * @param context is the resource context
	 * @return true if the resource needs to be scanned, otherwise false
	 */
	boolean isReadable(ApiContext context);

	/**
	 * Reads the consumes from the method's annotations and applies these to the
	 * module.
	 *
	 * @param context is the resource context
	 * @param module  is the container for the module data
	 * @param method  is the method for reading annotations
	 */
	void applyConsumes(ApiContext context, ApiModule module, Method method);

	/**
	 * Reads the produces from the method's annotations and applies these to the
	 * module.
	 *
	 * @param context is the resource context
	 * @param module  is the container for the module data
	 * @param method  is the method for reading annotations
	 */
	void applyProduces(ApiContext context, ApiModule module, Method method);

	/**
	 * Returns http method.
	 *
	 * @param context is the resource context
	 * @param method  is the method for reading annotations
	 * @return http method
	 */
	String getHttpMethod(ApiContext context, Method method);

	/**
	 * Returns module's path.
	 *
	 * @param context is the resource context
	 * @param method  is the method for reading annotations
	 * @return module's path
	 */
	String getPath(ApiContext context, ApiModule module, Method method);

	/**
	 * Reads the summary from the method's annotations and applies it to the module.
	 *
	 * @param module is the container for the module data
	 * @param method is the method for reading annotations
	 */
	void applySummary(ApiModule module, Method method);

	/**
	 * Reads the description from the method's annotations and applies it to the
	 * module.
	 *
	 * @param module is the container for the module data
	 * @param method is the method for reading annotations
	 */
	void applyDescription(ApiModule module, Method method);

	/**
	 * Reads the schemes from the method's annotations and applies these to the
	 * module.
	 *
	 * @param context is the resource context
	 * @param module  is the container for the module data
	 * @param method  is the method for reading annotations
	 */
	void applySchemes(ApiContext context, ApiModule module, Method method);

	/**
	 * Sets the deprecated flag to the module.
	 *
	 * @param module is the container for the module data
	 * @param method is the method for reading annotations
	 */
	void setDeprecated(ApiModule module, Method method);

	/**
	 * Reads the security requirement from the method's annotations and applies
	 * these to the module.
	 *
	 * @param context is the resource context
	 * @param module  is the container for the module data
	 * @param method  is the method for reading annotations
	 */
	void applySecurityRequirements(ApiContext context, ApiModule module, Method method);

	/**
	 * Reads the tags from the method's annotations and applies these to the module.
	 *
	 * @param context is the resource context
	 * @param module  is the container for the module data
	 * @param method  is the method for reading annotations
	 * @return
	 */
	String applyTags(ApiContext context, ApiModule module, Method method);

	/**
	 * Reads the responses from the method's annotations and applies these to the
	 * module.
	 *
	 * @param context is the resource context
	 * @param module  is the container for the module data
	 * @param method  is the method for reading annotations
	 */
	void applyResponses(ApiContext context, ApiModule module, Method method);

	/**
	 * Reads the parameters from the method's annotations and applies these to the
	 * module.
	 *
	 * @param context     is the resource context
	 * @param module      is the container for the module data
	 * @param type        is the type of parameter
	 * @param annotations are the method's annotations
	 */
	void applyParameters(ApiContext context, ApiModule module, Type type, Annotation[] annotations);

	/**
	 * Reads the implicit parameters from the method's annotations and applies these
	 * to the module.
	 *
	 * @param context is the resource context
	 * @param module  is the container for the module data
	 * @param method  is the method for reading annotations
	 */
	void applyImplicitParameters(ApiContext context, ApiModule module, Method method);

	/**
	 * Reads the extensions from the method's annotations and applies these to the
	 * module.
	 *
	 * @param context is the resource context
	 * @param module  is the container for the module data
	 * @param method  is the method for reading annotations
	 */
	void applyExtensions(ApiContext context, ApiModule module, Method method);

	/**
	 * 
	 * @param context
	 * @param module
	 * @param method
	 */
	void applyParameters(ApiContext context, ApiModule module, Method method);

	/**
	 * 
	 * @param operation
	 * @param method
	 */
	void applyOperationId(ApiModule module, Method method);

}
