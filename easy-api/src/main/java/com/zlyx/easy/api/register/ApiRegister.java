package com.zlyx.easy.api.register;

import java.util.Map;

import org.springframework.context.ApplicationContext;

/**
 * @Auth 赵光
 * @Describle
 */
public interface ApiRegister {

	/**
	 * 注册需要生成api的bean
	 * 
	 * @param context
	 * @return
	 */
	Map<Class<?>, Object> register(ApplicationContext applicationContext);
}
