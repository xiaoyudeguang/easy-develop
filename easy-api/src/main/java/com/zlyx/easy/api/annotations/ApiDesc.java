package com.zlyx.easy.api.annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 接口描述注解
 * 
 * @auth 赵光
 * @Desc api描述
 */
@Documented
@Target({ ElementType.TYPE, ElementType.METHOD })
@Retention(RetentionPolicy.RUNTIME)
public @interface ApiDesc {

	/**
	 * 作用简述
	 * 
	 * @return
	 */
	String value();

	/**
	 * 接口作者
	 * 
	 * @return
	 */
	String author() default "";

	/**
	 * 接口负责人
	 * 
	 * @return
	 */
	String leader() default "";

	/**
	 * 编写时间
	 * 
	 * @return
	 */
	String date() default "";

	/**
	 * 详细描述
	 * 
	 * @return
	 */
	String notes() default "";

}
