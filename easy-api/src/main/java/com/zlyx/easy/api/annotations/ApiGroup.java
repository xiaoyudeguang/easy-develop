package com.zlyx.easy.api.annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.springframework.core.annotation.AliasFor;
import org.springframework.stereotype.Component;

/**
 * Api注册注解(不要写在启动类和被切面的类上，会报错)
 * 
 * @auth 赵光
 * @Desc Api注册注解
 */
@Documented
@Target({ ElementType.TYPE, ElementType.METHOD })
@Retention(RetentionPolicy.RUNTIME)
@Component
public @interface ApiGroup {

	/**
	 * bean名称
	 * 
	 * @return
	 */
	@AliasFor(annotation = Component.class, attribute = "value")
	String value() default "";

	/**
	 * 文档分组名称(没有设置统一划分到value下)
	 * 
	 * @return
	 */
	String group() default "";

	/**
	 * 指定生成文档的注解类型或接口/父类
	 * 
	 * @return
	 */
	Class<?>[] clses() default Void.class;

	/**
	 * 作用
	 * 
	 * @return
	 */
	String[] todo();

}
