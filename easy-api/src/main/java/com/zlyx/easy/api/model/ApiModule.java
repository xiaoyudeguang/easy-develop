package com.zlyx.easy.api.model;

import java.lang.reflect.Method;
import java.util.Map;

import com.zlyx.easy.api.annotations.ApiDesc;
import com.zlyx.easy.core.map.Maps;
import com.zlyx.easy.core.utils.DateUtils;
import com.zlyx.easy.core.utils.JsonUtils;
import com.zlyx.easy.core.utils.StringUtils;

import io.swagger.models.Operation;
import io.swagger.util.ReflectionUtils;

public class ApiModule extends Operation implements Cloneable {

	private Map<String, String> params = Maps.newMap();

	private String author;

	private String leader;

	private String date;

	private String tag;

	private String className;

	private String methodName;

	public ApiModule(Method method) {
		this.className = method.getDeclaringClass().getName();
		this.methodName = method.getName();
		setApiDesc(method);
	}

	public void setApiDesc(Method method) {
		ApiDesc methodApiDesc = ReflectionUtils.getAnnotation(method, ApiDesc.class);
		ApiDesc clsApiDesc = ReflectionUtils.getAnnotation(method.getDeclaringClass(), ApiDesc.class);
		if (methodApiDesc != null) {
			if (StringUtils.isEmpty(author)) {
				author = methodApiDesc.author();
			}
			if (StringUtils.isEmpty(leader)) {
				leader = methodApiDesc.leader();
			}
			if (StringUtils.isEmpty(date)) {
				date = methodApiDesc.date();
			}
		}
		if (clsApiDesc != null) {
			if (StringUtils.isEmpty(author)) {
				author = clsApiDesc.author();
			}
			if (StringUtils.isEmpty(leader)) {
				leader = clsApiDesc.leader();
			}
			if (StringUtils.isEmpty(date)) {
				date = clsApiDesc.date();
			}
		}
		if (StringUtils.isEmpty(author)) {
			author = "匿名";
		}
		if (StringUtils.isEmpty(leader)) {
			leader = author;
		}
		if (StringUtils.isEmpty(date)) {
			date = DateUtils.getNow();
		}
	}

	public ApiModule() {
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public String getMethodName() {
		return methodName;
	}

	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}

	public String getTag() {
		return tag;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}

	public String getParam(String name) {
		return params.get(name);
	}

	public Map<String, String> getParams() {
		return params;
	}

	public void addParam(String name, String describle) {
		this.params.put(name, describle);
	}

	@Override
	public String toString() {
		return JsonUtils.toJson(this);
	}

	public String getLeader() {
		return leader;
	}

	public void setLeader(String leader) {
		this.leader = leader;
	}
}
