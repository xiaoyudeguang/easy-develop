package com.zlyx.easy.database.local;

import java.util.List;
import java.util.Map;

/**
 * @Auth 赵光
 * @Describle
 * @2019年1月11日 上午10:19:55
 */
public class Page<T> {

	/**
	 * 当前页数
	 */
	private Integer pageNum = 1;

	/**
	 * 分页大小
	 */
	private Integer pageSize = 10;

	/**
	 * 分页参数
	 */
	private Map<String, Object> condition;

	/**
	 * 数据总数
	 */
	private Long total;

	/**
	 * 数据
	 */
	private List<T> data;

	/**
	 * 升序字段
	 */
	private String[] ascs;

	/**
	 * 降序字段
	 */
	private String[] descs;

	public Page() {

	}

	@SuppressWarnings("unchecked")
	public Page(List<?> data, Long total) {
		this.data = (List<T>) data;
		this.total = total;
	}

	public Long getTotal() {
		return total;
	}

	public void setTotal(Long total) {
		this.total = total;
	}

	public List<T> getData() {
		return data;
	}

	public void setData(List<T> data) {
		this.data = data;
	}

	public Integer getPageNum() {
		return pageNum;
	}

	public void setPageNumber(Integer pageNum) {
		this.pageNum = pageNum;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	public Map<String, Object> getCondition() {
		return condition;
	}

	public void setCondition(Map<String, Object> condition) {
		this.condition = condition;
	}

	public String[] getAscs() {
		return ascs;
	}

	public void setAscs(String[] ascs) {
		this.ascs = ascs;
	}

	public String[] getDescs() {
		return descs;
	}

	public void setDescs(String[] descs) {
		this.descs = descs;
	}
}
