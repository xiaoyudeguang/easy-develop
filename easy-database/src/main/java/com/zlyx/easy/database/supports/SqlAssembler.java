package com.zlyx.easy.database.supports;

import com.zlyx.easy.core.loggers.Logger;
import com.zlyx.easy.core.map.EasyMap;
import com.zlyx.easy.database.enums.SqlType;

/**
 * @Auth 赵光
 * @Describle SQL打包器
 * @2019年1月7日 上午11:15:03
 */
public final class SqlAssembler {

	public static String assemble(Object target, EasyMap<String, String> sqlElements) {
		if (sqlElements.isNotEmpty() && sqlElements.containsKey(SqlSupport.TYPENAME)) {
			if (sqlElements.withoutKey(SqlSupport.TABLENAME)) {
				Logger.err(target.getClass(), "TableName Can't been null!");
				return null;
			}
			SqlType type = SqlType.valueOf(sqlElements.getValue(SqlSupport.TYPENAME));
			if (SqlType.Select == type) {
				return Select(sqlElements);
			} else if (SqlType.Delete == type) {
				return delete(target, sqlElements);
			} else if (SqlType.Update == type) {
				return update(target, sqlElements);
			} else if (SqlType.Insert == type) {
				return insert(target, sqlElements);
			}
		}
		return null;
	}

	private static String Select(EasyMap<String, String> sqlElements) {
		if (sqlElements.withoutKey(SqlSupport.RESULTS) || "".equals(sqlElements.get(SqlSupport.RESULTS))) {
			sqlElements.put(SqlSupport.RESULTS, "*");
		}
		if (sqlElements.withoutKey(SqlSupport.CONDITON)) {
			sqlElements.put(SqlSupport.CONDITON, "");
		}
		return SqlSupport.SELECT.replace(SqlSupport.RESULTS, sqlElements.get(SqlSupport.RESULTS))
				.replace(SqlSupport.TABLENAME, sqlElements.get(SqlSupport.TABLENAME))
				.replace(SqlSupport.CONDITON, sqlElements.get(SqlSupport.CONDITON));
	}

	private static String delete(Object target, EasyMap<String, String> sqlElements) {
		if (sqlElements.withoutKey(SqlSupport.CONDITON)) {
			sqlElements.put(SqlSupport.CONDITON, "");
		}
		return SqlSupport.DELETE.replace(SqlSupport.TABLENAME, sqlElements.get(SqlSupport.TABLENAME))
				.replaceAll(SqlSupport.CONDITON, sqlElements.get(SqlSupport.CONDITON));
	}

	private static String update(Object target, EasyMap<String, String> sqlElements) {
		if (sqlElements.withoutKey(SqlSupport.CONTENT)) {
			Logger.err(target.getClass(), "Content Can't been null!");
			return null;
		}
		if (sqlElements.withoutKey(SqlSupport.CONDITON)) {
			sqlElements.put(SqlSupport.CONDITON, "");
		}
		return SqlSupport.UPDATE.replace(SqlSupport.TABLENAME, sqlElements.get(SqlSupport.TABLENAME))
				.replace(SqlSupport.CONTENT, sqlElements.get(SqlSupport.CONTENT))
				.replaceAll(SqlSupport.CONDITON, sqlElements.get(SqlSupport.CONDITON));
	}

	private static String insert(Object target, EasyMap<String, String> sqlElements) {
		if (sqlElements.withoutKey(SqlSupport.CONTENT)) {
			Logger.err(target.getClass(), "Content Can't been null!");
			return null;
		}
		return SqlSupport.INSERT.replace(SqlSupport.TABLENAME, sqlElements.get(SqlSupport.TABLENAME))
				.replace(SqlSupport.CONTENT, sqlElements.get(SqlSupport.CONTENT));
	}

}
