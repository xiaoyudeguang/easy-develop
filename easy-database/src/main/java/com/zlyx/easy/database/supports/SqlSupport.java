package com.zlyx.easy.database.supports;

import com.zlyx.easy.database.enums.SqlType;

/**
 * @Auth 赵光
 * @Describle
 * @2019年1月7日 上午10:40:42
 */
public class SqlSupport {

	public static String UPDATE = "update ${tableName} set ${content} ${condition}";

	public static String DELETE = "delete from ${tableName} ${condition}";

	public static String SELECT = "select ${results} from ${tableName} ${condition}";

	public static String INSERT = "insert into ${tableName}(${names}) ${content}";

	public static final String TABLENAME = "${tableName}";

	public static final String CONDITON = "${condition}";

	public static final String CONTENT = "${content}";

	public static final String NAMES = "${names}";

	public static final String RESULTS = "${results}";

	public static final String WHERE = " where ";

	public static final String AND = "(${AND})";

	public static final String OR = "(${OR})";

	public static final String PREFIX = "(${PREFIX})";

	public static final String HEAD = "{";

	public static final String END = ")";

	public static final String TYPENAME = SqlType.class.getSimpleName();

}
