package com.zlyx.easy.database.annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 
 * @Auth 赵光
 * @Describle
 * @2018年12月22日 下午4:37:56
 */
@Target({ ElementType.METHOD })
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface EasySelect {

	public static final String FILED_ALL = "*";

	public final static String PAGENUM = "pageNum";

	public final static String PAGSIZE = "pageSize";

	/**
	 * 实体类
	 * 
	 * @return
	 */
	Class<?> tableCls();

	/**
	 * 结果集
	 * 
	 * @return
	 */
	String[] results() default FILED_ALL;

	/**
	 * where条件(用and连接)
	 */
	String[] where() default "";

	/**
	 * 分页(需要在参数中存在两个参数:pageNum(当前页数)和pageSize(分页大小))
	 * 
	 * @return
	 */
	boolean page() default false;

	/**
	 * 作用描述
	 * 
	 * @return
	 */
	String[] todo() default "";

}
