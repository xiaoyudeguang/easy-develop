package com.zlyx.easy.database.enums;

import com.zlyx.easy.core.map.EasyMap;
import com.zlyx.easy.core.map.Maps;

public enum SqlType {

	Select, Insert, Update, Delete, Count;

	private static EasyMap<String, SqlType> types = Maps.newMap();

	static {
		for (SqlType type : values()) {
			types.put(type.name(), type);
		}
	}

	public SqlType getSqlType(String typeName) {
		return types.getValue(typeName);
	}
}