package com.zlyx.easy.database.utils;

import java.lang.reflect.Field;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.zlyx.easy.core.tool.CamelTool;

/**
 * @Auth 赵光
 * @Describle
 * @2019年1月9日 下午8:40:39
 */
public class TableUtils {

	/**
	 * 根据实体类获取别名
	 * 
	 * @param tableName
	 * @return
	 */
	public static String getTableName(Class<?> cls) {
		Table table = cls.getAnnotation(Table.class);
		if (table != null && !"".equals(table.name())) {
			return table.name();
		}
		Entity entity = cls.getAnnotation(Entity.class);
		if (entity != null && !"".equals(entity.name())) {
			return entity.name();
		}
		return CamelTool.camel2Underline(cls.getSimpleName());
	}

	/**
	 * 获得字段名称
	 * 
	 * @param field
	 * @return
	 */
	public static String getColumnName(Field field) {
		Column column = field.getAnnotation(Column.class);
		if (column != null && !"".equals(column.name())) {
			return column.name();
		}
		return CamelTool.camel2Underline(field.getName());
	}
}
