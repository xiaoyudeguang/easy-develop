package com.zlyx.easy.database.interfaces;

import java.lang.reflect.Method;

import com.zlyx.easy.database.supports.SqlFactory;

/**
 * @Auth 赵光
 * @Describle SQL编译前过滤器(方法级别)
 * @2019年1月11日 上午11:16:10
 */
public interface SqlFilter {

	/**
	 * 你可以实现doFilter方法在真正编译SQL前做一些定制的处理
	 * @param factory SqlFactory工厂
	 * @param method 接口方法
	 * @return
	 */
	 SqlFactory doFilter(SqlFactory factory, Method method);
}
