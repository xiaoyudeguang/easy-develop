package com.zlyx.easy.database.enums;

public enum MatchType{
	
	EQUAL(" @{s}.${s} = #{s}"), 
	GTHAN(" @{s}.${s} = #{s}"),
	LTHAN(" @{s}.${s} = #{s}"), 
	NOTIN(" @{s}.$(s) > $(s)"),
	BETWEEN(" @{s}.${s} > #{s}"),
	LIKE(" @{s}.${s} like '%#{s}%'"), 
	LLIKE(" @{s}.${s} like '%#{s}'"), 
	RLIKE(" @{s}.${s} like '#{s}%'");

	private String symbol;
	
	private MatchType(String symbol) {
		this.symbol = symbol;
	}

	public String getSymbol() {
		return this.symbol;
	}
}