package ${package.Mapper};

import com.zlyx.easy.mybatis.annotations.EasyMapper;

import ${package.Entity}.${entity};
import ${superMapperClassPackage};

/**
 * ${table.comment!}Mapper接口
 * @author ${author}
 * @since ${date}
 */
@EasyMapper(todo = "${table.comment}Mapper接口")
<#if kotlin>
interface ${table.mapperName} : ${superMapperClass}<${entity}>
<#else>
public interface ${table.mapperName} extends ${superMapperClass}<${entity}> {

}
</#if>
