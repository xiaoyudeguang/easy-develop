package ${package.Controller};

import org.springframework.web.bind.annotation.RequestMethod;

import com.zlyx.easy.core.utils.HttpUtils.HttpResponse;
import com.zlyx.easy.swagger.annotations.SpringMapping;
<#if restControllerStyle>
import com.zlyx.easy.swagger.annotations.SpringController;
<#else>
import org.springframework.stereotype.Controller;
</#if>
<#if superControllerClassPackage??>
import ${superControllerClassPackage};
</#if>
import ${package.Service}.${table.serviceName};
import ${package.Entity}.${entity};

/**
 * ${table.comment!}相关接口
 * @author ${author}
 * @date ${date}
 */
<#if restControllerStyle>
@SpringController(value = {"<#if package.ModuleName??>/${package.ModuleName}</#if>/<#if controllerMappingHyphenStyle??>${controllerMappingHyphen}<#else>${table.entityPath}</#if>"}, todo = {"${table.comment}相关接口"})
<#else>
@Controller
</#if>
<#if kotlin>
class ${table.controllerName}<#if superControllerClass??> : ${superControllerClass}<${table.serviceName}, ${entity}>()</#if>
<#else>
<#if superControllerClass??>
public class ${table.controllerName} extends ${superControllerClass}<${table.serviceName}, ${entity}> {
<#else>
public class ${table.controllerName} {
</#if>

	@SpringMapping(value = { "/test" }, todo = { "调用service层示例" }, method = RequestMethod.GET)
	public int callService(String name, Integer age) {
		return this.baseService.hashCode();
	}
	
	@SpringMapping(value = { "/throwException" }, todo = { "测试异常处理" })
	public HttpResponse throwException(String name, Integer age) throws Exception {
		throw new Exception("测试接口是否正常返回");
	}
}
</#if>
