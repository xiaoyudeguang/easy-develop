package com.zlyx.easy.web.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import com.baomidou.mybatisplus.generator.config.DataSourceConfig;
import com.baomidou.mybatisplus.generator.config.ITypeConvert;
import com.zlyx.easy.web.generate.core.defaults.DefaultTypeConvert;

/**
 * @Auth 赵光
 * @Describle
 * @2019年6月12日 下午6:48:27
 */
@Configuration
@ConditionalOnClass(DataSourceConfig.class)
@EnableConfigurationProperties(DataSourceProperties.class)
public class CodeConfiguration {

	@Autowired
	DataSourceProperties properties;

	@Bean
	@ConditionalOnMissingBean
	public DataSourceConfig getDataSource(ITypeConvert typeConvert) {
		DataSourceConfig dataSource = new DataSourceConfig();
		dataSource.setUrl(properties.getUrl());
		dataSource.setDriverName(properties.getDriverClassName());
		dataSource.setUsername(properties.getUsername());
		dataSource.setPassword(properties.getPassword());
		dataSource.setTypeConvert(typeConvert);
		return dataSource;
	}

	@Bean
	@ConditionalOnMissingBean
	public PaginationInterceptor paginationInterceptor() {
		return new PaginationInterceptor();
	}

	@Bean
	@ConditionalOnMissingBean
	public ITypeConvert typeConvert() {
		return new DefaultTypeConvert();
	}
}
