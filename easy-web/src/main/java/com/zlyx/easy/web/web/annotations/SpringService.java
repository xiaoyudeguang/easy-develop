package com.zlyx.easy.web.web.annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.springframework.core.annotation.AliasFor;
import org.springframework.stereotype.Service;

/**
 * 增强型@Service注解
 * 
 * @author 赵光
 * @version 创建时间：2018年10月29日 上午8:55:45
 * @ClassName 类名称
 * @Description 类描述
 */
@Documented
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Service
public @interface SpringService {

	/**
	 * 请求路径
	 * 
	 * @return
	 */
	@AliasFor(annotation = Service.class, attribute = "value")
	String value() default "";

	/**
	 * 作用
	 * 
	 * @return
	 */
	String todo();

	/**
	 * 描述
	 * 
	 * @return
	 */
	String[] desc() default "";
}
