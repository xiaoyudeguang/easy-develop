package com.zlyx.easy.web.web.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zlyx.easy.web.web.mybatis.AbstractMapper;

/**
 * @Auth 赵光
 * @Describle
 * @Date 2019年1月4日 下午9:04:02
 */
public interface AbstractService<M extends AbstractMapper<T>, T> extends IService<T> {

	/**
	 * 执行sql
	 * 
	 * @param sql
	 * @return
	 */
	public Object executeSQL(String sql);

}
