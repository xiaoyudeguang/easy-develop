package com.zlyx.easy.web.web.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zlyx.easy.web.web.mybatis.AbstractMapper;
import com.zlyx.easy.web.web.service.AbstractService;

/**
 * @Auth 赵光
 * @Describle
 * @Date 2019年1月4日 下午9:03:27
 */
public abstract class AbstractServiceImpl<Mapper extends AbstractMapper<T>, T> extends ServiceImpl<Mapper, T>
		implements AbstractService<Mapper, T> {

	protected Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	private Mapper mapper;

	@Override
	public Object executeSQL(String sql) {
		return mapper.executeSQL(sql);
	}
}