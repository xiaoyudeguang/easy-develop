package com.zlyx.easy.web.generate.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;

import com.zlyx.easy.core.map.EasyMap;
import com.zlyx.easy.core.utils.JsonUtils;
import com.zlyx.easy.swagger.annotations.SpringController;
import com.zlyx.easy.swagger.annotations.SpringMapping;
import com.zlyx.easy.web.generate.WebGenerator;
import com.zlyx.easy.web.generate.model.ModuleInfo;

import springfox.documentation.annotations.ApiIgnore;

/**
 * @Auth 赵光
 * @Desc 描述
 * @Date 2019年1月19日
 */
@ApiIgnore
@SpringController(value = { "/code" }, todo = { "代码生成" }, level = -1)
public class GenerateController {

	@Autowired
	private WebGenerator codeGenerator;

	@SpringMapping(value = { "/excute" }, todo = { "执行代码生成" })
	public String excute(@RequestBody String moduleInfo) throws Exception {
		EasyMap<String, Object> jsonObject = JsonUtils.toMap(moduleInfo);
		ModuleInfo info = new ModuleInfo();
		info.setAuth(jsonObject.getString("module_auth"));
		info.setPath(jsonObject.getString("module_path"));
		info.setNames(jsonObject.getList("entity_names"));
		info.setPaths(jsonObject.getList("entity_paths"));
		info.setTables(jsonObject.getList("entity_tables"));
		codeGenerator.batchGenerate(info);
		return "success";
	}
}
