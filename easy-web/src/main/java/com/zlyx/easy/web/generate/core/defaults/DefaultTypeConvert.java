/**
 *
 * @Author 赵光
 * @Desc CodeTypeConvert
 * @Date 2020年5月22日
 */
package com.zlyx.easy.web.generate.core.defaults;

import com.baomidou.mybatisplus.generator.config.GlobalConfig;
import com.baomidou.mybatisplus.generator.config.ITypeConvert;
import com.baomidou.mybatisplus.generator.config.rules.DbColumnType;
import com.baomidou.mybatisplus.generator.config.rules.IColumnType;

/**
 * 默认类型转换器
 * 
 * @Author 赵光
 * @Desc CodeTypeConvert
 * @Date 2019年5月22日
 */
public class DefaultTypeConvert implements ITypeConvert {

	public static ITypeConvert typeConvert = new DefaultTypeConvert();

	@Override
	public IColumnType processTypeConvert(GlobalConfig globalConfig, String fieldType) {
		String t = fieldType.toLowerCase();
		if (t.contains("char")) {
			return DbColumnType.STRING;
		} else if (t.contains("bigint")) {
			return DbColumnType.LONG;
		} else if (t.contains("tinyint(1)")) {
			return DbColumnType.BOOLEAN;
		} else if (t.contains("int")) {
			return DbColumnType.INTEGER;
		} else if (t.contains("text")) {
			return DbColumnType.STRING;
		} else if (t.contains("bit")) {
			return DbColumnType.BOOLEAN;
		} else if (t.contains("decimal")) {
			return DbColumnType.BIG_DECIMAL;
		} else if (t.contains("clob")) {
			return DbColumnType.CLOB;
		} else if (t.contains("blob")) {
			return DbColumnType.BLOB;
		} else if (t.contains("binary")) {
			return DbColumnType.BYTE_ARRAY;
		} else if (t.contains("float")) {
			return DbColumnType.FLOAT;
		} else if (t.contains("double")) {
			return DbColumnType.DOUBLE;
		}
		return DbColumnType.STRING;
	}

}
