package com.zlyx.easy.web.router;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import springfox.documentation.annotations.ApiIgnore;

/**
 * @Auth 赵光
 * @Desc 描述
 * @Date 2019年1月17日
 */
@ApiIgnore
@Controller
public class RouterController {

	@RequestMapping("easy-web")
	public String home() {
		return "redirect:index.html";
	}

	@RequestMapping("index")
	public String index() {
		return "redirect:index.html";
	}

	@RequestMapping("easy-code")
	public String code() {
		return "redirect:code.html";
	}
}
