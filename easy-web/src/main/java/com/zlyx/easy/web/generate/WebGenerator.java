package com.zlyx.easy.web.generate;

import org.springframework.stereotype.Component;

import com.baomidou.mybatisplus.generator.config.DataSourceConfig;
import com.zlyx.easy.core.buffer.EasyBuffer;
import com.zlyx.easy.core.loggers.Logger;
import com.zlyx.easy.core.tool.CamelTool;
import com.zlyx.easy.core.utils.StringUtils;
import com.zlyx.easy.web.generate.core.CodeGenerator;
import com.zlyx.easy.web.generate.model.ModuleInfo;

/**
 * 
 * @Auth 赵光
 * @Describle
 * @Date 2019年1月22日
 */
@Component
public class WebGenerator {

	private CodeGenerator codeGenerator;

	public WebGenerator(DataSourceConfig dataSource) {
		codeGenerator = new CodeGenerator(dataSource);
	}

	/**
	 * 执行代码生成
	 * 
	 * @param moduleInfo
	 */
	public void batchGenerate(ModuleInfo moduleInfo) {
		String packageName = null;
		String entityName = null;
		String tableName = null;
		for (int i = 0; i < moduleInfo.getNames().size(); i++) {
			tableName = moduleInfo.getTables().get(i);
			if (StringUtils.isNotEmpty(tableName)) {
				entityName = moduleInfo.getNames().get(i);
				if (StringUtils.isEmpty(entityName)) {
					entityName = CamelTool.underline2Camel(tableName);
				}
				packageName = moduleInfo.getPaths().get(i);
				if (StringUtils.isEmpty(packageName)) {
					packageName = entityName.toLowerCase();
				}
				codeGenerator.run(packageName, tableName, entityName, moduleInfo.getAuth());
			} else {
				Logger.warn(EasyBuffer.wrapper("第", i, "个业务线的数据表为空！"));
			}
		}
	}

}
