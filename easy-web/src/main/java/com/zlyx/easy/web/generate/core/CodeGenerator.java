package com.zlyx.easy.web.generate.core;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.config.DataSourceConfig;
import com.baomidou.mybatisplus.generator.config.GlobalConfig;
import com.baomidou.mybatisplus.generator.config.PackageConfig;
import com.baomidou.mybatisplus.generator.config.StrategyConfig;
import com.baomidou.mybatisplus.generator.config.TemplateConfig;
import com.baomidou.mybatisplus.generator.config.rules.DateType;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;
import com.zlyx.easy.core.utils.StringUtils;
import com.zlyx.easy.web.web.controller.AbstractController;
import com.zlyx.easy.web.web.mybatis.AbstractMapper;
import com.zlyx.easy.web.web.service.AbstractService;
import com.zlyx.easy.web.web.service.impl.AbstractServiceImpl;

/**
 * @Auth 赵光
 * @Describle
 * @2019年1月5日 下午12:59:27
 */
@Component
public class CodeGenerator {

	private Logger logger = LoggerFactory.getLogger(getClass());

	protected static String basePath = System.getProperty("user.dir") + "/src/main/java";

	private DataSourceConfig dataSource;

	public CodeGenerator(DataSourceConfig dataSource) {
		this.dataSource = dataSource;
	}

	/**
	 * 代码生成
	 * 
	 * @param packageName 包名
	 * @param entityName  实体名
	 * @param tableName   表名
	 * @param author      作者
	 */
	@SuppressWarnings("deprecation")
	public void run(String packageName, String entityName, String tableName, String author) {
		logger.info("开始执行代码生成...");
		AutoGenerator mpg = new AutoGenerator();
		mpg.setGlobalConfig(getGlobalConfig(entityName, author));
		mpg.setDataSource(dataSource);
		PackageConfig pc = new PackageConfig().setParent(packageName);
		mpg.setPackageInfo(pc);
		mpg.setTemplate(new TemplateConfig().setXml(null));

		// 策略配置
		StrategyConfig strategy = new StrategyConfig();
		strategy.setNaming(NamingStrategy.underline_to_camel);
		strategy.setColumnNaming(NamingStrategy.underline_to_camel);
		strategy.setEntityTableFieldAnnotationEnable(true);
		strategy.setRestControllerStyle(true);
		strategy.setSuperControllerClass(AbstractController.class.getName());
		strategy.setSuperServiceClass(AbstractService.class.getName());
		strategy.setSuperServiceImplClass(AbstractServiceImpl.class.getName());
		strategy.setSuperMapperClass(AbstractMapper.class.getName());
		strategy.setInclude(tableName);
		strategy.setControllerMappingHyphenStyle(true);
		strategy.setTablePrefix(pc.getModuleName() + "_");
		strategy.setEntityTableFieldAnnotationEnable(true);
		strategy.setEntityBuilderModel(true);
		mpg.setStrategy(strategy);
		mpg.setTemplateEngine(new FreemarkerTemplateEngine());
		mpg.execute();
		logger.info("代码生成结束...");
	}

	/**
	 * 文件命名规则配置
	 * 
	 * @param entityName 实体名称
	 * @return
	 */
	public GlobalConfig getGlobalConfig(String entityName, String author) {
		entityName = StringUtils.toUpCaseFirst(entityName);
		GlobalConfig gc = new GlobalConfig();
		gc.setOutputDir(basePath);
		gc.setAuthor(author);
		gc.setOpen(false);
		gc.setDateType(DateType.ONLY_DATE);
		gc.setEntityName(entityName);
		gc.setMapperName(entityName + "Mapper");
		gc.setControllerName(entityName + "Controller");
		gc.setServiceName(entityName + "Service");
		gc.setServiceImplName(entityName + "ServiceImpl");
		return gc;
	}

}
