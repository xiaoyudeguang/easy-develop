package com.zlyx.easy.web.web.mybatis;

import org.apache.ibatis.annotations.SelectProvider;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Auth 赵光
 * @Describle
 * @Date 2019年1月4日 下午9:06:53
 */
public interface AbstractMapper<T> extends BaseMapper<T> {

	/**
	 * 执行sql
	 * 
	 * @param sql
	 * @return
	 */
	@SelectProvider(method = "sql", type = AbstractMapper.class)
	public Object executeSQL(String sql);

	default String sql(String sql) {
		return sql;
	}

}
