package com.zlyx.easy.web.generate.model;

import java.util.List;

public class ModuleInfo {
	
	private String path;
	
	private String auth;
	
	private List<String> names;
	
	private List<String> tables;
	
	private List<String> paths;

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getAuth() {
		return auth;
	}

	public void setAuth(String auth) {
		this.auth = auth;
	}

	public List<String> getNames() {
		return names;
	}

	public void setNames(List<String> names) {
		this.names = names;
	}

	public List<String> getTables() {
		return tables;
	}

	public void setTables(List<String> tables) {
		this.tables = tables;
	}

	public List<String> getPaths() {
		return paths;
	}

	public void setPaths(List<String> paths) {
		this.paths = paths;
	}

	@Override
	public String toString() {
		return "ModuleInfo [path=" + path + ", auth=" + auth + ", names=" + names + ", tables="
				+ tables + ", paths=" + paths + "]";
	}
	
}
